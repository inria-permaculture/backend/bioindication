# Bioindication - backend

## Architecture
If you want to understand this repository architecture, here the detailled folder structure :

    __init__.py -> Make this directory considered as a Python package
    settings.py -> Settings/configuration for this Django project
    urls.py -> The URL declarations for this Django project; a “table of contents” of this Django-powered site
    wsgi.py -> An entry-point for WSGI-compatible web servers to serve this project