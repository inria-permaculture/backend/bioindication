import subprocess
import sys

tests = []
tests.append("python manage.py test --noinput core vernaclarify inventory identify bioindicator")

for test in tests:
    print(test)
    result = subprocess.run(test.split(" "), capture_output=True, text=True)
    print(result.stdout)
    print(result.stderr)
    if (result.returncode != 0):
        sys.exit(1)
