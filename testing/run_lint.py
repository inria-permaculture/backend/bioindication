import subprocess
import sys

# Run pylint and flake8 for the project and each app therein
tests = []
project = "bioindication"
apps = ["core", "vernaclarify", "inventory", "identify", "bioindicator"]

tests.append(f"pylint --load-plugins pylint_django --django-settings-module=bioindication.settings {project}")
tests.append(f"flake8 --exclude=migrations,__init__.py --max-line-length=100 {project}")

for app in apps:
    tests.append(f"pylint --load-plugins pylint_django --django-settings-module=bioindication.settings {app}")
    tests.append(f"flake8 --exclude=migrations,__init__.py --max-line-length=100 apps/{app}")

allpass = True
for test in tests:
    print(test)
    result = subprocess.run(test.split(" "), capture_output=True, text=True)
    print(result.stdout)
    print(result.stderr)
    if (result.returncode != 0):
        allpass = False

sys.exit(not allpass)
