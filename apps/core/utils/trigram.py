"""
Custom  postgresql trigram  (pg_trgm) stuff
https://www.postgresql.org/docs/9.6/pgtrgm.html
"""

from django.contrib.postgres.search import TrigramBase


# pylint: disable=W0223
class TrigramWordSimilarity(TrigramBase):
    """Add support for the word_similarity function from pg_trgm"""
    function = 'WORD_SIMILARITY'
