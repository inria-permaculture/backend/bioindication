"""
Datetime processing utils functions
"""
import datetime


def are_datetime_and_json_date_equal(model_date, json_date):
    """
    Convert date from python datetime and json format to a common one and compare them.
    Return true if it"s the same date or the two are null
    """
    # check if the two dates are not null
    if model_date and json_date:
        # convert them in a common format
        converted_model_date = model_date.replace(tzinfo=None)
        converted_json_date = datetime.datetime.strptime(
            json_date, "%Y-%m-%dT%H:%M:%S.%fZ")
        # return the comparaison result
        return converted_json_date == converted_model_date
    # check if the two dates are null
    if not model_date and not json_date:
        # they are all null so we return true
        return True
    # one of them is null and the other not, so we return false
    return False
