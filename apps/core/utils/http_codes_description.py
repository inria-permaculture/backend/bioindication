"""
HTTP codes description used in open api doc
"""

SUCCESS = "OK"
CREATED = "Created"
SUCCESS_NO_CONTENT = "No Content"
NOT_FOUND = "Not Found"
BAD_REQUEST = "Bad Request"
REQUEST_ENTITY_TOO_LARGE = "Request Entity Too Large"
BAD_GATEWAY = "Bad Gateway"
