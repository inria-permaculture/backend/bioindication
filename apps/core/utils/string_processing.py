"""
String processing utils functions
"""

from django.utils.html import conditional_escape


def escape_string(value):
    """Output escaped string"""
    return conditional_escape(value)
