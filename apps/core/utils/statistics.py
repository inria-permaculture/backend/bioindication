"""
Statistics related utils functions
"""

from math import sqrt
from decimal import Decimal


def calc_weighted_mean(weighted_values, weights):
    """
    Calculate the weighted mean
    """
    weighted_mean = sum(weighted_values)/sum(weights)
    return weighted_mean


def calc_weighted_squared_deviation_to_mean(weight, value, mean):
    """
    Calculate the weighted squared deviation to mean
    """
    weighted_squared_deviation_to_mean = weight * \
        ((value - mean) ** 2)
    return weighted_squared_deviation_to_mean


def calc_corrected_weighted_standard_deviation(sum_weighted_squared_deviation_to_mean, weights):
    """
    Calculate the corrected and weighted standard deviation
    """
    sum_weights = Decimal(sum(weights))
    nb_weights = len(weights)
    corrected_nb_of_weights = Decimal((
        nb_weights - 1) / nb_weights)
    if nb_weights == 1:
        corrected_weighted_standard_deviation = 0
    else:
        corrected_weighted_standard_deviation = sqrt(
            sum_weighted_squared_deviation_to_mean/(corrected_nb_of_weights * sum_weights))
    return corrected_weighted_standard_deviation
