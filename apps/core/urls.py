"""
API endpoints routes definitions
"""
from django.conf.urls import url
from core.views import GlobalStatisticsView

urlpatterns = [
    url(r'^stats/',
        GlobalStatisticsView.as_view(), name="statistics")
]
