"""
Admin panel models binding
"""

from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from core.models import Taxon, Identifiers, VernacularEntry, ScientificEntry


class TaxonAdmin(SimpleHistoryAdmin):
    """Taxon panel definition"""

    def has_add_permission(self, request):
        return False

    class IdentifiersInline(admin.TabularInline):
        """Identifiers subpanel definition"""
        model = Identifiers

    class VernacularEntryInline(admin.TabularInline):
        """Vernacular subpanel definition"""
        model = VernacularEntry

    class ScientificEntryInline(admin.TabularInline):
        """Scientific subpanel definition"""
        model = ScientificEntry

    fields = [field.name for field in Taxon._meta.fields]
    raw_id_fields = ("parent", "main_scientific_entry")
    readonly_fields = ("id",)
    inlines = [IdentifiersInline,
               VernacularEntryInline, ScientificEntryInline]
    history_list_display = ("parent", "main_scientific_entry", "rank")


admin.site.register(Taxon, TaxonAdmin)
