"""
Admin panel models binding
"""

from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from core.models import InventoryList


class InventoryListAdmin(SimpleHistoryAdmin):
    """InventoryList panel definition"""

    fields = [field.name for field in InventoryList._meta.fields]
    readonly_fields = ("id", "creation_date",
                       "last_update_date", "expiry_date")
    history_list_display = fields


admin.site.register(InventoryList, InventoryListAdmin)
