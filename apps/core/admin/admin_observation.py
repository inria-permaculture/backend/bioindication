"""
Admin panel models binding
"""

from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from core.models import Observation


class ObservationAdmin(SimpleHistoryAdmin):
    """Observation panel definition"""

    fields = [field.name for field in Observation._meta.fields]
    raw_id_fields = ("inventory", "taxon")
    readonly_fields = ("id", "creation_date", "last_update_date")
    history_list_display = fields


admin.site.register(Observation, ObservationAdmin)
