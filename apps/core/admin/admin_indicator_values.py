"""
Admin panel models binding
"""

from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from core.models import IndicatorValues


class IndicatorValuesAdmin(SimpleHistoryAdmin):
    """Indicator values panel definition"""

    fields = [field.name for field in IndicatorValues._meta.fields]
    raw_id_fields = ("taxon",)
    history_list_display = fields


admin.site.register(IndicatorValues, IndicatorValuesAdmin)
