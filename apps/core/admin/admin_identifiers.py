"""
Admin panel models binding
"""

from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from core.models import Identifiers


class IdentifiersAdmin(SimpleHistoryAdmin):
    """Identifiers panel definition"""

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    fields = [field.name for field in Identifiers._meta.fields]
    raw_id_fields = ("taxon",)
    history_list_display = fields


admin.site.register(Identifiers, IdentifiersAdmin)
