"""
Admin panel models binding
"""

from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from core.models import Uri


class UriAdmin(SimpleHistoryAdmin):
    """Uri panel definition"""

    fields = [field.name for field in Uri._meta.fields]
    raw_id_fields = ("inventory_list",)
    readonly_fields = ("id", "creation_date", "last_update_date")
    history_list_display = ("parent", "main_scientific_entry", "rank")


admin.site.register(Uri, UriAdmin)
