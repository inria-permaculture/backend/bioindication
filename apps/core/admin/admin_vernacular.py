"""
Admin panel models binding
"""

from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from core.models import VernacularEntry


class VernacularAdmin(SimpleHistoryAdmin):
    """Vernacular panel definition"""

    fields = [field.name for field in VernacularEntry._meta.fields]
    raw_id_fields = ("taxon",)
    readonly_fields = ("id",)
    history_list_display = ("parent", "main_scientific_entry", "rank")


admin.site.register(VernacularEntry, VernacularAdmin)
