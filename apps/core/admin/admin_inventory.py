"""
Admin panel models binding
"""

from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from core.models import Inventory


class InventoryAdmin(SimpleHistoryAdmin):
    """Inventory panel definition"""

    fields = [field.name for field in Inventory._meta.fields]
    raw_id_fields = ("inventory_list",)
    readonly_fields = ("id", "creation_date",
                       "last_update_date", "expiry_date")
    history_list_display = fields


admin.site.register(Inventory, InventoryAdmin)
