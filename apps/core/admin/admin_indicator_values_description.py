"""
Admin panel models binding
"""
from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from core.models import IndicatorValuesDescription


class IndicatorValuesDescriptionAdmin(SimpleHistoryAdmin):
    """
    Indicator values description panel definition
    """

    def has_delete_permission(self, request, obj=None):
        return False

    fields = [field.name for field in IndicatorValuesDescription._meta.fields]
    readonly_fields = ("id",)
    history_list_display = fields


admin.site.register(IndicatorValuesDescription,
                    IndicatorValuesDescriptionAdmin)
