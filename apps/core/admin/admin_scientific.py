"""
Admin panel models binding
"""

from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from core.models import ScientificEntry


class ScientificAdmin(SimpleHistoryAdmin):
    """Scientific panel definition"""

    fields = [field.name for field in ScientificEntry._meta.fields]
    raw_id_fields = ("taxon",)
    readonly_fields = ("id",)
    history_list_display = fields


admin.site.register(ScientificEntry, ScientificAdmin)
