"""
Admin panel models binding
"""

from core.admin.admin_identifiers import *
from core.admin.admin_indicator_values_description import *
from core.admin.admin_indicator_values import *
from core.admin.admin_inventory_list import *
from core.admin.admin_inventory import *
from core.admin.admin_observation import *
from core.admin.admin_plant_usages import *
from core.admin.admin_scientific import *
from core.admin.admin_taxon import *
from core.admin.admin_uri import *
from core.admin.admin_vernacular import *
