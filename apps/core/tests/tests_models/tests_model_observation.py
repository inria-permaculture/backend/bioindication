"""
Test the observation model
"""
from django.test import TestCase
from django.core.exceptions import ValidationError
from core.models import Observation


class ObservationModelFieldQuantificationPercentageTestCase(TestCase):
    """
    Test the observation"s soil_coverage validators
    """

    def test_set_lower_than_0_throw_error(self):
        """
        Test setting an soil_coverage with value lower than 0
        must send a ValidationError
        """
        with self.assertRaises(ValidationError):
            observation = Observation(soil_coverage=-1)
            observation.full_clean(exclude=("inventory", "taxon"))

    def test_set_greater_than_1_throw_error(self):
        """
        Test setting an soil_coverage with value greater than 1
        must send a ValidationError
        """
        with self.assertRaises(ValidationError):
            observation = Observation(soil_coverage=2)
            observation.full_clean(exclude=("inventory", "taxon"))

    def test_set_between_0_to_1_throw_no_error(self):
        """
        Test setting an soil_coverage with value between range [0,1]
        mustn"t send any error
        """
        # pylint: disable=bare-except
        raised = False
        try:
            observation = Observation(soil_coverage=0.5)
            observation.full_clean(exclude=("inventory", "taxon"))
        except ValidationError:
            raised = True
        self.assertFalse(raised)
