"""
Test classes for models
"""

from core.tests.tests_models.tests_models_validators import *
from core.tests.tests_models.tests_model_observation import *
from core.tests.tests_models.tests_model_uri import *
