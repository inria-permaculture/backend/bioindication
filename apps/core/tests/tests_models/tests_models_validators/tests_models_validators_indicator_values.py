# pylint: disable=missing-module-docstring
from django.test import TestCase
from django.core.exceptions import ValidationError
from core.models import IndicatorValuesDescription, IndicatorValues
from core.models.validators.validators_indicator_values import validate_indicator_values_field


class IndicatorValuesGenericFieldValidatorTestCase(TestCase):
    """
    Test the indicator value fields generic validator
    Retrieve dynamically all indicator values fields and test
    if the generic validator work with the current range defined
    by the unique IndicatorValuesDescription row
    """

    def setUp(self):
        """
        Set up common data
        """
        # load unique description to get the defined min/max range
        self.description = IndicatorValuesDescription.objects.first()
        # get all indic values from IndicatorValues model
        self.indicator_values_fields = [
            field.name for field in IndicatorValues._meta.fields
            if field.name not in ("taxon", "history") and "stdev" not in field.name]

    def test_set_value_lower_than_minimum_throw_error(self):
        """
        Test setting a value for a field lower than the minimum accepted
        value defined by the IndicatorValuesDescription related field
        must throw a ValidationError
        """
        nb_value_fields = 0
        nb_validation_errors_raised = 0
        for field in self.indicator_values_fields:
            nb_value_fields += 1
            try:
                min_value_defined = getattr(
                    self.description, f'{field}_min_value')
                validate_indicator_values_field(field, min_value_defined-1)
            except ValidationError:
                nb_validation_errors_raised += 1
        self.assertTrue(nb_value_fields == nb_validation_errors_raised)

    def test_set_value_greater_than_maximinum_throw_error(self):
        """
        Test setting a value for a field greater than the maximum accepted
        value defined by the IndicatorValuesDescription related field
        must throw a ValidationError
        """
        nb_value_fields = 0
        nb_validation_errors_raised = 0
        for field in self.indicator_values_fields:
            nb_value_fields += 1
            try:
                max_value_defined = getattr(
                    self.description, f'{field}_max_value')
                validate_indicator_values_field(field, max_value_defined+1)
            except ValidationError:
                nb_validation_errors_raised += 1
        self.assertTrue(nb_value_fields == nb_validation_errors_raised)

    def test_set_value_in_range_dont_throw_error(self):
        """
        Test setting a value for a field in the accepted range
        defined by the IndicatorValuesDescription min and max fields
        must not throw any error
        """
        nb_validation_errors_raised = 0
        for field in self.indicator_values_fields:
            try:
                max_value_defined = getattr(
                    self.description, f'{field}_max_value')
                validate_indicator_values_field(field, max_value_defined-1)
            except ValidationError:
                nb_validation_errors_raised += 1
        self.assertTrue(nb_validation_errors_raised == 0)
