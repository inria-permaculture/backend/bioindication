"""
Test the uri model
"""
from django.test import TestCase
from django.core.exceptions import ValidationError
from core.models import Uri
from core.models.model_inventory_list import InventoryList


class UriModelFieldIdentifierTestCase(TestCase):
    """
    Test the uri"s identifier validators
    """

    def test_set_not_enough_character_throw_error(self):
        """
        Test setting an identifier with not enough characters must throw a ValidationError
        """
        with self.assertRaises(ValidationError):
            uri = Uri(identifier="a")
            uri.full_clean(exclude=("id", "inventory_list"))

    def test_set_only_digits_dont_throw_error(self):
        """
        Test setting an identifier with only digits mustn't throw any ValidationError
        """
        raised = False
        try:
            uri = Uri(identifier="11111111")
            uri.full_clean(exclude=("id", "inventory_list"))
        except ValidationError:
            raised = True
        self.assertFalse(raised)

    def test_set_characters_not_only_digits_letters_dash_throw_error(self):
        """
        Test setting an identifier with characters that are not only digits/letters/dash
        must throw a ValidationError
        """
        with self.assertRaises(ValidationError):
            uri = Uri(identifier="hell&$^|2éàë")
            uri.full_clean(exclude=("id", "inventory_list"))

    def test_set_digits_letters_dash_throw_no_error(self):
        """
        Test setting an identifier with digits/letters/dash mustn't throw any ValidationError
        """
        raised = False
        try:
            uri = Uri(identifier="ceciestunbonidentifier-1")
            uri.full_clean(exclude=("id", "inventory_list"))
        except ValidationError:
            raised = True
        self.assertFalse(raised)

    def test_create_duplicate_identifier_throw_error(self):
        """
        Test setting twice the same identifier must throw a ValidationError
        """
        with self.assertRaises(ValidationError):
            duplicated_identifier = "coucou"
            inventory_list_to_please_uri = InventoryList.objects.create()
            Uri.objects.create(
                inventory_list=inventory_list_to_please_uri,
                identifier=duplicated_identifier)
            uri_with_duplicated_identifier = Uri(
                identifier=duplicated_identifier)
            uri_with_duplicated_identifier.full_clean(
                exclude=("id", "inventory_list"))
