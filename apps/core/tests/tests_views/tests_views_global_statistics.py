# pylint: disable=missing-module-docstring
from rest_framework import reverse, status
from rest_framework.test import APITestCase
from core.models import (Taxon, ScientificEntry,
                         VernacularEntry, InventoryList, Inventory, Observation)


class TestGlobalStatisticsApiViewTestCase(APITestCase):
    """
    Test the get global statistics view
    """

    def setUp(self):
        taxon_1 = Taxon.objects.create(id=0, is_a_plant=False)
        ScientificEntry.objects.create(taxon=taxon_1, name="Mentha x piperita")
        VernacularEntry.objects.create(taxon=taxon_1, name="Menthe poivrée")
        VernacularEntry.objects.create(taxon=taxon_1, name="Menthe débouche")

        taxon_2 = Taxon.objects.create(id=1, is_a_plant=True)
        taxon_3 = Taxon.objects.create(id=2, is_a_plant=True)

        inventory_list = InventoryList.objects.create()
        InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)
        Inventory.objects.create(
            inventory_list=inventory_list)
        Inventory.objects.create(
            inventory_list=inventory_list)
        Observation.objects.create(
            inventory=inventory, taxon=taxon_1)
        Observation.objects.create(
            inventory=inventory, taxon=taxon_2)
        Observation.objects.create(
            inventory=inventory, taxon=taxon_3)
        Observation.objects.create(
            inventory=inventory, taxon=taxon_3)

    def test_get_global_statistics_returns_http_code_200(self):
        """
        Test get global stats must return http code 200
        """
        response = self.client.get(
            reverse.reverse("statistics"),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_global_statistics_returns_valid_stats_data(self):
        """
        Test get global stats must return valid stats data
        """
        response = self.client.get(
            reverse.reverse("statistics"),
            format="json")

        self.assertEqual(response.data["nb_taxa"], 3)
        self.assertEqual(response.data["nb_scientific_names"], 1)
        self.assertEqual(response.data["nb_vernacular_names"], 2)
        self.assertEqual(response.data["nb_inventory_lists"], 2)
        self.assertEqual(response.data["nb_inventories"], 3)
        self.assertEqual(response.data["nb_observations"], 4)
        self.assertEqual(response.data["nb_plants_observed"], 3)
        self.assertEqual(response.data["nb_unique_plants_observed"], 2)
        self.assertEqual(response.data["nb_unique_taxa_observed"], 3)
