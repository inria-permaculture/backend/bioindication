"""
Test the string processing module
"""
from django.test import TestCase
from core.utils import string_processing


class EscapeStringTestCase(TestCase):
    """
    Test the escape string function
    """

    def test_escape_string_without_dangerous_character_returns_same_string(self):
        """
        Test escaping a string without dangerous character must return the same string as inputed
        """
        my_non_dangerous_string = "coucoulescopains"
        self.assertEqual(my_non_dangerous_string,
                         string_processing.escape_string(my_non_dangerous_string))

    def test_escape_string_with_dangerous_characters_returns_escaped_string(self):
        """
        Test escaping a string withdangerous characters must return an escaped string
        """
        my_dangerous_string = "<script>&'maliciousscripthere < /script >"
        self.assertEqual("&lt;script&gt;&amp;&#x27;maliciousscripthere &lt; /script &gt;",
                         string_processing.escape_string(my_dangerous_string))
