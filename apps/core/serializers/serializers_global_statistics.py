"""
Global statistics model transformations
"""
from rest_framework import serializers


class GlobalStatisticsSerializer(serializers.Serializer):
    # pylint: disable=abstract-method
    """
    Global statistics serializer
    """
    nb_taxa = serializers.IntegerField()
    nb_scientific_names = serializers.IntegerField()
    nb_vernacular_names = serializers.IntegerField()
    nb_inventory_lists = serializers.IntegerField()
    nb_inventories = serializers.IntegerField()
    nb_observations = serializers.IntegerField()
    nb_plants_observed = serializers.IntegerField()
    nb_unique_plants_observed = serializers.IntegerField()
    nb_unique_taxa_observed = serializers.IntegerField()
    nb_plant_usages = serializers.IntegerField()
