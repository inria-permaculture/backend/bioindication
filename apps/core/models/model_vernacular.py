"""
Data models definitions
"""

from django.db import models
from django.db.models.functions import Upper
from django.contrib.postgres.indexes import GinIndex, OpClass
from simple_history.models import HistoricalRecords
from core.models.enums import Lifestage, Gender, Language


class VernacularEntry(models.Model):
    """Vernacular model definition"""

    def __str__(self):
        return self.name

    taxon = models.ForeignKey(
        to="Taxon", on_delete=models.CASCADE, related_name="vernacular_entries")
    name = models.TextField(db_index=True)
    language = models.CharField(
        max_length=2, default=Language.FR, choices=Language.choices, blank=True)
    locality = models.CharField(max_length=100, default="", blank=True)
    source = models.CharField(max_length=100, default="", blank=True)
    gender = models.CharField(
        max_length=10, default=Gender.ALL, choices=Gender.choices, blank=True)
    lifestage = models.CharField(
        max_length=11, default=Lifestage.ALL, choices=Lifestage.choices, blank=True)
    comment = models.CharField(max_length=150, default="", blank=True)
    history = HistoricalRecords()

    class Meta:
        indexes = [
            GinIndex(
                OpClass(Upper('name'), name='gin_trgm_ops'),
                name='core_v_name_trgm_gin_idx',
            )
        ]
