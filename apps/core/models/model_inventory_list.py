"""
Data models definitions
"""

from django.db import models
from core.models.enums import Status
from simple_history.models import HistoricalRecords


class InventoryList(models.Model):
    """InventoryList model definition"""

    def __str__(self):
        return f"{self.id} - Inventory list {self.name} in state {self.status}"

    name = models.CharField(max_length=50, db_index=True)
    description = models.TextField(max_length=250, default="", blank=True)
    status = models.CharField(default=Status.OPEN,
                              choices=Status.choices, max_length=8)
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update_date = models.DateTimeField(auto_now=True)
    expiry_date = models.DateTimeField(blank=True, null=True, default=None)
    history = HistoricalRecords()
