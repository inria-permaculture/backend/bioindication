"""
Data models definitions
"""

from core.models.model_identifiers import *
from core.models.model_indicator_values_description import *
from core.models.model_indicator_values import *
from core.models.model_inventory_list import *
from core.models.model_inventory import *
from core.models.model_observation import *
from core.models.model_plant_usages import *
from core.models.model_scientific import *
from core.models.model_taxon import *
from core.models.model_uri import *
from core.models.model_vernacular import *
