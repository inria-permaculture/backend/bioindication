"""
Data models definitions
"""

from decimal import Decimal
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from core.models.enums import Alive, Spontaneous, Lifestage, Gender, Quantification
from simple_history.models import HistoricalRecords


class Observation(models.Model):
    """Observation model definition"""

    def __str__(self):
        # pylint: disable=no-member
        return (f"{self.id} - Taxon {self.taxon.main_scientific_entry.name} in "
                f"inventory {self.inventory.name}")

    inventory = models.ForeignKey(
        to="Inventory", on_delete=models.CASCADE)
    taxon = models.ForeignKey(
        to="Taxon", on_delete=models.CASCADE)
    comment = models.TextField(max_length=250, default="", blank=True)
    observator_nickname = models.CharField(
        max_length=30, default="", blank=True)
    number_of_individuals = models.PositiveIntegerField(blank=True, null=True)
    soil_coverage = models.DecimalField(
        blank=True, null=True, max_digits=4, decimal_places=3,
        validators=[MinValueValidator(Decimal('0.01')),
                    MaxValueValidator(1)])
    alive = models.CharField(default=Alive.UNDEFINED,
                             choices=Alive.choices, max_length=9)
    spontaneous = models.CharField(default=Spontaneous.UNDEFINED,
                                   choices=Spontaneous.choices, max_length=9)
    gender = models.CharField(default=Gender.UNDEFINED,
                              choices=Gender.choices, max_length=9)
    lifestage = models.CharField(default=Lifestage.UNDEFINED,
                                 choices=Lifestage.choices, max_length=11)
    quantification = models.CharField(default=Quantification.PRESENCE,
                                      choices=Quantification.choices, max_length=8)
    user_scientific_name = models.TextField(default="", blank=True)
    user_vernacular_name = models.TextField(default="", blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update_date = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()
