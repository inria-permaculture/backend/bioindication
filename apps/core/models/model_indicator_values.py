"""
Data models definitions
"""

from django.db import models
from simple_history.models import HistoricalRecords
from core.models.model_taxon import Taxon
from core.models.validators.validators_indicator_values import (validate_light,
                                                                validate_temperature,
                                                                validate_continentality,
                                                                validate_edaphic_moisture,
                                                                validate_atmospheric_moisture,
                                                                validate_soil_reaction,
                                                                validate_nutrient,
                                                                validate_salinity,
                                                                validate_soil_texture,
                                                                validate_organic_matter)


class IndicatorValues(models.Model):
    """Indicator values model definition"""

    taxon = models.OneToOneField(
        to=Taxon, on_delete=models.CASCADE, primary_key=True)

    light = models.DecimalField(
        blank=True, null=True, max_digits=3, decimal_places=1,
        validators=[validate_light])
    light_stdev = models.DecimalField(
        blank=True, null=True, default=None, max_digits=3, decimal_places=1)

    temperature = models.DecimalField(
        blank=True, null=True, max_digits=3, decimal_places=1,
        validators=[validate_temperature])
    temperature_stdev = models.DecimalField(
        blank=True, null=True, default=None, max_digits=3, decimal_places=1)

    continentality = models.DecimalField(
        blank=True, null=True, max_digits=3, decimal_places=1,
        validators=[validate_continentality])
    continentality_stdev = models.DecimalField(
        blank=True, null=True, default=None, max_digits=3, decimal_places=1)

    edaphic_moisture = models.DecimalField(
        blank=True, null=True, max_digits=3, decimal_places=1,
        validators=[validate_edaphic_moisture])
    edaphic_moisture_stdev = models.DecimalField(
        blank=True, null=True, default=None, max_digits=3, decimal_places=1)

    atmospheric_moisture = models.DecimalField(
        blank=True, null=True, max_digits=3, decimal_places=1,
        validators=[validate_atmospheric_moisture])
    atmospheric_moisture_stdev = models.DecimalField(
        blank=True, null=True, default=None, max_digits=3, decimal_places=1)

    soil_reaction = models.DecimalField(
        blank=True, null=True, max_digits=3, decimal_places=1,
        validators=[validate_soil_reaction])
    soil_reaction_stdev = models.DecimalField(
        blank=True, null=True, default=None, max_digits=3, decimal_places=1)

    nutrient = models.DecimalField(
        blank=True, null=True, max_digits=3, decimal_places=1,
        validators=[validate_nutrient])
    nutrient_stdev = models.DecimalField(
        blank=True, null=True, default=None, max_digits=3, decimal_places=1)

    salinity = models.DecimalField(
        blank=True, null=True, max_digits=3, decimal_places=1,
        validators=[validate_salinity])
    salinity_stdev = models.DecimalField(
        blank=True, null=True, default=None, max_digits=3, decimal_places=1)

    soil_texture = models.DecimalField(
        blank=True, null=True, max_digits=3, decimal_places=1,
        validators=[validate_soil_texture])
    soil_texture_stdev = models.DecimalField(
        blank=True, null=True, default=None, max_digits=3, decimal_places=1)

    organic_matter = models.DecimalField(
        blank=True, null=True, max_digits=3, decimal_places=1,
        validators=[validate_organic_matter])
    organic_matter_stdev = models.DecimalField(
        blank=True, null=True, default=None, max_digits=3, decimal_places=1)

    history = HistoricalRecords()
