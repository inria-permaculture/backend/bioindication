"""
Data models definitions
"""

from django.db import models
from simple_history.models import HistoricalRecords
from core.models.model_taxon import Taxon


class PlantUsages(models.Model):
    """Plant usages model definition"""

    ValidStrata = models.TextChoices("Stratum", "HERB SHRUB TREE")

    taxon = models.OneToOneField(
        to=Taxon, on_delete=models.CASCADE, primary_key=True, related_name="plant_usages")
    food = models.BooleanField(null=False, default=False)
    honey = models.BooleanField(null=False, default=False)
    ornamental = models.BooleanField(null=False, default=False)
    medicine = models.BooleanField(null=False, default=False)
    fuelwood = models.BooleanField(null=False, default=False)
    construction = models.BooleanField(null=False, default=False)
    psychotropic = models.BooleanField(null=False, default=False)
    poisonous = models.BooleanField(null=False, default=False)
    green_manure = models.BooleanField(null=False, default=False)
    basketry = models.BooleanField(null=False, default=False)
    depolluting = models.BooleanField(null=False, default=False)
    textile = models.BooleanField(null=False, default=False)
    paper = models.BooleanField(null=False, default=False)
    tinctorial = models.BooleanField(null=False, default=False)
    fragrance = models.BooleanField(null=False, default=False)
    stratum = models.CharField(max_length=5, choices=ValidStrata.choices, null=False, default='')
    invasive = models.BooleanField(null=False, default=False)
    history = HistoricalRecords()
