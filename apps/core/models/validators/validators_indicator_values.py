"""
Validators definitions for indicator values fields
"""
import sys
from string import Template
from django.core.exceptions import ValidationError
from core.models.model_indicator_values_description import IndicatorValuesDescription

FIELD_VALUE_NOT_IN_RANGE = Template(
    'La valeur du champ $field doit être entre [$min_value,$max_value]')


def validate_indicator_values_field(field, value):
    """
    Validate that the given value is in
    range defined by the IndicatorValuesDescription unique row
    """
    try:
        indicator_values_description = IndicatorValuesDescription.objects.all()[0]
    except IndexError:
        sys.exit("Error: no IndicatorValuesDescription found in DB")

    min_value = getattr(indicator_values_description, f'{field}_min_value')
    max_value = getattr(indicator_values_description, f'{field}_max_value')
    if value < min_value or value > max_value:
        field_label = getattr(indicator_values_description, f'{field}_label')
        raise ValidationError(FIELD_VALUE_NOT_IN_RANGE.substitute(field=field_label,
                                                                  min_value=min_value,
                                                                  max_value=max_value))


def validate_light(light_value):
    """
    Validate that the given light value is in
    range defined by the IndicatorValuesDescription unique row
    """
    validate_indicator_values_field('light', light_value)


def validate_temperature(temperature_value):
    """
    Validate that the given temperature value is in
    range defined by the IndicatorValuesDescription unique row
    """
    validate_indicator_values_field('temperature', temperature_value)


def validate_continentality(continentality_value):
    """
    Validate that the given continentality value is in
    range defined by the IndicatorValuesDescription unique row
    """
    validate_indicator_values_field('continentality', continentality_value)


def validate_edaphic_moisture(edaphic_moisture_value):
    """
    Validate that the given edaphic_moisture value is in
    range defined by the IndicatorValuesDescription unique row
    """
    validate_indicator_values_field('edaphic_moisture', edaphic_moisture_value)


def validate_atmospheric_moisture(atmospheric_moisture_value):
    """
    Validate that the given light value is in
    range defined by the IndicatorValuesDescription unique row
    """
    validate_indicator_values_field(
        'atmospheric_moisture', atmospheric_moisture_value)


def validate_soil_reaction(soil_reaction_value):
    """
    Validate that the given soil_reaction value is in
    range defined by the IndicatorValuesDescription unique row
    """
    validate_indicator_values_field('soil_reaction', soil_reaction_value)


def validate_nutrient(nutrient_value):
    """
    Validate that the given nutrient value is in
    range defined by the IndicatorValuesDescription unique row
    """
    validate_indicator_values_field('nutrient', nutrient_value)


def validate_salinity(salinity_value):
    """
    Validate that the given salinity value is in
    range defined by the IndicatorValuesDescription unique row
    """
    validate_indicator_values_field('salinity', salinity_value)


def validate_soil_texture(soil_texture_value):
    """
    Validate that the given soil_texture value is in
    range defined by the IndicatorValuesDescription unique row
    """
    validate_indicator_values_field('soil_texture', soil_texture_value)


def validate_organic_matter(organic_matter_value):
    """
    Validate that the given organic_matter value is in
    range defined by the IndicatorValuesDescription unique row
    """
    validate_indicator_values_field('organic_matter', organic_matter_value)
