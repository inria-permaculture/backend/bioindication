"""
Data models definitions
"""

from django.db import models
from simple_history.models import HistoricalRecords


class IndicatorValuesDescription(models.Model):
    """Indicator values description model definition"""

    id = models.IntegerField(primary_key=True)

    light_label = models.CharField(max_length=20)
    light_min_value_label = models.CharField(max_length=20)
    light_max_value_label = models.CharField(max_length=20)
    light_min_value = models.IntegerField()
    light_max_value = models.IntegerField()

    temperature_label = models.CharField(max_length=20)
    temperature_min_value_label = models.CharField(max_length=20)
    temperature_max_value_label = models.CharField(max_length=20)
    temperature_min_value = models.IntegerField()
    temperature_max_value = models.IntegerField()

    continentality_label = models.CharField(max_length=20)
    continentality_min_value_label = models.CharField(max_length=20)
    continentality_max_value_label = models.CharField(max_length=20)
    continentality_min_value = models.IntegerField()
    continentality_max_value = models.IntegerField()

    edaphic_moisture_label = models.CharField(max_length=20)
    edaphic_moisture_min_value_label = models.CharField(max_length=20)
    edaphic_moisture_max_value_label = models.CharField(max_length=20)
    edaphic_moisture_min_value = models.IntegerField()
    edaphic_moisture_max_value = models.IntegerField()

    atmospheric_moisture_label = models.CharField(max_length=20)
    atmospheric_moisture_min_value_label = models.CharField(max_length=20)
    atmospheric_moisture_max_value_label = models.CharField(max_length=20)
    atmospheric_moisture_min_value = models.IntegerField()
    atmospheric_moisture_max_value = models.IntegerField()

    soil_reaction_label = models.CharField(max_length=20)
    soil_reaction_min_value_label = models.CharField(max_length=20)
    soil_reaction_max_value_label = models.CharField(max_length=20)
    soil_reaction_min_value = models.IntegerField()
    soil_reaction_max_value = models.IntegerField()

    nutrient_label = models.CharField(max_length=20)
    nutrient_min_value_label = models.CharField(max_length=20)
    nutrient_max_value_label = models.CharField(max_length=20)
    nutrient_min_value = models.IntegerField()
    nutrient_max_value = models.IntegerField()

    salinity_label = models.CharField(max_length=20)
    salinity_min_value_label = models.CharField(max_length=20)
    salinity_max_value_label = models.CharField(max_length=20)
    salinity_min_value = models.IntegerField()
    salinity_max_value = models.IntegerField()

    soil_texture_label = models.CharField(max_length=20)
    soil_texture_min_value_label = models.CharField(max_length=20)
    soil_texture_max_value_label = models.CharField(max_length=20)
    soil_texture_min_value = models.IntegerField()
    soil_texture_max_value = models.IntegerField()

    organic_matter_label = models.CharField(max_length=20)
    organic_matter_min_value_label = models.CharField(max_length=20)
    organic_matter_max_value_label = models.CharField(max_length=20)
    organic_matter_min_value = models.IntegerField()
    organic_matter_max_value = models.IntegerField()

    history = HistoricalRecords()

    def save(self, *args, **kwargs):
        # pylint: disable=invalid-name
        """
        Avoid creating news indicator values descriptions by
        overriding existing one
        """
        self.id = 1
        super().save(*args, **kwargs)
