"""
Data models definitions
"""

from django.db import models
from django.core.validators import RegexValidator, MinLengthValidator
from simple_history.models import HistoricalRecords


class Uri(models.Model):
    """Uri model definition"""

    def __str__(self):
        return f"{self.id} - Uri {self.identifier} of list {self.inventory_list.name}"

    inventory_list = models.OneToOneField(
        to="InventoryList", on_delete=models.CASCADE, related_name="uri")
    identifier = models.CharField(max_length=100, unique=True, validators=[
        RegexValidator(
            regex="^[a-zA-Z-0-9]+$",
            message="L\"identifier doit être composés de lettres, chiffres ou -"),
        MinLengthValidator(
            limit_value=3)
    ])
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update_date = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()
