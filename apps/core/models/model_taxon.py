"""
Data models definitions
"""

from django.db import models
from core.models.enums import Rank
from simple_history.models import HistoricalRecords


class Taxon(models.Model):
    """Taxon model definition"""

    def __str__(self):
        string_representation = ""

        if self.main_scientific_entry:
            string_representation = self.main_scientific_entry.name
        else:
            string_representation = "No scientific name"

        return f"{self.id} : {string_representation}"

    id = models.IntegerField(primary_key=True)
    parent = models.ForeignKey(
        to="self", on_delete=models.SET_NULL, null=True, default=None)
    main_scientific_entry = models.ForeignKey(
        to="ScientificEntry",
        on_delete=models.SET_NULL,
        null=True,
        default=None,
        related_name="taxon+"
    )
    rank = models.CharField(
        max_length=21, default=Rank.UNDEFINED, choices=Rank.choices, null=False)
    is_a_plant = models.BooleanField(null=False, default=False)
    is_domesticated = models.BooleanField(null=False, default=False)
    history = HistoricalRecords()
