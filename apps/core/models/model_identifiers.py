"""
Data models definitions
"""

from django.db import models
from django.core.validators import RegexValidator
from simple_history.models import HistoricalRecords
from core.models.model_taxon import Taxon


class Identifiers(models.Model):
    """Identifiers model definition"""

    def __str__(self):
        string_representation = ""

        if self.taxon.main_scientific_entry:
            string_representation = self.taxon.main_scientific_entry.name
        else:
            string_representation = "No scientific name"

        return f"{string_representation}"

    taxon = models.OneToOneField(
        to=Taxon, on_delete=models.CASCADE, primary_key=True, related_name="identifiers")
    gbif = models.CharField(max_length=10, validators=[RegexValidator(
        regex="^[0-9]*$",
        message="L'identifiant gbif doit être un nombre entier.")], default="", blank=True)
    ncbi = models.CharField(max_length=10, validators=[RegexValidator(
        regex="^[0-9]*$",
        message="L'identifiant ncbi doit être un nombre entier.")], default="", blank=True)
    inaturalist = models.CharField(max_length=10, validators=[RegexValidator(
        regex="^[0-9]*$",
        message="L'identifiant inaturalist doit être un nombre entier.")], default="", blank=True)
    iucn = models.CharField(max_length=42, validators=[RegexValidator(
        regex="^[0-9]+/[0-9]+$",
        message="L'identifiant iucn doit être composé de deux nombres séparés par un '/'.")],
        default="", blank=True)
    wikidata = models.CharField(max_length=21, validators=[RegexValidator(
        regex="^Q[0-9]+$",
        message="L'identifiant wikidata doit commencer par la lettre Q suivi d'un nombre.")],
        default="", blank=True)
    wikispecies = models.CharField(max_length=50, default="", blank=True)
    wikipedia = models.CharField(max_length=50, default="", blank=True)
    taxref = models.CharField(max_length=10, validators=[RegexValidator(
        regex="^[0-9]*$",
        message="L'identifiant taxref doit être un nombre entier.")], default="", blank=True)
    history = HistoricalRecords()
