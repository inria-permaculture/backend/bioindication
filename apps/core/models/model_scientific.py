"""
Data models definitions
"""

from django.db import models
from django.db.models.functions import Upper
from django.contrib.postgres.indexes import GinIndex, OpClass
from simple_history.models import HistoricalRecords


class ScientificEntry(models.Model):
    """Scientific model definition"""

    def __str__(self):
        return self.name

    taxon = models.ForeignKey(
        to="Taxon", on_delete=models.CASCADE, related_name="scientific_entries")
    name = models.TextField(db_index=True)
    source = models.CharField(max_length=100, default="", blank=True)
    comment = models.CharField(max_length=150, default="", blank=True)
    history = HistoricalRecords()

    class Meta:
        indexes = [
            GinIndex(
                OpClass(Upper('name'), name='gin_trgm_ops'),
                name='core_sc_name_trgm_gin_idx',
            )
        ]
