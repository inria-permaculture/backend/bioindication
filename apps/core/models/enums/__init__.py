"""
Enum definitions
"""

from core.models.enums.enum_alive import *
from core.models.enums.enum_gender import *
from core.models.enums.enum_language import *
from core.models.enums.enum_lifestage import *
from core.models.enums.enum_quantification import *
from core.models.enums.enum_rank import *
from core.models.enums.enum_spontaneous import *
from core.models.enums.enum_status import *
