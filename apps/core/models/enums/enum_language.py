"""
Enum definition
"""

from django.db import models


class Language(models.TextChoices):
    """Language enumeration"""
    FR = "FR"
