"""
Enum definition
"""

from django.db import models


class Lifestage(models.TextChoices):
    """Lifestage enumeration"""
    ALL = "ALL"
    ZYGOTE = "ZYGOTE"
    EMBRYO = "EMBRYO"
    LARVA = "LARVA"
    JUVENILE = "JUVENILE"
    ADULT = "ADULT"
    SPOROPHYTE = "SPOROPHYTE"
    SPORE = "SPORE"
    GAMETOPHYTE = "GAMETOPHYTE"
    GAMETE = "GAMETE"
    PUPA = "PUPA"
    UNDEFINED = "UNDEFINED"
