"""
Enum definition
"""

from django.db import models


class Status(models.TextChoices):
    """Status enumeration"""
    OPEN = "OPEN"
    CLOSED = "CLOSED"
    ARCHIVED = "ARCHIVED"
