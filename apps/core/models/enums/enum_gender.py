"""
Enum definition
"""

from django.db import models


class Gender(models.TextChoices):
    """Gender enumeration"""
    ALL = "ALL"
    FEMALE = "FEMALE"
    MALE = "MALE"
    UNDEFINED = "UNDEFINED"
