"""
Enum definition
"""

from django.db import models


class Spontaneous(models.TextChoices):
    """Spontaneous enumeration"""
    YES = "YES"
    NO = "NO"
    UNDEFINED = "UNDEFINED"
