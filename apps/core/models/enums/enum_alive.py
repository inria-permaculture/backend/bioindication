"""
Enum definition
"""

from django.db import models


class Alive(models.TextChoices):
    """Alive enumeration"""
    YES = "YES"
    NO = "NO"
    UNDEFINED = "UNDEFINED"
