"""
Enum definition
"""

from django.db import models


class Quantification(models.TextChoices):
    """Quantification enumeration"""
    COUNT = "COUNT"
    COVER = "COVER"
    PRESENCE = "PRESENCE"
