"""
Data models definitions
"""

from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.core.validators import MinValueValidator, MaxValueValidator
from core.models.enums import Status
from simple_history.models import HistoricalRecords


class Inventory(models.Model):
    """Inventory model definition"""

    def __str__(self):
        return (f"{self.id} - {self.name}({self.status}) "
                f"of list : {self.inventory_list.name}")

    inventory_list = models.ForeignKey(
        to="InventoryList", on_delete=models.CASCADE, related_name="inventories")
    name = models.CharField(max_length=50, db_index=True)
    status = models.CharField(default=Status.OPEN,
                              choices=Status.choices, max_length=8)
    description = models.TextField(max_length=250, default="", blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update_date = models.DateTimeField(auto_now=True)
    expiry_date = models.DateTimeField(blank=True, null=True)
    is_publicly_visible = models.BooleanField(
        blank=False, null=False, default=False)
    coordinates = ArrayField(
        ArrayField(
            models.DecimalField(
                max_digits=9, decimal_places=6),
            size=2
        ),
        null=True,
        blank=True
    )
    area = models.DecimalField(blank=True, null=True, max_digits=9, decimal_places=1,
                               default=0,
                               validators=[
                                   MinValueValidator(0),
                                   MaxValueValidator(10000000)]
                               )
    history = HistoricalRecords()
