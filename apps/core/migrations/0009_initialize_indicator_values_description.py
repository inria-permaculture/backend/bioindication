# Created a la mano

from django.db import migrations
from django.core.management import call_command
from core.models import IndicatorValuesDescription


def load_initial_indicator_values_description_data(apps, schema_editor):
    """
    Load indicator values description fixture
    """
    call_command('loaddata', 'indicator_values_description.json')


def unload_indicator_values_description_data(apps, schema_editor):
    """
    Delete existing indicator values description(normally there is only one)
    """
    IndicatorValuesDescription.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20211105_1000'),
    ]

    operations = [
        migrations.RunPython(code=load_initial_indicator_values_description_data,
                             reverse_code=unload_indicator_values_description_data),
    ]
