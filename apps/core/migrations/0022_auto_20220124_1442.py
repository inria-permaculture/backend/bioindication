# Generated by Django 3.2.9 on 2022-01-24 14:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0021_auto_20211221_1209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalobservation',
            name='user_scientific_name',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='historicalobservation',
            name='user_vernacular_name',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='historicalscientificentry',
            name='name',
            field=models.TextField(db_index=True),
        ),
        migrations.AlterField(
            model_name='historicalvernacularentry',
            name='name',
            field=models.TextField(db_index=True),
        ),
        migrations.AlterField(
            model_name='observation',
            name='user_scientific_name',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='observation',
            name='user_vernacular_name',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='scientificentry',
            name='name',
            field=models.TextField(db_index=True),
        ),
        migrations.AlterField(
            model_name='vernacularentry',
            name='name',
            field=models.TextField(db_index=True),
        ),
    ]
