# Bioindication - backend

## Architecture
If you want to understand this repository architecture, here the detailled folder structure :

    admin -> Admin panel models registration folder
    management -> Specifics commands implementations folder for manage.py
    migrations -> Migration folder with instructions to automatically update the database schema based on Django models
    models -> Data models folder
    serializers -> Data transformations folder
    tests -> Unit/Integration tests folder
    utils -> Common functions reusable accross multiple apps
    views -> REST endpoints and related
    __init__.py -> Make this directory considered as a Python package
    urls.py -> The URL declarations for this Django application
