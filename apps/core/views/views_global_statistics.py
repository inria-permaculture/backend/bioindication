"""
Retrieve some global statistics
"""

from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser
from drf_spectacular.utils import (extend_schema, OpenApiResponse)
from core.utils import http_codes_description
from core.models import (Taxon, ScientificEntry,
                         VernacularEntry, InventoryList, Inventory,
                         Observation, PlantUsages)
from core.serializers import GlobalStatisticsSerializer


class GlobalStatisticsView(views.APIView):
    """
    View related to retrieve of global stats
    """
    # set parser to allow parsing HTTP requests with files data inside
    parser_classes = (MultiPartParser,)

    @extend_schema(responses={
        200: OpenApiResponse(response=GlobalStatisticsSerializer,
                             description=http_codes_description.SUCCESS),
    })
    @method_decorator(cache_page(60))
    def get(self, request, *args, **kwargs):
        """
        Retrieve statistics related to inventories app usages
        """
        data = {
            "nb_taxa": Taxon.objects.count(),
            "nb_scientific_names": ScientificEntry.objects.count(),
            "nb_vernacular_names": VernacularEntry.objects.count(),
            "nb_inventory_lists": InventoryList.objects.count(),
            "nb_inventories": Inventory.objects.count(),
            "nb_observations": Observation.objects.count(),
            "nb_plants_observed": Observation.objects.filter(taxon__is_a_plant=True).count(),
            "nb_unique_plants_observed": Observation.objects.filter(taxon__is_a_plant=True)
            .values('taxon').distinct().count(),
            "nb_unique_taxa_observed": Observation.objects.values('taxon').distinct().count(),
            "nb_plant_usages":
                PlantUsages.objects.filter(food=True).count() +
                PlantUsages.objects.filter(honey=True).count() +
                PlantUsages.objects.filter(ornamental=True).count() +
                PlantUsages.objects.filter(medicine=True).count() +
                PlantUsages.objects.filter(fuelwood=True).count() +
                PlantUsages.objects.filter(construction=True).count() +
                PlantUsages.objects.filter(psychotropic=True).count() +
                PlantUsages.objects.filter(poisonous=True).count() +
                PlantUsages.objects.filter(green_manure=True).count() +
                PlantUsages.objects.filter(basketry=True).count() +
                PlantUsages.objects.filter(depolluting=True).count() +
                PlantUsages.objects.filter(textile=True).count() +
                PlantUsages.objects.filter(paper=True).count() +
                PlantUsages.objects.filter(tinctorial=True).count() +
                PlantUsages.objects.filter(fragrance=True).count() +
                PlantUsages.objects.filter(stratum=True).count() +
                PlantUsages.objects.filter(invasive=True).count()
        }
        serializer = GlobalStatisticsSerializer(data)
        return Response(serializer.data, status=status.HTTP_200_OK)
