"""
Generate and save fake data command
"""
from django.core.management.base import BaseCommand
from core.models import InventoryList, Uri, Inventory, Observation, Taxon


class Command(BaseCommand):
    """Manage importing fake data for testing purpose"""
    help = "Import fake data for testing purpose"

    def handle(self, *args, **options):
        # Main command function

        # Create a taxon
        taxon = Taxon.objects.create(id=1)

        # Create an inventory list
        inventory_list = InventoryList.objects.create(
            name="Hello", description="Liste d'inventaire X")

        # Create and add uri to the newly created inventory list
        Uri.objects.create(inventory_list=inventory_list,
                           identifier="shared-inventories")

        # Create and add inventory to the newly created inventory list
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, name="Hello", description="Inventaire du jardin")

        # Create and add observation to newly created inventory
        Observation.objects.create(inventory=inventory, taxon=taxon)

        print("Successfully imported fake data into database")
