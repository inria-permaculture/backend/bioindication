# pylint: disable=too-many-locals
"""
Import taxonomy data to database from a csv file command
Used for kickstart the bioindication project with an external taxonomy
"""
import csv
import time
from django.db import connection
from django.core.management.color import no_style
from django.core.management.base import BaseCommand
from core.models import (Taxon, VernacularEntry,
                         ScientificEntry, Identifiers, PlantUsages)
from core.models.enums import Rank, Language


class Command(BaseCommand):
    """Import taxonomy data from a csv file"""
    help = "Import taxonomy data from a csv file."
    batch_size = 10000
    name_of_parent_of_all_plants = "Viridiplantae"

    @staticmethod
    def __reset_sql_sequences(target_models):
        """
        Sync sql sequences of a list of models
        Mainly used for autoincremented fields(for example: id field)
        """
        sequence_sql = connection.ops.sequence_reset_sql(
            no_style(), target_models)
        with connection.cursor() as cursor:
            for sql in sequence_sql:
                cursor.execute(sql)
        for model in target_models:
            print(
                f"Successfully reset {model._meta.db_table} table sequences")

    def add_arguments(self, parser):
        # Manage argument for specifying csv paths
        parser.add_argument("file_path", nargs="+", type=str)

    def __create_taxa_with_data(self, reader, plants_id_hash):
        # Create taxa & identifiers based on specified csv file
        taxa = []
        translation_dic = {
            "no rank": Rank.UNDEFINED,
            "root": Rank.ROOT,
            "superkingdom": Rank.SUPERKINGDOM,
            "kingdom":  Rank.KINGDOM,
            "subkingdom": Rank.SUBKINGDOM,
            "superphylum": Rank.SUPERPHYLUM,
            "phylum": Rank.PHYLUM,
            "subphylum": Rank.SUBPHYLUM,
            "superclass": Rank.SUPERCLASS,
            "class": Rank.CLASS,
            "subclass": Rank.SUBCLASS,
            "infraclass": Rank.INFRACLASS,
            "cohort": Rank.COHORT,
            "subcohort": Rank.SUBCOHORT,
            "superorder": Rank.SUPERORDER,
            "order": Rank.ORDER,
            "suborder": Rank.SUBORDER,
            "infraorder": Rank.INFRAORDER,
            "superfamily": Rank.SUPERFAMILY,
            "family": Rank.FAMILY,
            "subfamily": Rank.SUBFAMILY,
            "tribe": Rank.TRIBE,
            "subtribe": Rank.SUBTRIBE,
            "genus": Rank.GENUS,
            "subgenus": Rank.SUBGENUS,
            "section": Rank.SECTION,
            "subsection": Rank.SUBSECTION,
            "series": Rank.SERIES,
            "species": Rank.SPECIES,
            "subspecies": Rank.SUBSPECIES,
            "varietas": Rank.VARIETAS,
            "subvariety": Rank.SUBVARIETY,
            "forma": Rank.FORMA,
            "forma specialis": Rank.FORMASPECIALIS,
            "clade": Rank.CLADE,
            "strain": Rank.STRAIN,
            "serogroup": Rank.SEROGROUP,
            "serotype": Rank.SEROTYPE,
            "biotype": Rank.BIOTYPE,
            "genotype": Rank.GENOTYPE,
            "species group": Rank.SPECIESGROUP,
            "species subgroup": Rank.SPECIESSUBGROUP,
            "isolate": Rank.ISOLATE,
            "parvorder": Rank.PARVORDER,
            "morph": Rank.MORPH,
            "pathogroup": Rank.PATHOGROUP,
        }

        print("Loading data from provided taxon file...")
        for row in reader:
            taxa.append(row)
        print("Done")

        nb_taxa = len(taxa)
        batch_size = self.batch_size
        nb_batches = nb_taxa // batch_size + 1
        print(f"{nb_taxa} lines of data divided into {nb_batches} batches")

        print("Populating database with taxa")
        for num_batch in range(nb_batches):
            print(f"\tDealing with batch {num_batch} of {nb_batches}")
            taxon_list = []
            if num_batch < nb_batches - 1:
                ids_in_batch = range(batch_size * num_batch,
                                     batch_size * (num_batch + 1))
            else:
                ids_in_batch = range(batch_size * num_batch, nb_taxa)

            for taxon_id in ids_in_batch:
                taxon_list.append(Taxon(
                    id=taxa[taxon_id]["id"],
                    rank=translation_dic[taxa[taxon_id]["type"]],
                ))

            Taxon.objects.bulk_create(taxon_list)
            print(f"\tSuccessfully imported {len(taxon_list)} taxa")
        print(f"Successfully imported all taxa ({nb_taxa} taxa)")

        print("Adding scientific names, ncbi tax_id and parent-child relations to imported taxa...")
        for num_batch in range(nb_batches):
            print(f"\tDealing with batch {num_batch} of {nb_batches}")
            scientifics = []
            identifiers = []
            taxons_data = []
            plant_usages = []
            if num_batch < nb_batches - 1:
                ids_in_batch = range(batch_size * num_batch,
                                     batch_size * (num_batch + 1))
            else:
                ids_in_batch = range(batch_size * num_batch, nb_taxa)

            for taxon_id in ids_in_batch:
                scientifics.append(ScientificEntry(
                    id=taxa[taxon_id]["id"],
                    taxon_id=taxa[taxon_id]["id"],
                    name=taxa[taxon_id]["scientific_name"],
                    source="https://www.ncbi.nlm.nih.gov/home/download/",
                ))
                identifiers.append(Identifiers(
                    taxon_id=taxa[taxon_id]["id"],
                    ncbi=taxa[taxon_id]["node_id"]
                ))
                is_a_plant = False
                if plants_id_hash[int(taxa[taxon_id]["id"])] == 1:
                    is_a_plant = True
                    plant_usages.append(PlantUsages(
                        taxon_id=taxa[taxon_id]["id"]))
                taxons_data.append(Taxon(
                    id=taxa[taxon_id]["id"],
                    parent_id=taxa[taxon_id]["parent_id"],
                    main_scientific_entry_id=taxa[taxon_id]["id"],
                    is_a_plant=is_a_plant
                ))

            ScientificEntry.objects.bulk_create(scientifics)
            Identifiers.objects.bulk_create(identifiers)
            PlantUsages.objects.bulk_create(plant_usages)
            Taxon.objects.bulk_update(taxons_data, [
                "parent_id",
                "main_scientific_entry_id",
                "is_a_plant"]
            )

    def __create_vernaculars(self, reader):
        # Create vernaculars based on specified csv file
        vernaculars = []
        nb_row = 0
        for row in reader:
            vernaculars.append(VernacularEntry(
                taxon_id=row["id"],
                name=row["name"],
                source="https://github.com/damiendevienne/taxonomy-fr",
                language=Language.FR,
            ))
            nb_row += 1
        VernacularEntry.objects.bulk_create(vernaculars, self.batch_size)
        print(f"Successfully loaded {nb_row} vernaculars")

    def __extract_id_of_all_plants(self, reader):
        """
        Extract the id of all taxa that are plants
        """
        id_of_parent_of_all_plants = -1
        max_id_in_file = -1
        taxa = []

        for row in reader:
            taxon_id = int(row["id"])
            parent_id = int(row["parent_id"])
            taxa.append({"id": taxon_id, "parent_id": parent_id})
            if parent_id > max_id_in_file:
                max_id_in_file = parent_id
            if row["scientific_name"] == self.name_of_parent_of_all_plants:
                id_of_parent_of_all_plants = taxon_id

        if id_of_parent_of_all_plants < 0:
            print(
                f"\t{self.name_of_parent_of_all_plants} not found in the given taxonomy")
            print("\tNo taxa will be flagged as plants in the next step")

        direct_descendants_hash = []
        for _ in range(max_id_in_file+1):
            direct_descendants_hash.append([])

        for taxon in taxa:
            direct_descendants_hash[taxon["parent_id"]].append(taxon["id"])

        nb_plants = 0
        plants_id_hash = [-1] * (max_id_in_file+1)
        plants_to_browse_from = [id_of_parent_of_all_plants]

        still_plants_to_browse_from = True
        while still_plants_to_browse_from:
            plants_found = []
            for plant_id in plants_to_browse_from:
                plants_found.extend(direct_descendants_hash[plant_id])

            for plant_id in plants_to_browse_from:
                plants_id_hash[plant_id] = 1
                nb_plants += 1

            plants_to_browse_from = plants_found

            if len(plants_found) == 0:
                still_plants_to_browse_from = False

        print(f"Successfully flagged {nb_plants} taxa as plants")
        return plants_id_hash

    def handle(self, *args, **options):
        # Main command function

        print("Flagging taxa corresponding to plants")
        t_0 = time.process_time()
        taxons_csv_fieldnames = (
            "id", "parent_id", "scientific_name", "type", "node_id")
        with open(options["file_path"][0], encoding="utf8") as csvfile:
            reader = csv.DictReader(csvfile, taxons_csv_fieldnames)
            reader.__next__()  # skip the csv header
            plants_id_hash = self.__extract_id_of_all_plants(reader)
        t_1 = time.process_time()
        print(f"Plant taxa flagged in {t_1 - t_0:.0f} seconds")

        t_0 = time.process_time()
        taxons_csv_fieldnames = (
            "id", "parent_id", "scientific_name", "type", "node_id")
        with open(options["file_path"][0], encoding="utf8") as csvfile:
            reader = csv.DictReader(csvfile, taxons_csv_fieldnames)
            reader.__next__()  # skip the csv header
            self.__create_taxa_with_data(reader, plants_id_hash)
        t_1 = time.process_time()
        print(f"Taxonomic data imported in {t_1 - t_0:.0f} seconds")

        # reset the ScientificEntry sequence to avoid regenerating existing ids later
        # reset the Taxon sequence too if we allow creating new taxa later
        Command.__reset_sql_sequences([ScientificEntry, Taxon])

        t_0 = time.process_time()
        vernaculars_csv_fieldnames = ("id", "name")
        with open(options["file_path"][1], encoding="utf8") as csvfile:
            reader = csv.DictReader(csvfile, vernaculars_csv_fieldnames)
            reader.__next__()  # skip the csv header
            self.__create_vernaculars(reader)
        t_1 = time.process_time()
        print(f"Vernacular names imported in {t_1 - t_0:.0f} seconds")
