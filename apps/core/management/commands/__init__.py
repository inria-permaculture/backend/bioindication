"""
Django specific commands
"""

from core.management.commands.import_fake_data import *
from core.management.commands.import_taxonomy import *
