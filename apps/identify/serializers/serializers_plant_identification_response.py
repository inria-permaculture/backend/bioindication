# pylint: disable=abstract-method, missing-class-docstring
"""
Plantnet identifications response processing
"""
from rest_framework import serializers
from drf_spectacular.utils import extend_schema_field
from core.models import ScientificEntry


class MatchingTaxonFieldMixin(serializers.Serializer):
    """
    Custom serializer field to add data(id, main scientific name) of the
    first matching taxon in our taxonomy based on the scientific name
    """
    cgb_taxon_id = serializers.SerializerMethodField()
    cgb_taxon_main_scientific_name = serializers.SerializerMethodField()

    @extend_schema_field(field=serializers.IntegerField())
    def get_cgb_taxon_id(self, obj):
        # pylint: disable=invalid-name, no-self-use
        """
        Find and return the id of the first matching taxon
        """
        # get all our scientific names that match the scientific name of plantnet
        scientific_entries = []
        if 'scientific_name_without_author' in obj:
            scientific_entries = ScientificEntry.objects.filter(
                name=obj['scientific_name_without_author'])
        elif 'scientific_name' in obj:
            scientific_entries = ScientificEntry.objects.filter(
                name=obj['scientific_name'])
        if scientific_entries.count() > 0:
            # get the first matching taxon id
            matching_taxon_id = scientific_entries.first().taxon.id
            return matching_taxon_id
        return -1

    @extend_schema_field(field=serializers.CharField())
    def get_cgb_taxon_main_scientific_name(self, obj):
        # pylint: disable=invalid-name, no-self-use
        """
        Find and return the main scientific name of the first matching taxon
        """
        # get all our scientific names that match the scientific name of plantnet
        scientific_entries = []
        if 'scientific_name_without_author' in obj:
            scientific_entries = ScientificEntry.objects.filter(
                name=obj['scientific_name_without_author'])
        elif 'scientific_name' in obj:
            scientific_entries = ScientificEntry.objects.filter(
                name=obj['scientific_name'])
        if scientific_entries.count() > 0:
            # get the first matching taxon main scientific name
            matching_taxon_name = scientific_entries.first().taxon.main_scientific_entry.name
            return matching_taxon_name
        return ""


class PlantIdentificationResponseQuerySerializer(serializers.Serializer):
    project = serializers.CharField()
    images = serializers.ListField(
        child=serializers.CharField(), allow_empty=False
    )
    organs = serializers.ListField(
        child=serializers.CharField(), allow_empty=False
    )


class PlantIdentificationResponseImagesUrlSerializer(serializers.Serializer):
    o = serializers.CharField()
    m = serializers.CharField()
    s = serializers.CharField()


class PlantIdentificationResponseImagesDateSerializer(serializers.Serializer):
    timestamp = serializers.IntegerField()
    string = serializers.CharField()


class PlantIdentificationResponseImagesSerializer(serializers.Serializer):
    organ = serializers.CharField()
    author = serializers.CharField()
    licence = serializers.CharField(required=False)
    date = PlantIdentificationResponseImagesDateSerializer()
    url = PlantIdentificationResponseImagesUrlSerializer()
    citation = serializers.CharField()


class PlantIdentificationResponseFamilySerializer(MatchingTaxonFieldMixin,
                                                  serializers.Serializer):
    scientific_name_without_author = serializers.CharField()
    scientific_name_authorship = serializers.CharField(allow_blank=True)
    scientific_name = serializers.CharField()


class PlantIdentificationResponseGenusSerializer(MatchingTaxonFieldMixin,
                                                 serializers.Serializer):
    scientific_name_without_author = serializers.CharField()
    scientific_name_authorship = serializers.CharField(allow_blank=True)
    scientific_name = serializers.CharField()


class PlantIdentificationResponseSpeciesSerializer(MatchingTaxonFieldMixin,
                                                   serializers.Serializer):
    scientific_name_without_author = serializers.CharField()
    scientific_name_authorship = serializers.CharField()
    scientific_name = serializers.CharField()
    genus = PlantIdentificationResponseGenusSerializer()
    family = PlantIdentificationResponseFamilySerializer()
    common_names = serializers.ListField(
        child=serializers.CharField(), allow_empty=True)


class PlantIdentificationResponseResultsSerializer(serializers.Serializer):
    score = serializers.FloatField()
    species = PlantIdentificationResponseSpeciesSerializer()
    images = serializers.ListField(
        child=PlantIdentificationResponseImagesSerializer(),
        allow_empty=True,
        required=False)


class PlantIdentificationResponseGenusIdentificationSerializer(MatchingTaxonFieldMixin,
                                                               serializers.Serializer):
    score = serializers.FloatField()
    scientific_name = serializers.CharField()


class PlantIdentificationResponseFamilyIdentificationSerializer(MatchingTaxonFieldMixin,
                                                                serializers.Serializer):
    score = serializers.FloatField()
    scientific_name = serializers.CharField()


class PlantIdentificationResponseSerializer(serializers.Serializer):
    family = serializers.ListField(
        child=PlantIdentificationResponseFamilyIdentificationSerializer(), allow_empty=True)
    genus = serializers.ListField(
        child=PlantIdentificationResponseGenusIdentificationSerializer(), allow_empty=True)
    species = serializers.ListField(
        child=PlantIdentificationResponseResultsSerializer(), allow_empty=False)
