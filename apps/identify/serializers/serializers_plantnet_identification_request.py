# pylint: disable=abstract-method, missing-class-docstring
"""
Plant identification request/response processing
"""
from django.conf import settings
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

# Custom validation error messages
LANGUAGE_NOT_ALLOWED = (f"Le language selectionné n'est pas autorisé. Les languages "
                        f"acceptés sont : {settings.PLANTNET_API['ALLOWED_LANGUAGES']}")
IMAGE_FORMAT_NOT_ALLOWED = (f"Le format de l'image n'est pas autorisé. Les formats"
                            f"autorisés sont : {settings.PLANTNET_API['ALLOWED_IMAGE_FORMATS']}")


class PlantIdentificationRequestSerializer(serializers.Serializer):
    """
    Validate client request data and next used to be
    transformed to the plant identification request format
    """
    images = serializers.ListField(
        child=serializers.ImageField(),
        min_length=settings.PLANTNET_API["MIN_NUMBER_OF_IMAGES_OR_ERROR"],
        max_length=settings.PLANTNET_API["MAX_NUMBER_OF_IMAGES_OR_ERROR"],
    )
    include_related_images = serializers.BooleanField(required=True)
    language = serializers.CharField(required=True)

    def validate_language(self, value):
        # pylint: disable=no-self-use
        """
        Field level validation for language value
        Compare language with those defined in settings
        """
        allowed_languages = settings.PLANTNET_API["ALLOWED_LANGUAGES"]
        if value not in allowed_languages:
            raise ValidationError(LANGUAGE_NOT_ALLOWED)
        return value

    def validate_images(self, value):
        # pylint: disable=no-self-use
        """
        Field level validation for images list
        Compare images format with those defined in settings
        """
        allowed_image_formats = settings.PLANTNET_API["ALLOWED_IMAGE_FORMATS"]
        allowed_image_formats_to_lower = [format.lower(
        ) for format in allowed_image_formats]

        for image in value:
            image_format_to_lower = image.content_type.lower()
            image_format_is_allowed = False

            for allowed_image_format in allowed_image_formats_to_lower:
                if allowed_image_format in image_format_to_lower:
                    image_format_is_allowed = True

            if not image_format_is_allowed:
                raise ValidationError(IMAGE_FORMAT_NOT_ALLOWED)

        return value
