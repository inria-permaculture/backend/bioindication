# pylint: disable=missing-module-docstring
import humps


def __get_genus_or_family_from_species_results(taxonomy_rank, identif_response):
    """
    Merge genus or family identification results from species results
    """
    taxonomy_identifications = []
    taxon_identified = {}
    for result in identif_response["species"]:
        scientific_name = result["species"][taxonomy_rank]["scientific_name_without_author"]
        if scientific_name not in taxon_identified:
            taxon_identified[scientific_name] = result["score"]
        else:
            taxon_identified[scientific_name] += result["score"]
    for name, score in taxon_identified.items():
        taxonomy_identifications.append(
            {"scientific_name": name, "score": score})
    return taxonomy_identifications


def transform_plantnet_response(plantnet_json):
    """
    Transform plantnet identification response dic to our own identification dic format
    It transforms camel case plantnet keys in snake case style and
    Embed identification of genus and families
    """
    # convert json keys from camelcase to snakecase
    transformed_data = humps.decamelize(plantnet_json)
    transformed_data["species"] = transformed_data["results"]
    transformed_data["family"] = __get_genus_or_family_from_species_results(
        "family", transformed_data)
    transformed_data["genus"] = __get_genus_or_family_from_species_results(
        "genus", transformed_data)

    return transformed_data
