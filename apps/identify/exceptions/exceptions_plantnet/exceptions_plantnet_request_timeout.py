# pylint: disable=missing-module-docstring
from rest_framework.exceptions import APIException


class PlantnetRequestTimeout(APIException):
    """
    Connection with plantnet is timeout
    """
    status_code = 502
    default_detail = "Plantnet ne répond pas"
    default_code = 'plantnet_response_timeout'
