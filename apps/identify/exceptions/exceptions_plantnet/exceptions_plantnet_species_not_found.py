# pylint: disable=missing-module-docstring
from rest_framework.exceptions import APIException


class PlantnetSpeciesNotFound(APIException):
    """
    Plantnet did not find the requested species
    """

    status_code = 404
    default_detail = "Plantnet n'a pas trouvé d'espèce correspondante"
    default_code = "plantnet_species_not_found"
