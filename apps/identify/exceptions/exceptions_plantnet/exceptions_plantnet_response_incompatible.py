# pylint: disable=missing-module-docstring
from rest_framework.exceptions import APIException


class PlantnetResponseIncompatible(APIException):
    """
    Plantnet response can't be correctly serialized
    """
    status_code = 502
    default_detail = "Le format de réponse de Plantnet n'est pas compatible"
    default_code = "plantnet_response_incompatible"
