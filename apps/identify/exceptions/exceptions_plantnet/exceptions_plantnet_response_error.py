# pylint: disable=missing-module-docstring
from rest_framework.exceptions import APIException


class PlantnetResponseError(APIException):
    """
    Plantnet didn't respond correctly
    """

    def __init__(self, status_code, error, message):
        embed_error_in_detail = (f"{self.default_detail} : error =>{error} "
                                 f"and status code => {status_code} with message => {message}")
        super().__init__(embed_error_in_detail, self.default_code)

    status_code = 502
    default_detail = "Plantnet a renvoyé une erreur"
    default_code = "plantnet_response_error"
