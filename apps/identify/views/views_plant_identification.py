"""
Plants identification by photos
"""
from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser
from rest_framework.exceptions import ValidationError
from drf_spectacular.utils import (extend_schema, OpenApiResponse)
from core.utils import http_codes_description
from identify.serializers import (
    PlantIdentificationRequestSerializer, PlantIdentificationResponseSerializer)
from identify.services.services_plantnet import PlantnetService
from identify.exceptions import PlantnetResponseIncompatible
from identify.transformers import transform_plantnet_response


class PlantIdentificationView(views.APIView):
    """
    View related to plants identification
    """
    # set parser to allow parsing HTTP requests with files data inside
    parser_classes = (MultiPartParser,)

    @extend_schema(request=PlantIdentificationRequestSerializer, responses={
        200: OpenApiResponse(response=PlantIdentificationResponseSerializer,
                             description=http_codes_description.SUCCESS),
        400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
        413: OpenApiResponse(description=http_codes_description.REQUEST_ENTITY_TOO_LARGE),
        502: OpenApiResponse(description=http_codes_description.BAD_GATEWAY),
    })
    def post(self, request, *args, **kwargs):
        # pylint: disable=unused-argument
        """
        Plants identification using the plantnet api.
        Takes 1 to 5 photos of the same plant in jpeg or png and
        returns a list of matching taxa
        in the plantnet taxonomy and in our taxonomy if possible.
        """
        plantnet_service = PlantnetService()

        input_serializer = PlantIdentificationRequestSerializer(
            data=self.request.data)
        # validate input, raised validation errors autogenerate a client responses
        input_serializer.is_valid(raise_exception=True)

        # construct the plantnet request body based on serialized data
        language = input_serializer.validated_data["language"]
        include_related_images = input_serializer.validated_data["include_related_images"]
        data = []
        files = []
        for image in input_serializer.validated_data["images"]:
            # Add organ to data dictionnary
            data.append(
                ("organs", plantnet_service.DEFAULT_ORGAN_TAG))
            # Add images to files dictionnary
            files.append(
                ("images", (image.name, image, image.content_type)))

        # make an identification request
        response_data = plantnet_service.post_identification(
            language, include_related_images, data, files)

        try:
            # transform the json to our own format
            transformed_response_data = transform_plantnet_response(
                response_data)
        except KeyError as error:
            raise PlantnetResponseIncompatible() from error

        # validate the transformed response and embed specific fields
        serializer = PlantIdentificationResponseSerializer(
            data=transformed_response_data)

        try:
            serializer.is_valid(raise_exception=True)
        # handle plantnet response transformation and serialization error and inform client
        except ValidationError as error:
            raise PlantnetResponseIncompatible() from error

        return Response(serializer.data, status=status.HTTP_200_OK)
