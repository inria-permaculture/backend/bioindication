"""
API endpoints routes definitions
"""
from django.conf.urls import url
from identify.views import PlantIdentificationView

urlpatterns = [
    url(r'^plants/$',
        PlantIdentificationView.as_view(), name="plants-identification")
]
