# pylint: disable=missing-module-docstring
from unittest.mock import Mock, patch
from requests import Timeout
from rest_framework.test import APITestCase
from rest_framework import status
from identify.services import PlantnetService
from identify.exceptions.exceptions_plantnet import (
    PlantnetRequestTimeout, PlantnetResponseError, PlantnetSpeciesNotFound)


class TestPlantnetServiceTestCase(APITestCase):
    """
    Test the plantnet service
    """

    def setUp(self):
        """Setup test class data"""
        self.plantnet_service = PlantnetService()

    @patch('requests.post')
    def test_post_identify_known_species_returns_json(self, mocked_post):
        """
        Test identifying a plant known by plantnet must return json
        """
        mocked_json_response = {"test": "yes"}
        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: mocked_json_response)

        plantnet_json_response = self.plantnet_service.post_identification(
            "fr", True, {}, {})

        self.assertEqual(plantnet_json_response, mocked_json_response)

    @patch('requests.post')
    def test_post_identify_unknown_species_returns_not_found_error(self, mocked_post):
        """
        Test identifying a plant not known by plantnet must return a 404 not found error
        """
        mocked_post.return_value = Mock(
            status_code=status.HTTP_404_NOT_FOUND, json=lambda: {})

        with self.assertRaises(PlantnetSpeciesNotFound):
            self.plantnet_service.post_identification("fr", True, {}, {})

    @patch('requests.post')
    def test_post_identify_taking_too_much_time_to_respond_returns_timeout_error(self, mocked_post):
        """
        Test identifying with plantnet taking too much time to respond must return a timeout error
        """
        def post_timeout_side_effect(*args, **kwargs):
            raise Timeout()

        mocked_post.side_effect = post_timeout_side_effect

        with self.assertRaises(PlantnetRequestTimeout):
            self.plantnet_service.post_identification("fr", True, {}, {})

    @patch('requests.post')
    def test_post_identify_throwing_error_returns_plantnet_response_error(self, mocked_post):
        """
        Test identifying with plantnet throwing an error must
        return a plantnet response error
        """
        plantnet_error_response = {
            "statusCode": "coucou", "error": "coucac", "message": "coucic"}
        mocked_post.return_value = Mock(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, json=lambda: plantnet_error_response)

        with self.assertRaises(PlantnetResponseError):
            self.plantnet_service.post_identification("fr", True, {}, {})
