# pylint: disable=missing-module-docstring
from io import BytesIO
from unittest.mock import Mock, patch
from PIL import Image
from requests import Timeout
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import Taxon, ScientificEntry


def create_image(image_format):
    """
    Create an in memory image with the specified format
    """
    file_stream = BytesIO()
    image = Image.new("RGB", (1, 1))
    image.save(file_stream, image_format)
    file_stream.seek(0)
    dummy_uploaded_image = SimpleUploadedFile(
        "monimage.png", content=file_stream.read(), content_type=image_format)
    return dummy_uploaded_image


def create_multiple_images(nb_images, image_format):
    """
    Create a list of images with the specified format
    """
    images = []
    for _ in range(nb_images):
        images.append(create_image(image_format))
    return images


class TestPlantnetIdentificationApiViewTestCase(APITestCase):
    """
    Test the plantnet identification view
    """

    def setUp(self):
        """Setup test class data"""
        valid_image_format = settings.PLANTNET_API["ALLOWED_IMAGE_FORMATS"][2]
        self.dummy_image = create_image(valid_image_format)
        self.valid_language = settings.PLANTNET_API["ALLOWED_LANGUAGES"][0]
        # template of the compatible plantnet response format
        self.dummy_plantnet_response = {"query": {
            "project": "string",
            "images": [
                "string"
            ],
            "organs": [
                "string"
            ]
        },
            "language": "string",
            "prefered_referential": "string",
            "remaining_identification_requests": 0,
            "results": [
            {
                "score": 0,
                "species": {
                    "scientific_name_without_author": "string",
                    "scientific_name_authorship": "string",
                    "scientific_name": "string",
                    "genus": {
                        "scientific_name_without_author": "string",
                        "scientific_name_authorship": "string",
                        "scientific_name": "string"
                    },
                    "family": {
                        "scientific_name_without_author": "string",
                        "scientific_name_authorship": "string",
                        "scientific_name": "string"
                    },
                    "common_names": [
                        "string"
                    ]
                },
                "images": [
                    {
                        "organ": "string",
                        "author": "string",
                        "license": "string",
                        "date": {
                            "timestamp": 0,
                            "string": "string"
                        },
                        "citation": "string",
                        "url": {
                            "o": "string",
                            "m": "string",
                            "s": "string"
                        }
                    }
                ],
                "gbif": {
                    "id": 0
                }
            }
        ]
        }

    @patch('requests.post')
    def test_identify_a_plantnet_identified_species_returns_results(self, mocked_post):
        """
        Test identifying a plant known by plantnet must return results
        """
        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart").json()

        self.assertIn("species", response)

    @patch('requests.post')
    def test_identify_a_plantnet_identified_species_returns_http_code_200(self, mocked_post):
        """
        Test identifying a plant known by plantnet must return a http code 200
        """
        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('requests.post')
    def test_identify_a_plantnet_identified_species_returns_valid_data(self, mocked_post):
        """
        Test identifying a plant known by plantnet must return valid data
        """
        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart").json()

        # species
        self.assertEqual(response["species"][0]["score"],
                         self.dummy_plantnet_response["results"][0]["score"])
        out_species_subdic = response["species"][0]["species"]
        in_species_subdic = self.dummy_plantnet_response["results"][0]["species"]
        self.assertTrue("cgb_taxon_id" in out_species_subdic)
        self.assertTrue(
            "cgb_taxon_main_scientific_name" in out_species_subdic)
        self.assertEqual(out_species_subdic["scientific_name_without_author"],
                         in_species_subdic["scientific_name_without_author"])
        self.assertEqual(out_species_subdic["scientific_name_authorship"],
                         in_species_subdic["scientific_name_authorship"])
        self.assertEqual(
            out_species_subdic["scientific_name"], in_species_subdic["scientific_name"])
        self.assertEqual(
            out_species_subdic["common_names"], in_species_subdic["common_names"])
        # results -> species -> genus
        self.assertTrue("cgb_taxon_id" in out_species_subdic["genus"])
        self.assertTrue(
            "cgb_taxon_main_scientific_name" in out_species_subdic["genus"])
        self.assertEqual(out_species_subdic["genus"]["scientific_name_without_author"],
                         in_species_subdic["genus"]["scientific_name_without_author"])
        self.assertEqual(out_species_subdic["genus"]["scientific_name_authorship"],
                         in_species_subdic["genus"]["scientific_name_authorship"])
        self.assertEqual(out_species_subdic["genus"]["scientific_name"],
                         in_species_subdic["genus"]["scientific_name"])
        # results -> species -> family
        self.assertTrue("cgb_taxon_id" in out_species_subdic["family"])
        self.assertTrue(
            "cgb_taxon_main_scientific_name" in out_species_subdic["family"])
        self.assertEqual(out_species_subdic["family"]["scientific_name_without_author"],
                         in_species_subdic["family"]["scientific_name_without_author"])
        self.assertEqual(out_species_subdic["family"]["scientific_name_authorship"],
                         in_species_subdic["family"]["scientific_name_authorship"])
        self.assertEqual(out_species_subdic["family"]["scientific_name"],
                         in_species_subdic["family"]["scientific_name"])
        # results -> images
        self.assertEqual(response["species"][0]["images"][0]["organ"],
                         self.dummy_plantnet_response["results"][0]["images"][0]["organ"])
        self.assertEqual(response["species"][0]["images"][0]["author"],
                         self.dummy_plantnet_response["results"][0]["images"][0]["author"])
        self.assertEqual(response["species"][0]["images"][0]["date"],
                         self.dummy_plantnet_response["results"][0]["images"][0]["date"])
        self.assertEqual(response["species"][0]["images"][0]["url"],
                         self.dummy_plantnet_response["results"][0]["images"][0]["url"])
        self.assertEqual(response["species"][0]["images"][0]["citation"],
                         self.dummy_plantnet_response["results"][0]["images"][0]["citation"])

    @patch('requests.post')
    def test_identify_with_invalid_plantnet_response_returns_http_code_502(self, mocked_post):
        """
        Test identifying with plantnet giving an invalid response must
        return a http code 502
        """
        incompatible_dummy_plantnet_response = {
            "newKey": "value", "newKey2": "value"}
        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: incompatible_dummy_plantnet_response)
        request_data = {"images": [self.dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(reverse.reverse('plants-identification'),
                                    data=request_data,
                                    format="multipart")

        self.assertEqual(response.status_code, status.HTTP_502_BAD_GATEWAY)

    @patch('requests.post')
    def test_identify_an_uknown_species_returns_http_code_404(self, mocked_post):
        """
        Test identifying an unknown species must return a http code 404
        """
        mocked_post.return_value = Mock(
            status_code=status.HTTP_404_NOT_FOUND, json=lambda: {})
        request_data = {"images": [self.dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(reverse.reverse('plants-identification'),
                                    data=request_data,
                                    format="multipart")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @patch('requests.post')
    def test_identify_plantnet_endpoint_timeout_returns_http_code_502(self, mocked_post):
        """
        Test identifying with plantnet endpoint not responding in time
        must return a http code 502
        """
        def post_timeout_side_effect(*args, **kwargs):
            raise Timeout()

        mocked_post.side_effect = post_timeout_side_effect
        request_data = {"images": [self.dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(reverse.reverse('plants-identification'),
                                    data=request_data,
                                    format="multipart")

        self.assertEqual(response.status_code, status.HTTP_502_BAD_GATEWAY)

    @patch('requests.post')
    def test_identify_plantnet_throwing_error_returns_http_code_502(self, mocked_post):
        """
        Test identifying with plantnet endpoint returning an error must
        return a http code 502
        """
        plantnet_error_response = {
            "statusCode": "coucou", "error": "coucac", "message": "coucic"}
        mocked_post.return_value = Mock(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, json=lambda: plantnet_error_response)
        request_data = {"images": [self.dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(reverse.reverse('plants-identification'),
                                    data=request_data,
                                    format="multipart")

        self.assertEqual(response.status_code, status.HTTP_502_BAD_GATEWAY)


class TestPlantnetIdentificationApiViewInputsTestCase(APITestCase):
    """
    Test the plantnet identification view parameters behaviours
    """

    def setUp(self):
        """Setup test class data"""
        self.valid_image_format = settings.PLANTNET_API["ALLOWED_IMAGE_FORMATS"][2]
        self.valid_dummy_image = create_image(self.valid_image_format)

        not_accepted_image_format = "gif"
        self.invalid_dummy_image = create_image(not_accepted_image_format)

        self.valid_language = settings.PLANTNET_API["ALLOWED_LANGUAGES"][0]

        # template of the compatible plantnet response format
        # scientific_name_without_author is populated with the name "Mentha" to allow
        # testing the correspondence with our taxonomy
        self.matching_names = {"species": "Mentha Piperita",
                               "genus": "Mentha",
                               "family": "Menthae"}
        self.dummy_plantnet_response = {"query": {
            "project": "string",
            "images": [
                "string"
            ],
            "organs": [
                "string"
            ]
        },
            "language": "string",
            "preferedReferential": "string",
            "switchToProject": "string",
            "remainingIdentificationRequests": 0,
            "results": [
            {
                "score": 0,
                "species": {
                    "scientific_name_without_author": self.matching_names["species"],
                    "scientific_name_authorship": "string",
                    "scientific_name": "string",
                    "genus": {
                        "scientific_name_without_author": self.matching_names["genus"],
                        "scientific_name_authorship": "string",
                        "scientific_name": "string"
                    },
                    "family": {
                        "scientific_name_without_author": self.matching_names["family"],
                        "scientific_name_authorship": "string",
                        "scientific_name": "string"
                    },
                    "common_names": [
                        "string"
                    ]
                },
                "images": [
                    {
                        "organ": "string",
                        "author": "string",
                        "license": "string",
                        "date": {
                            "timestamp": 0,
                            "string": "string"
                        },
                        "citation": "string",
                        "url": {
                            "o": "string",
                            "m": "string",
                            "s": "string"
                        }
                    }
                ],
                "gbif": {
                    "id": 0
                }
            }
        ]
        }

    @patch('requests.post')
    def test_identify_with_allowed_image_returns_http_code_200(self, mocked_post):
        """
        Test identifying with one image in an allowed image format must return http code 200
        """
        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.valid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('requests.post')
    def test_identify_with_allowed_images_returns_http_code_200(self, mocked_post):
        """
        Test identifying with images in allowed image formats must return http code 200
        """
        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        another_valid_dummy_image = create_image(self.valid_image_format)
        request_data = {"images": [self.valid_dummy_image, another_valid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_identify_with_not_allowed_image_format_returns_http_code_400(self):
        """
        Test identifying with image in a non allowed image format must return http code 400
        """
        request_data = {"images": [self.invalid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_identify_with_not_only_allowed_images_returns_http_code_400(self):
        """
        Test identifying with multiple images and at least one in a non allowed image format
        must return http code 400
        """
        request_data = {"images": [self.invalid_dummy_image, self.valid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @patch('requests.post')
    def test_identify_with_allowed_language_returns_http_status_200(self, mocked_post):
        """
        Test identifying with an allowed language must return an http code 200
        """
        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.valid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_identify_with_non_allowed_language_returns_http_status_400(self):
        """
        Test identifying with a non allowed language must return an http code 400
        """
        invalid_language = "fgr"
        request_data = {"images": [self.valid_dummy_image],
                        "include_related_images": True,
                        "language": invalid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @patch('requests.post')
    def test_identify_with_min_allowed_images_returns_http_code_200(self, mocked_post):
        """
        Test identifying with the minimum allowed images must return http code 200
        """
        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        images = create_multiple_images(
            settings.PLANTNET_API["MIN_NUMBER_OF_IMAGES_OR_ERROR"], self.valid_image_format)
        request_data = {"images": images,
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('requests.post')
    def test_identify_with_max_allowed_images_returns_http_code_200(self, mocked_post):
        """
        Test identifying with the max allowed images must return http code 200
        """
        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        images = create_multiple_images(
            settings.PLANTNET_API["MAX_NUMBER_OF_IMAGES_OR_ERROR"], self.valid_image_format)
        request_data = {"images": images,
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_identify_with_less_than_min_allowed_images_returns_http_code_400(self):
        """
        Test identifying with less than the minimum of allowed images must return http code 400
        """
        images = create_multiple_images(
            settings.PLANTNET_API["MIN_NUMBER_OF_IMAGES_OR_ERROR"]-1,
            self.valid_image_format)
        request_data = {"images": images,
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_identify_with_more_than_max_allowed_images_returns_http_code_400(self):
        """
        Test identifying with more than the maximum of allowed images must return http code 400
        """
        images = create_multiple_images(
            settings.PLANTNET_API["MAX_NUMBER_OF_IMAGES_OR_ERROR"]+1,
            self.valid_image_format)
        request_data = {"images": images,
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @patch('requests.post')
    def test_identify_unmatching_taxon_returns_empty_related_taxon_id(self, mocked_post):
        """
        Test identifying with plantnet returning a name
        not related to taxons in our database must return empty taxon id list in response
        """
        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.valid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart").json()

        self.assertEqual(response["species"][0]["species"]["cgb_taxon_id"], -1)
        self.assertEqual(response["species"][0]
                         ["species"]["genus"]["cgb_taxon_id"], -1)
        self.assertEqual(response["species"][0]
                         ["species"]["family"]["cgb_taxon_id"], -1)

    @patch('requests.post')
    def test_identify_unmatching_taxon_returns_empty_related_taxon_name(self, mocked_post):
        """
        Test identifying with plantnet returning a name
        not related to taxons in our database must return empty taxon name list in response
        """
        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.valid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart").json()

        self.assertEqual(response["species"][0]["species"]
                         ["cgb_taxon_main_scientific_name"], "")
        self.assertEqual(response["species"][0]["species"]
                         ["genus"]["cgb_taxon_main_scientific_name"], "")
        self.assertEqual(response["species"][0]["species"]
                         ["family"]["cgb_taxon_main_scientific_name"], "")

    @patch('requests.post')
    def test_identify_matching_species_returns_embed_related_taxon_id(self, mocked_post):
        """
        Test identifying with plantnet returning a species name
        related to taxons in our database must add related taxon id in response
        """
        ScientificEntry.objects.create(
            id=1,
            taxon_id=10,
            name=self.matching_names["species"]
        )
        taxon = Taxon.objects.create(id=10, main_scientific_entry_id=1)

        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.valid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart").json()

        self.assertEqual(response["species"][0]
                         ["species"]["cgb_taxon_id"], taxon.id)

    @patch('requests.post')
    def test_identify_matching_genus_returns_embed_related_taxon_id(self, mocked_post):
        """
        Test identifying with plantnet returning a genus name
        related to taxons in our database must add related taxon id in response
        """
        ScientificEntry.objects.create(
            id=1,
            taxon_id=10,
            name=self.matching_names["genus"]
        )
        taxon = Taxon.objects.create(id=10, main_scientific_entry_id=1)

        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.valid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart").json()

        self.assertEqual(response["species"][0]
                         ["species"]["genus"]["cgb_taxon_id"], taxon.id)

    @patch('requests.post')
    def test_identify_matching_family_returns_embed_related_taxon_id(self, mocked_post):
        """
        Test identifying with plantnet returning a family name
        related to taxons in our database must add related taxon id in response
        """
        ScientificEntry.objects.create(
            id=1,
            taxon_id=10,
            name=self.matching_names["family"]
        )
        taxon = Taxon.objects.create(id=10, main_scientific_entry_id=1)

        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.valid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart").json()

        self.assertEqual(response["species"][0]
                         ["species"]["family"]["cgb_taxon_id"], taxon.id)

    @patch('requests.post')
    def test_identify_matching_species_returns_embed_related_taxon_name(
            self,
            mocked_post):
        """
        Test identifying with plantnet returning a species name
        related to taxons in our database must add related taxon name in response
        """
        ScientificEntry.objects.create(
            id=1,
            taxon_id=10,
            name=self.matching_names["species"]
        )
        species_taxon = Taxon.objects.create(
            id=10, main_scientific_entry_id=1)

        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.valid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart").json()

        self.assertEqual(response["species"][0]
                         ["species"]["cgb_taxon_main_scientific_name"],
                         species_taxon.main_scientific_entry.name)

    @patch('requests.post')
    def test_identify_matching_genus_returns_embed_related_taxon_name(
            self,
            mocked_post):
        """
        Test identifying with plantnet returning a genus name
        related to taxons in our database must add related taxon name in response
        """
        ScientificEntry.objects.create(
            id=1,
            taxon_id=10,
            name=self.matching_names["genus"]
        )
        genus_taxon = Taxon.objects.create(
            id=10, main_scientific_entry_id=1)

        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.valid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart").json()

        self.assertEqual(response["species"][0]["species"]
                         ["genus"]["cgb_taxon_main_scientific_name"],
                         genus_taxon.main_scientific_entry.name)

    @patch('requests.post')
    def test_identify_matching_family_returns_embed_related_taxon_name(
            self,
            mocked_post):
        """
        Test identifying with plantnet returning a family name
        related to taxons in our database must add related taxon name in response
        """
        ScientificEntry.objects.create(
            id=1,
            taxon_id=10,
            name=self.matching_names["family"]
        )
        family_taxon = Taxon.objects.create(
            id=10, main_scientific_entry_id=1)

        mocked_post.return_value = Mock(
            status_code=status.HTTP_200_OK, json=lambda: self.dummy_plantnet_response)
        request_data = {"images": [self.valid_dummy_image],
                        "include_related_images": True,
                        "language": self.valid_language}

        response = self.client.post(
            reverse.reverse('plants-identification'),
            data=request_data,
            format="multipart").json()

        self.assertEqual(response["species"][0]["species"]
                         ["family"]["cgb_taxon_main_scientific_name"],
                         family_taxon.main_scientific_entry.name)
