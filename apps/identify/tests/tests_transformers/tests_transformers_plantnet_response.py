# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from identify.transformers import transform_plantnet_response


class TestTransformerPlantnetResponseToIdentificationResponse(APITestCase):
    """
    Test the dictionnary transformation between plantnet response and our
    identification view response
    """

    def test_transform_dic_with_camel_case_keys_transforms_to_snake_case_keys(self):
        """
        Test transforming a dictionnary with camel case keys must
        return dic with snake case keys
        """
        json_with_camel_case_keys = {"results": [], "oLaLa": 5, "aLo": 2, }

        transformed_json = transform_plantnet_response(
            json_with_camel_case_keys)

        keys = transformed_json.keys()
        self.assertIn("species", keys)
        self.assertIn("o_la_la", keys)
        self.assertIn("a_lo", keys)
