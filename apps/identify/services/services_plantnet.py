# pylint: disable=missing-module-docstring,too-few-public-methods

import json
import requests
from django.conf import settings
from identify.exceptions.exceptions_plantnet import (
    PlantnetRequestTimeout, PlantnetResponseError, PlantnetSpeciesNotFound)


class PlantnetService():
    """
    Wrapper of the plantnet api endpoints
    """

    # plantnet request configuration
    BASE_PLANTNET_IDENTIFY_URL = settings.PLANTNET_API["BASE_PLANTNET_IDENTIFY_URL"]
    SECONDS_BEFORE_TIMEOUT = settings.PLANTNET_API["SECONDS_BEFORE_TIMEOUT"]
    API_KEY = settings.PLANTNET_API["KEY"]
    # plantnet will made it useless soon
    DEFAULT_ORGAN_TAG = settings.PLANTNET_API["DEFAULT_ORGAN_TAG"]
    # target project to make identification
    DEFAULT_PROJECT = settings.PLANTNET_API["DEFAULT_PROJECT"]

    def _get_plantnet_post_url(self,
                               include_related_images,
                               language):
        """
        Get the identification request post url with params
        """
        # Construct path
        path = f"{self.BASE_PLANTNET_IDENTIFY_URL}/{self.DEFAULT_PROJECT}"

        # Construct query param
        params = (f"?api-key={self.API_KEY}&"
                  f"include-related-images={json.dumps(include_related_images)}&lang={language}")

        # Return the complete url
        return path+params

    def post_identification(self, language, include_related_images, data, files):
        """
        Wrapper of the plantnet post identification endpoint
        """
        url_to_request = self._get_plantnet_post_url(
            include_related_images, language)

        # try sending the request to plantnet
        try:
            response = requests.post(
                url_to_request, data=data, files=files, timeout=self.SECONDS_BEFORE_TIMEOUT)
        # handle request timeout and inform client
        except requests.Timeout as error:
            raise PlantnetRequestTimeout() from error

        plantnet_response = response.json()

        # check and return the response if the request succeed
        if response.status_code == 200:
            return plantnet_response

        if response.status_code == 404:
            raise PlantnetSpeciesNotFound()

        # handle status code not ok and encapsulate error to the client response
        raise PlantnetResponseError(
            status_code=plantnet_response["statusCode"],
            error=plantnet_response["error"],
            message=plantnet_response["message"])
