# Bioindication - backend

## Architecture
If you want to understand this repository architecture, here the detailled folder structure :

    exceptions -> Custom exceptions
    serializers -> Data transformations folder
    services -> Wrapper for external api mapping
    tests -> Unit/Integration tests folder
    transformers -> JSON to JSON mapping
    views -> REST endpoints and related
    __init__.py -> Make this directory considered as a Python package
    urls.py -> The URL declarations for this Django application
