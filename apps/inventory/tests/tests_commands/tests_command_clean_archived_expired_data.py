# pylint: disable=missing-module-docstring
from datetime import timedelta
from django.utils import timezone
from django.test import TestCase
from django.core.management import call_command
from core.models import InventoryList, Inventory, Status


class CleanArchivedExpiredDataCommandTestCase(TestCase):
    """
    Test the clean_archived_expired_data command
    """

    def test_exec_command_with_an_expired_and_archived_inventory_list_remove_it(self):
        """
        Test calling the command with an expired/archived inventory_list must remove it
        """
        InventoryList.objects.create(
            **{"id": 1, "name": "aRandomName",
               "expiry_date": timezone.now()-timedelta(days=10),
               "status": Status.ARCHIVED})
        call_command("clean_archived_expired_data")
        self.assertEqual(InventoryList.objects.all().count(), 0)

    def test_exec_command_with_an_expired_and_archived_inventory_remove_it(self):
        """
        Test calling the command with an expired/archived inventory must remove it
        """
        parent_inventory_list = InventoryList.objects.create(
            **{"id": 1, "name": "aRandomName",
               "expiry_date": timezone.now()-timedelta(days=10),
               "status": Status.ARCHIVED})
        Inventory.objects.create(
            **{"id": 1, "inventory_list": parent_inventory_list, "name": "aRandomName",
               "expiry_date": timezone.now()-timedelta(days=10),
               "status": Status.ARCHIVED})
        call_command("clean_archived_expired_data")
        self.assertEqual(Inventory.objects.all().count(), 0)

    def test_exec_command_with_an_archived_but_not_expired_inventory_list_doesnt_remove_it(self):
        """
        Test calling the command with an archived but not expired inventory list mustnt remove it
        """
        InventoryList.objects.create(
            **{"id": 1, "name": "aRandomName",
               "expiry_date": timezone.now()+timedelta(days=10),
               "status": Status.ARCHIVED})
        call_command("clean_archived_expired_data")
        self.assertEqual(InventoryList.objects.all().count(), 1)

    def test_exec_command_with_an_expired_but_not_archived_inventory_list_doesnt_remove_it(self):
        """
        Test calling the command with an expired but not archived inventory list mustnt remove it
        """
        InventoryList.objects.create(
            **{"id": 1, "name": "aRandomName",
               "expiry_date": timezone.now()-timedelta(days=10),
               "status": Status.OPEN})
        call_command("clean_archived_expired_data")
        self.assertEqual(InventoryList.objects.all().count(), 1)

    def test_exec_command_with_an_archived_but_not_expired_inventory_doesnt_remove_it(self):
        """
        Test calling the command with an archived but not expired inventory mustnt remove it
        """
        parent_inventory_list = InventoryList.objects.create(
            **{"id": 1, "name": "aRandomName",
               "expiry_date": timezone.now()+timedelta(days=10),
               "status": Status.OPEN})
        Inventory.objects.create(
            **{"id": 1, "inventory_list": parent_inventory_list, "name": "aRandomName",
               "expiry_date": timezone.now()+timedelta(days=10),
               "status": Status.ARCHIVED})
        call_command("clean_archived_expired_data")
        self.assertEqual(Inventory.objects.all().count(), 1)

    def test_exec_command_with_an_expired_but_not_archived_inventory_doesnt_remove_it(self):
        """
        Test calling the command with an expired but not archived inventory mustnt remove it
        """
        parent_inventory_list = InventoryList.objects.create(
            **{"id": 1, "name": "aRandomName",
               "expiry_date": timezone.now()-timedelta(days=10),
               "status": Status.OPEN})
        Inventory.objects.create(
            **{"id": 1, "inventory_list": parent_inventory_list, "name": "aRandomName",
               "expiry_date": timezone.now()-timedelta(days=10),
               "status": Status.OPEN})
        call_command("clean_archived_expired_data")
        self.assertEqual(Inventory.objects.all().count(), 1)
