"""
Test classes
"""

from inventory.tests.tests_commands import *
from inventory.tests.tests_serializers import *
from inventory.tests.tests_viewsets import *
