# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import InventoryList, Inventory
from core.utils import are_datetime_and_json_date_equal


class RetrievePubliclyVisibleInventoriesViewsetTestCase(APITestCase):
    """
    Test the retrieve publicly visible inventories view
    """

    def test_get_inventories_without_visible_inventories_returns_http_code_200(self):
        """
        Test get inventories withtout publicly visible inventories
        must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        Inventory.objects.create(
            inventory_list=inventory_list, is_publicly_visible=False)

        response = self.client.get(
            reverse.reverse(
                "inventory-public"),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_inventories_without_visible_inventories_returns_empty_data(self):
        """
        Test get inventories withtout publicly visible inventories
        must return empty data
        """
        inventory_list = InventoryList.objects.create()
        Inventory.objects.create(
            inventory_list=inventory_list, is_publicly_visible=False)

        response = self.client.get(
            reverse.reverse(
                "inventory-public"),
            format="json")

        self.assertEqual(response.data, [])

    def test_get_inventories_with_visible_inventories_returns_visible_inventories(self):
        """
        Test get inventories with publicly visible inventories
        must return list of inventories
        """
        inventory_list = InventoryList.objects.create()
        Inventory.objects.create(
            inventory_list=inventory_list, is_publicly_visible=True)
        Inventory.objects.create(
            inventory_list=inventory_list, is_publicly_visible=True)
        Inventory.objects.create(
            inventory_list=inventory_list, is_publicly_visible=True)
        Inventory.objects.create(
            inventory_list=inventory_list, is_publicly_visible=False)

        response = self.client.get(
            reverse.reverse(
                "inventory-public"),
            format="json")

        self.assertEqual(len(response.data), 3)

    def test_get_inventories_with_visible_inventories_returns_valid_data(self):
        """
        Test get inventories with publicly visible inventories
        must return valid data
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, is_publicly_visible=True)

        response = self.client.get(
            reverse.reverse(
                "inventory-public"),
            format="json")

        self.assertEqual(len(response.data), 1)
        self.assertEqual(inventory_list.id,
                         response.data[0]["inventory_list"])
        self.assertEqual(inventory.id,
                         response.data[0]["id"])
        self.assertEqual(inventory.name,
                         response.data[0]["name"])
        self.assertEqual(inventory.description,
                         response.data[0]["description"])
        self.assertEqual(inventory.status,
                         response.data[0]["status"])
        self.assertEqual(inventory.is_publicly_visible,
                         response.data[0]["is_publicly_visible"])
        self.assertTrue(are_datetime_and_json_date_equal(inventory.creation_date,
                                                         response.data[0]["creation_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(inventory.last_update_date,
                                                         response.data[0]["last_update_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(inventory.expiry_date,
                                                         response.data[0]["expiry_date"]))
