# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import InventoryList, Inventory
from core.utils import are_datetime_and_json_date_equal


class RetrieveInventoryViewsetTestCase(APITestCase):
    """
    Test the inventory retrieve view
    """

    def test_get_an_existing_inventory_returns_http_code_200(self):
        """
        Test get an existing inventory must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(inventory_list=inventory_list)

        response = self.client.get(
            reverse.reverse(
                "inventory-detail", args=[inventory.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_an_existing_inventory_returns_valid_data(self):
        """
        Test get an existing inventory must return all data
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(inventory_list=inventory_list)

        response = self.client.get(
            reverse.reverse(
                "inventory-detail", args=[inventory.id]),
            format="json")

        self.assertEqual(inventory_list.id,
                         response.data["inventory_list"])
        self.assertEqual(inventory.id,
                         response.data["id"])
        self.assertEqual(inventory.name,
                         response.data["name"])
        self.assertEqual(inventory.description,
                         response.data["description"])
        self.assertEqual(inventory.status,
                         response.data["status"])
        self.assertEqual(inventory.is_publicly_visible,
                         response.data["is_publicly_visible"])
        self.assertEqual(inventory.coordinates,
                         response.data["coordinates"])
        self.assertEqual(inventory.area,
                         response.data["area"])
        self.assertTrue(are_datetime_and_json_date_equal(inventory.creation_date,
                                                         response.data["creation_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(inventory.last_update_date,
                                                         response.data["last_update_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(inventory.expiry_date,
                                                         response.data["expiry_date"]))

    def test_get_a_non_existing_inventory_returns_http_code_404(self):
        """
        Test get a non existing inventory must return a http code 404
        """
        non_existing_inventory_id = 999

        response = self.client.get(
            reverse.reverse(
                "inventory-detail", args=[non_existing_inventory_id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
