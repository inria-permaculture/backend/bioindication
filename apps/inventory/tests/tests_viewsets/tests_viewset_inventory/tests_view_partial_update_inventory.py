# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import InventoryList, Inventory
from core.models.enums.enum_status import Status
from core.utils import are_datetime_and_json_date_equal


class PartialUpdateInventoryViewsetTestCase(APITestCase):
    """
    Test the partial update inventory view
    """

    def test_partially_update_with_valid_data_returns_http_code_200(self):
        """
        Test partially update an inventory with valid data must return a http code 200
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
            "is_publicly_visible": True,
            "coordinates": [[10.300000, 20.600000]],
            "area": 50
        }
        inventory_list = InventoryList.objects.create()
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list)

        response = self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partially_update_with_valid_data_returns_valid_data(self):
        """
        Test partially update an inventory with valid data must return valid data
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
            "is_publicly_visible": True,
            "coordinates": [[10.300000, 20.600000]],
            "area": 50
        }
        inventory_list = InventoryList.objects.create()
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list)

        response = self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        updated_inventory = Inventory.objects.get(
            id=inventory_to_partially_update.id)
        self.assertEqual(response.data["id"],
                         updated_inventory.id)
        self.assertEqual(response.data["inventory_list"],
                         updated_inventory.inventory_list.id)
        self.assertEqual(response.data["name"],
                         updated_inventory.name)
        self.assertEqual(response.data["description"],
                         updated_inventory.description)
        self.assertEqual(response.data["status"],
                         updated_inventory.status)
        self.assertEqual(response.data["is_publicly_visible"],
                         updated_inventory.is_publicly_visible)
        self.assertTrue(are_datetime_and_json_date_equal(
            updated_inventory.creation_date,
            response.data["creation_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(
            updated_inventory.last_update_date,
            response.data["last_update_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(
            updated_inventory.expiry_date,
            response.data["expiry_date"]))

    def test_partially_update_with_valid_data_really_update_it(self):
        """
        Test partially update an inventory with valid data must update it
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
            "is_publicly_visible": True,
            "coordinates": [[10.300000, 20.600000]],
            "area": 50
        }
        inventory_list = InventoryList.objects.create()
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list)

        self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        updated_inventory = Inventory.objects.get(
            id=inventory_to_partially_update.id)
        self.assertEqual(updated_inventory.name,
                         inventory_data["name"])
        self.assertEqual(updated_inventory.description,
                         inventory_data["description"])
        self.assertEqual(updated_inventory.is_publicly_visible,
                         inventory_data["is_publicly_visible"])

    def test_partially_update_a_non_existing_inventory_returns_http_code_404(self):
        """
        Test partially update a non existing inventory must return a http code 404
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        non_existing_inventory_id_to_partially_update = 999

        response = self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[non_existing_inventory_id_to_partially_update]),
            data={**inventory_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_partially_update_inventory_list_fk_dont_change_it(self):
        """
        Test partially update an inventory related inventory list must not change it
        """
        inventory_list = InventoryList.objects.create()
        unrelated_inventory_list = InventoryList.objects.create()
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
            "inventory_list": unrelated_inventory_list.id
        }
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list)

        self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        updated_inventory = Inventory.objects.get(
            id=inventory_to_partially_update.id)
        self.assertNotEqual(updated_inventory.inventory_list.id,
                            inventory_data["inventory_list"])

    def test_partially_update_open_inventory_returns_http_code_200(self):
        """
        Test partially update an open inventory with valid data must return a http code 200
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list = InventoryList.objects.create()
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.OPEN)

        response = self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partially_update_open_inventory_really_update_data(self):
        """
        Test partially update an open inventory with valid data must update it
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list = InventoryList.objects.create()
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.OPEN)

        self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        updated_inventory = Inventory.objects.get(
            id=inventory_to_partially_update.id)
        self.assertEqual(updated_inventory.name,
                         inventory_data["name"])
        self.assertEqual(updated_inventory.description,
                         inventory_data["description"])

    def test_partially_update_closed_inventory_returns_http_code_400(self):
        """
        Test partially update a closed inventory with valid data must return a http code 400
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list = InventoryList.objects.create()
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.CLOSED)

        response = self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partially_update_closed_inventory_dont_update_data(self):
        """
        Test partially update a closed inventory with valid data must not update it
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list = InventoryList.objects.create()
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.CLOSED)

        self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        updated_inventory = Inventory.objects.get(
            id=inventory_to_partially_update.id)
        self.assertNotEqual(updated_inventory.name,
                            inventory_data["name"])
        self.assertNotEqual(updated_inventory.description,
                            inventory_data["description"])

    def test_partially_update_archived_inventory_returns_http_code_400(self):
        """
        Test partially update an archived inventory with valid data must return a http code 400
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list = InventoryList.objects.create()
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.ARCHIVED)

        response = self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partially_update_archived_inventory_dont_update_data(self):
        """
        Test partially update an archived inventory with valid data must not update it
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list = InventoryList.objects.create()
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.ARCHIVED)

        self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        updated_inventory = Inventory.objects.get(
            id=inventory_to_partially_update.id)
        self.assertNotEqual(updated_inventory.name,
                            inventory_data["name"])
        self.assertNotEqual(updated_inventory.description,
                            inventory_data["description"])

    def test_partially_update_open_inventory_list_returns_http_code_200(self):
        """
        Test partially update an open inventory list with valid data must return a http code 200
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list = InventoryList.objects.create(status=Status.OPEN)
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list)

        response = self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partially_update_open_inventory_list_really_update_data(self):
        """
        Test partially update an open inventory list with valid data must update it
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list = InventoryList.objects.create(status=Status.OPEN)
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.OPEN)

        self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        updated_inventory = Inventory.objects.get(
            id=inventory_to_partially_update.id)
        self.assertEqual(updated_inventory.name,
                         inventory_data["name"])
        self.assertEqual(updated_inventory.description,
                         inventory_data["description"])

    def test_partially_update_closed_inventory_list_returns_http_code_400(self):
        """
        Test partially update a closed inventory list with valid data must return a http code 400
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list = InventoryList.objects.create(status=Status.CLOSED)
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list)

        response = self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partially_update_closed_inventory_list_dont_update_data(self):
        """
        Test partially update a closed inventory list with valid data must not update it
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list = InventoryList.objects.create(status=Status.CLOSED)
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.CLOSED)

        self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        updated_inventory = Inventory.objects.get(
            id=inventory_to_partially_update.id)
        self.assertNotEqual(updated_inventory.name,
                            inventory_data["name"])
        self.assertNotEqual(updated_inventory.description,
                            inventory_data["description"])

    def test_partially_update_archived_inventory_list_returns_http_code_400(self):
        """
        Test partially update an archived inventory list with valid data must return a http code 400
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list = InventoryList.objects.create(status=Status.ARCHIVED)
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list)

        response = self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partially_update_archived_inventory_list_dont_update_data(self):
        """
        Test partially update an archived inventory list with valid data must not update it
        """
        inventory_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list = InventoryList.objects.create(status=Status.ARCHIVED)
        inventory_to_partially_update = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.ARCHIVED)

        self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[inventory_to_partially_update.id]),
            data={**inventory_data},
            format="json")

        updated_inventory = Inventory.objects.get(
            id=inventory_to_partially_update.id)
        self.assertNotEqual(updated_inventory.name,
                            inventory_data["name"])
        self.assertNotEqual(updated_inventory.description,
                            inventory_data["description"])
