# pylint: disable=missing-module-docstring, too-many-public-methods
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import InventoryList, Inventory
from core.models.enums.enum_status import Status


class ChangeStatusInventoryViewsetTestCase(APITestCase):
    """
    Test the change status inventory views
    """

    def test_change_status_to_open_when_status_is_open_returns_http_code_400(self):
        """
        Test change status to open when status is already open must return a http code 400
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.OPEN)

        response = self.client.post(
            reverse.reverse(
                "inventory-open", args=[inventory.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_status_to_open_when_status_is_open_dont_change_state(self):
        """
        Test change status to open when status is already open must not change state
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.OPEN)

        self.client.post(
            reverse.reverse(
                "inventory-open", args=[inventory.id]),
            format="json")

        self.assertEqual(Inventory.objects.get(
            id=inventory.id).status, Status.OPEN)

    def test_change_status_to_open_when_status_is_closed_returns_http_code_200(self):
        """
        Test change status to open when status is closed must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.CLOSED, name="inv")

        response = self.client.post(
            reverse.reverse(
                "inventory-open", args=[inventory.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_change_status_to_open_when_status_is_closed_changes_state(self):
        """
        Test change status to open when status is closed must change state
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.CLOSED, name="inv")

        self.client.post(
            reverse.reverse(
                "inventory-open", args=[inventory.id]),
            format="json")

        self.assertEqual(Inventory.objects.get(
            id=inventory.id).status, Status.OPEN)

    def test_change_status_to_open_when_status_is_archived_returns_http_code_400(self):
        """
        Test change status to open when status is archived must return a http code 400
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.ARCHIVED)

        response = self.client.post(
            reverse.reverse(
                "inventory-open", args=[inventory.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_status_to_open_when_status_is_archived_dont_change_state(self):
        """
        Test change status to open when status is archived must not change state
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.ARCHIVED)

        self.client.post(
            reverse.reverse(
                "inventory-open", args=[inventory.id]),
            format="json")

        self.assertEqual(Inventory.objects.get(
            id=inventory.id).status, Status.ARCHIVED)

    def test_change_status_to_open_on_non_existing_inventory_returns_http_code_404(self):
        """
        Test change status to open on a non existing inventory must return http code 404
        """
        non_existing_inventory_id = 999

        response = self.client.post(
            reverse.reverse(
                "inventory-open", args=[non_existing_inventory_id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_change_status_to_closed_when_status_is_open_returns_http_code_200(self):
        """
        Test change status to closed when status is open must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.OPEN, name="inv")

        response = self.client.post(
            reverse.reverse(
                "inventory-close", args=[inventory.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_change_status_to_closed_when_status_is_open_changes_state(self):
        """
        Test change status to closed when status is open must change state
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.OPEN, name="inv")

        self.client.post(
            reverse.reverse(
                "inventory-close", args=[inventory.id]),
            format="json")

        self.assertEqual(Inventory.objects.get(
            id=inventory.id).status, Status.CLOSED)

    def test_change_status_to_closed_when_status_is_closed_returns_http_code_400(self):
        """
        Test change status to closed when status is closed must return a http code 400
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.CLOSED)

        response = self.client.post(
            reverse.reverse(
                "inventory-close", args=[inventory.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_status_to_closed_when_status_is_closed_dont_change_state(self):
        """
        Test change status to closed when status is closed must not change state
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.CLOSED)

        self.client.post(
            reverse.reverse(
                "inventory-close", args=[inventory.id]),
            format="json")

        self.assertEqual(Inventory.objects.get(
            id=inventory.id).status, Status.CLOSED)

    def test_change_status_to_closed_when_status_is_archived_returns_http_code_200(self):
        """
        Test change status to closed when status is archived must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.ARCHIVED, name="inv")

        response = self.client.post(
            reverse.reverse(
                "inventory-close", args=[inventory.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_change_status_to_closed_when_status_is_archived_changes_state(self):
        """
        Test change status to closed when status is archived must change state
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.ARCHIVED, name="inv")

        self.client.post(
            reverse.reverse(
                "inventory-close", args=[inventory.id]),
            format="json")

        self.assertEqual(Inventory.objects.get(
            id=inventory.id).status, Status.CLOSED)

    def test_change_status_to_closed_on_non_existing_inventory_returns_http_code_404(self):
        """
        Test change status to closed on a non existing inventory must return http code 404
        """
        non_existing_inventory_id = 999

        response = self.client.post(
            reverse.reverse(
                "inventory-close", args=[non_existing_inventory_id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_change_status_to_archived_when_status_is_open_returns_http_code_200(self):
        """
        Test change status to archived when status is open must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.OPEN, name="inv")

        response = self.client.post(
            reverse.reverse(
                "inventory-archive", args=[inventory.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_change_status_to_archived_when_status_is_open_changes_state(self):
        """
        Test change status to archived when status is open must change state
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.OPEN, name="inv")

        self.client.post(
            reverse.reverse(
                "inventory-archive", args=[inventory.id]),
            format="json")

        self.assertEqual(Inventory.objects.get(
            id=inventory.id).status, Status.ARCHIVED)

    def test_change_status_to_archived_when_status_is_closed_returns_http_code_200(self):
        """
        Test change status to archived when status is closed must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.CLOSED, name="inv")

        response = self.client.post(
            reverse.reverse(
                "inventory-archive", args=[inventory.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_change_status_to_archived_when_status_is_closed_changes_state(self):
        """
        Test change status to archived when status is closed must change state
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.CLOSED, name="inv")

        self.client.post(
            reverse.reverse(
                "inventory-archive", args=[inventory.id]),
            format="json")

        self.assertEqual(Inventory.objects.get(
            id=inventory.id).status, Status.ARCHIVED)

    def test_change_status_to_archived_when_status_is_archived_returns_http_code_400(self):
        """
        Test change status to archived when status is archived must return a http code 400
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.ARCHIVED)

        response = self.client.post(
            reverse.reverse(
                "inventory-archive", args=[inventory.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_status_to_archived_when_status_is_archived_dont_changes_state(self):
        """
        Test change status to archived when status is archived must not change state
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list, status=Status.ARCHIVED)

        self.client.post(
            reverse.reverse(
                "inventory-archive", args=[inventory.id]),
            format="json")

        self.assertEqual(Inventory.objects.get(
            id=inventory.id).status, Status.ARCHIVED)

    def test_change_status_to_archived_on_non_existing_inventory_returns_http_code_404(self):
        """
        Test change status to archived on a non existing inventory must return http code 404
        """
        non_existing_inventory_id = 999

        response = self.client.post(
            reverse.reverse(
                "inventory-archive", args=[non_existing_inventory_id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
