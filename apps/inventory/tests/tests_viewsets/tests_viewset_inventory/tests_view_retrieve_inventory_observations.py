# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import InventoryList, Inventory
from core.models.model_observation import Observation
from core.models.model_taxon import Taxon


class RetrieveInventoryObservationsViewsetTestCase(APITestCase):
    """
    Test the retrieve inventory"s observations view
    """

    def test_get_inventory_without_observations_returns_http_code_200(self):
        """
        Test get existing inventory observations withtout observation must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(inventory_list=inventory_list)

        response = self.client.get(
            reverse.reverse(
                "inventory-observations", args=[inventory.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_inventory_observations_without_observations_returns_empty_data(self):
        """
        Test get existing inventory observations withtout observation must return empty data
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(inventory_list=inventory_list)

        response = self.client.get(
            reverse.reverse(
                "inventory-observations", args=[inventory.id]),
            format="json")

        self.assertEqual(len(response.data), 0)

    def test_get_inventory_observations_returns_http_code_200(self):
        """
        Test get existing inventory observations must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(inventory_list=inventory_list)
        taxon = Taxon.objects.create(id=0)
        Observation.objects.create(inventory=inventory, taxon=taxon)
        Observation.objects.create(inventory=inventory, taxon=taxon)
        Observation.objects.create(inventory=inventory, taxon=taxon)

        response = self.client.get(
            reverse.reverse(
                "inventory-observations", args=[inventory.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_inventory_observations_returns_valid_data(self):
        """
        Test get existing inventory observations must return valid data
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(inventory_list=inventory_list)
        taxon = Taxon.objects.create(id=0)
        Observation.objects.create(inventory=inventory, taxon=taxon)
        Observation.objects.create(inventory=inventory, taxon=taxon)
        Observation.objects.create(inventory=inventory, taxon=taxon)

        response = self.client.get(
            reverse.reverse(
                "inventory-observations", args=[inventory.id]),
            format="json")

        self.assertEqual(len(response.data), 3)

    def test_get_observations_of_a_non_existing_inventory_returns_http_code_404(self):
        """
        Test get observations of non existing inventory must return a http code 404
        """
        non_existing_inventory_id = 999

        response = self.client.get(
            reverse.reverse(
                "inventory-observations", args=[non_existing_inventory_id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
