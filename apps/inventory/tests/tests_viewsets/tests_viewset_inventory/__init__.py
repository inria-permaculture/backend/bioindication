# pylint: disable=line-too-long
"""
Test classes for inventory viewset
"""

from inventory.tests.tests_viewsets.tests_viewset_inventory.tests_view_change_status_inventory import *
from inventory.tests.tests_viewsets.tests_viewset_inventory.tests_view_create_inventory import *
from inventory.tests.tests_viewsets.tests_viewset_inventory.tests_view_partial_update_inventory import *
from inventory.tests.tests_viewsets.tests_viewset_inventory.tests_view_retrieve_inventory_observations import *
from inventory.tests.tests_viewsets.tests_viewset_inventory.tests_view_retrieve_inventory import *
from inventory.tests.tests_viewsets.tests_viewset_inventory.tests_view_retrieve_publicly_visible_inventories import *
