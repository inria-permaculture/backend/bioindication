# pylint: disable=missing-module-docstring
from decimal import Decimal
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import InventoryList, Inventory
from core.models.enums.enum_status import Status


class CreateInventoryViewsetTestCase(APITestCase):
    """
    Test the create inventory view
    """

    def test_create_with_valid_data_returns_http_code_201(self):
        """
        Test create an inventory with valid data must return a http code 201
        """
        related_inventory_list = InventoryList.objects.create()
        inventory_data = {
            'inventory_list': related_inventory_list.id,
            "name": "Helloooo",
            "description": "Inventory of the year",
            "is_publicly_visible": True,
            "coordinates": [[10.300000, 20.600000]],
            "area": 50
        }

        response = self.client.post(
            reverse.reverse(
                'inventory-list'),
            data={**inventory_data},
            format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_with_valid_data_really_create_it(self):
        """
        Test create an inventory with valid data must create it
        """
        related_inventory_list = InventoryList.objects.create()
        inventory_data = {
            'inventory_list': related_inventory_list.id,
            "name": "Helloooo",
            "description": "Inventory of the year",
            "is_publicly_visible": True,
            "coordinates": [[10.300000, 20.600000]],
            "area": 50
        }

        self.client.post(
            reverse.reverse(
                'inventory-list'),
            data={**inventory_data},
            format='json')

        created_inventory = Inventory.objects.first()
        self.assertEqual(created_inventory.inventory_list.id,
                         inventory_data['inventory_list'])
        self.assertEqual(created_inventory.name,
                         inventory_data['name'])
        self.assertEqual(created_inventory.description,
                         inventory_data['description'])
        self.assertEqual(created_inventory.is_publicly_visible,
                         inventory_data['is_publicly_visible'])

    def test_create_with_valid_data_returns_all_data(self):
        """
        Test create an inventory with valid data must return all data
        """
        related_inventory_list = InventoryList.objects.create()
        inventory_data = {
            'inventory_list': related_inventory_list.id,
            "name": "Helloooo",
            "description": "Inventory of the year",
            "is_publicly_visible": True,
            "coordinates": [[10.300000, 20.600000]],
            "area": 50
        }

        response = self.client.post(
            reverse.reverse(
                'inventory-list'),
            data={**inventory_data},
            format='json')

        self.assertEqual(response.data['inventory_list'],
                         inventory_data['inventory_list'])
        self.assertEqual(response.data['name'],
                         inventory_data['name'])
        self.assertEqual(response.data['description'],
                         inventory_data['description'])
        self.assertEqual(response.data['is_publicly_visible'],
                         inventory_data['is_publicly_visible'])
        self.assertEqual(response.data['coordinates'][0][0],
                         Decimal('10.300000'))
        self.assertEqual(response.data['coordinates'][0][1],
                         Decimal('20.600000'))
        self.assertEqual(response.data['area'],
                         inventory_data['area'])

    def test_create_without_inventory_list_returns_http_code_400(self):
        """
        Test create an inventory without inventory list must return a http code 400
        """
        inventory_data = {
            "name": "Helloooo",
            "description": "Inventory of the year",
        }

        response = self.client.post(
            reverse.reverse(
                'inventory-list'),
            data={**inventory_data},
            format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_without_inventory_list_dont_create_it(self):
        """
        Test create an inventory without a related inventory list must not create it
        """
        inventory_data = {
            "name": "Helloooo",
            "description": "Inventory of the year"
        }

        self.client.post(
            reverse.reverse(
                'inventory-list'),
            data={**inventory_data},
            format='json')

        self.assertEqual(Inventory.objects.count(), 0)

    def test_create_with_open_inventory_list_returns_http_code_201(self):
        """
        Test create an inventory with an open inventory list must return a http code 201
        """
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        inventory_data = {
            'inventory_list': related_inventory_list.id,
            "name": "Helloooo",
            "description": "Inventory of the year"
        }

        response = self.client.post(
            reverse.reverse(
                'inventory-list'),
            data={**inventory_data},
            format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_with_open_inventory_list_really_create_it(self):
        """
        Test create an inventory with an open inventory list must create it
        """
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        inventory_data = {
            'inventory_list': related_inventory_list.id,
            "name": "Helloooo",
            "description": "Inventory of the year"
        }

        self.client.post(
            reverse.reverse(
                'inventory-list'),
            data={**inventory_data},
            format='json')

        created_inventory = Inventory.objects.first()
        self.assertEqual(created_inventory.inventory_list.id,
                         inventory_data['inventory_list'])
        self.assertEqual(created_inventory.name,
                         inventory_data['name'])
        self.assertEqual(created_inventory.description,
                         inventory_data['description'])

    def test_create_with_closed_inventory_list_returns_http_code_400(self):
        """
        Test create an inventory with a closed inventory list must return a http code 400
        """
        related_inventory_list = InventoryList.objects.create(
            status=Status.CLOSED)
        inventory_data = {
            'inventory_list': related_inventory_list.id,
            "name": "Helloooo",
            "description": "Inventory of the year"
        }

        response = self.client.post(
            reverse.reverse(
                'inventory-list'),
            data={**inventory_data},
            format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_with_closed_inventory_list_dont_create_it(self):
        """
        Test create an inventory with a closed inventory list must not create it
        """
        related_inventory_list = InventoryList.objects.create(
            status=Status.CLOSED)
        inventory_data = {
            'inventory_list': related_inventory_list.id,
            "name": "Helloooo",
            "description": "Inventory of the year"
        }

        self.client.post(
            reverse.reverse(
                'inventory-list'),
            data={**inventory_data},
            format='json')

        self.assertEqual(Inventory.objects.count(), 0)

    def test_create_with_archived_inventory_list_returns_http_code_400(self):
        """
        Test create an inventory with an archived inventory list must return a http code 400
        """
        related_inventory_list = InventoryList.objects.create(
            status=Status.CLOSED)
        inventory_data = {
            'inventory_list': related_inventory_list.id,
            "name": "Helloooo",
            "description": "Inventory of the year"
        }

        response = self.client.post(
            reverse.reverse(
                'inventory-list'),
            data={**inventory_data},
            format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_with_archived_inventory_list_dont_create_it(self):
        """
        Test create an inventory with an archived inventory list must not create it
        """
        related_inventory_list = InventoryList.objects.create(
            status=Status.ARCHIVED)
        inventory_data = {
            'inventory_list': related_inventory_list.id,
            "name": "Helloooo",
            "description": "Inventory of the year"
        }

        self.client.post(
            reverse.reverse(
                'inventory-list'),
            data={**inventory_data},
            format='json')

        self.assertEqual(Inventory.objects.count(), 0)
