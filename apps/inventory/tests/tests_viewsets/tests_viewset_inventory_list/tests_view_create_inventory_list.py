# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import InventoryList, Inventory, Uri


class CreateInventoryListViewsetTestCase(APITestCase):
    """
    Test the create inventory list view
    """

    def test_create_with_valid_data_returns_http_code_201(self):
        """
        Test create an inventory list with valid data must return a http code 201
        """
        inventory_list = {
            "name": "MalisteInventaire",
            "description": "Une liste pas comme les autresr",
            "uri": {
                "identifier": "myinventoriehs"
            },
            "inventories": [
                {
                    "name": "Inventaire Jardin",
                    "description": "Le jardin en biodiversitbé"
                }
            ]
        }

        response = self.client.post(
            reverse.reverse(
                "inventory-list-list"),
            data={**inventory_list},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_with_valid_data_create_inventory_list_and_related(self):
        """
        Test create an inventory list with valid data must create an
        inventory list, uri and inventory
        """
        inventory_list = {
            "name": "MalisteInventaire",
            "description": "Une liste pas comme les autrest",
            "uri": {
                "identifier": "myinventoriezzs"
            },
            "inventories": [
                {
                    "name": "Inventaire Jardin",
                    "description": "Le jardin en biodiversitééé"
                }
            ]
        }

        self.client.post(
            reverse.reverse(
                "inventory-list-list"),
            data={**inventory_list},
            format="json")

        self.assertEqual(InventoryList.objects.count(), 1)
        self.assertEqual(Inventory.objects.count(), 1)
        self.assertEqual(Uri.objects.count(), 1)

    def test_create_without_inventory_data_returns_http_code_400(self):
        """
        Test create an inventory list with a valid identifier but
        without an inventory must return an http code 400
        """
        inventory_list = {
            "name": "MalisteInventaire",
            "description": "Une liste pas comme les autresf",
            "uri": {
                "identifier": "myinventoriesc"
            }
        }

        response = self.client.post(
            reverse.reverse(
                "inventory-list-list"),
            data={**inventory_list},
            format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_with_already_used_identifier_returns_http_code_400(self):
        """
        Test create an inventory list with an already used identifier
        must return an http code 400
        """
        unique_inventory_list = {
            "name": "MalisteInventaire",
            "description": "Une liste pas comme les autresd",
            "uri": {
                "identifier": "myinventoriesv"
            }
        }
        redundant_inventory_list = {
            "name": "MalisteInventaire 2",
            "description": "Une liste pas comme les autres 2",
            "uri": {
                "identifier": "myinventoriesnb"
            }
        }
        self.client.post(
            reverse.reverse(
                "inventory-list-list"),
            data={**unique_inventory_list},
            format="json")

        response = self.client.post(
            reverse.reverse(
                "inventory-list-list"),
            data={**redundant_inventory_list},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_with_already_used_identifier_dont_creates_inventory_list_and_related(self):
        """
        Test create an inventory list with an already used identifier
        mustnt create it nor related inventory and uri
        """
        unique_inventory_list = {
            "name": "MalisteInventaire",
            "description": "Une liste pas comme les autresd",
            "uri": {
                "identifier": "myinventoriesjj"
            },
            "inventories": [
                {
                    "name": "string",
                    "description": "string"
                }
            ]
        }
        redundant_inventory_list = {
            "name": "MalisteInventaire 2",
            "description": "Une liste pas comme les autres 2",
            "uri": {
                "identifier": "myinventoriesjj"
            },
            "inventories": [
                {
                    "name": "string",
                    "description": "string"
                }
            ]
        }
        self.client.post(
            reverse.reverse(
                "inventory-list-list"),
            data={**unique_inventory_list},
            format="json")

        self.client.post(
            reverse.reverse(
                "inventory-list-list"),
            data={**redundant_inventory_list},
            format="json")

        self.assertEqual(InventoryList.objects.count(), 1)
        self.assertEqual(Uri.objects.count(), 1)
