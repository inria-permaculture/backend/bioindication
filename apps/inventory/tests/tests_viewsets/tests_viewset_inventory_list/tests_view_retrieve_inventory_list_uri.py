# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import InventoryList, Uri
from core.utils import are_datetime_and_json_date_equal


class RetrieveInventoryListUriViewsetTestCase(APITestCase):
    """
    Test the retrieve inventory list"s uri view
    """

    def test_get_an_existing_inventory_list_uri_returns_http_code_200(self):
        """
        Test get an existing inventory list uri must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        Uri.objects.create(
            inventory_list=inventory_list,
            identifier="heyyy")

        response = self.client.get(
            reverse.reverse(
                "inventory-list-uri", args=[inventory_list.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_an_existing_inventory_list_uri_returns_valid_data(self):
        """
        Test get an existing inventory list uri must return all data
        """
        inventory_list = InventoryList.objects.create()
        related_uri = Uri.objects.create(
            inventory_list=inventory_list,
            identifier="heyyy")

        response = self.client.get(
            reverse.reverse(
                "inventory-list-uri", args=[inventory_list.id]),
            format="json")

        self.assertEqual(related_uri.id, response.data["id"])
        self.assertEqual(related_uri.identifier,
                         response.data["identifier"])
        self.assertEqual(related_uri.inventory_list.id,
                         response.data["inventory_list"])
        self.assertTrue(are_datetime_and_json_date_equal(related_uri.creation_date,
                                                         response.data["creation_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(related_uri.last_update_date,
                                                         response.data["last_update_date"]))

    def test_get_uri_of_a_non_existing_inventory_list_returns_http_code_404(self):
        """
        Test get an uri of non existing inventory list must return a http code 404
        """
        non_existing_inventory_list_id = 999
        response = self.client.get(
            reverse.reverse(
                "inventory-list-uri", args=[non_existing_inventory_list_id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
