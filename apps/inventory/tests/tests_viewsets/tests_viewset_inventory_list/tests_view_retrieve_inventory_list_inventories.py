# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import InventoryList, Inventory


class RetrieveInventoryListInventoriesViewsetTestCase(APITestCase):
    """
    Test the retrieve inventory list"s inventories view
    """

    def test_get_inventory_list_inventories_without_inventory_returns_http_code_200(self):
        """
        Test get existing inventory list inventories withtout inventory must return a http code 200
        """
        inventory_list = InventoryList.objects.create()

        response = self.client.get(
            reverse.reverse(
                "inventory-list-inventories", args=[inventory_list.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_inventory_list_inventories_without_inventory_returns_empty_data(self):
        """
        Test get existing inventory list inventories withtout inventory must return empty data
        """
        inventory_list = InventoryList.objects.create()

        response = self.client.get(
            reverse.reverse(
                "inventory-list-inventories", args=[inventory_list.id]),
            format="json")

        self.assertEqual(len(response.data), 0)

    def test_get_inventory_list_inventories_returns_http_code_200(self):
        """
        Test get existing inventory list inventories must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        Inventory.objects.create(
            inventory_list=inventory_list)
        Inventory.objects.create(
            inventory_list=inventory_list)
        Inventory.objects.create(
            inventory_list=inventory_list)

        response = self.client.get(
            reverse.reverse(
                "inventory-list-inventories", args=[inventory_list.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_inventory_list_inventories_returns_valid_data(self):
        """
        Test get existing inventory list inventories must return valid data
        """
        inventory_list = InventoryList.objects.create()
        Inventory.objects.create(
            inventory_list=inventory_list)
        Inventory.objects.create(
            inventory_list=inventory_list)
        Inventory.objects.create(
            inventory_list=inventory_list)

        response = self.client.get(
            reverse.reverse(
                "inventory-list-inventories", args=[inventory_list.id]),
            format="json")

        self.assertEqual(len(response.data), 3)

    def test_get_inventories_of_a_non_existing_inventory_list_returns_http_code_404(self):
        """
        Test get inventories of non existing inventory list must return a http code 404
        """
        non_existing_inventory_list_id = 999

        response = self.client.get(
            reverse.reverse(
                "inventory-list-inventories", args=[non_existing_inventory_list_id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
