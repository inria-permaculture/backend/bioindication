# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import InventoryList
from core.utils import are_datetime_and_json_date_equal


class RetrieveInventoryListViewsetTestCase(APITestCase):
    """
    Test the retrieve inventory list view
    """

    def test_get_an_existing_inventory_list_returns_http_code_200(self):
        """
        Test get an existing inventory list must return a http code 200
        """
        inventory_list = InventoryList.objects.create()

        response = self.client.get(
            reverse.reverse(
                "inventory-list-detail", args=[inventory_list.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_an_existing_inventory_list_returns_valid_data(self):
        """
        Test get an existing inventory list must return all data
        """
        inventory_list = InventoryList.objects.create()

        response = self.client.get(
            reverse.reverse(
                "inventory-list-detail", args=[inventory_list.id]),
            format="json")

        self.assertEqual(inventory_list.id,
                         response.data["id"])
        self.assertEqual(inventory_list.name,
                         response.data["name"])
        self.assertEqual(inventory_list.description,
                         response.data["description"])
        self.assertEqual(inventory_list.status,
                         response.data["status"])
        self.assertTrue(are_datetime_and_json_date_equal(inventory_list.creation_date,
                                                         response.data["creation_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(inventory_list.last_update_date,
                                                         response.data["last_update_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(inventory_list.expiry_date,
                                                         response.data["expiry_date"]))

    def test_get_a_non_existing_inventory_list_returns_http_code_404(self):
        """
        Test get a non existing inventory list must return a http code 404
        """
        non_existing_inventory_list_id = 999
        response = self.client.get(
            reverse.reverse(
                "inventory-list-detail", args=[non_existing_inventory_list_id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
