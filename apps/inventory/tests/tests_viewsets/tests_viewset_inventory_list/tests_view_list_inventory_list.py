# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import InventoryList, Uri
from core.utils import are_datetime_and_json_date_equal


class ListInventoryListViewsetTestCase(APITestCase):
    """
    Test the list inventory list view
    """

    def test_list_with_existing_identifier_returns_http_code_200(self):
        """
        Test list with an existing identifier must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        related_uri = Uri.objects.create(
            inventory_list=inventory_list, identifier="mylittleponey")

        response = self.client.get(
            reverse.reverse(
                "inventory-list-list"),
            data={"identifier": related_uri.identifier},
            format="json")

        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_list_with_existing_identifier_returns_valid_data(self):
        """
        Test list with an existing identifier must
        return valid inventory_list data
        """
        inventory_list = InventoryList.objects.create()
        related_uri = Uri.objects.create(
            inventory_list=inventory_list, identifier="mylittleponey")

        response = self.client.get(
            reverse.reverse(
                "inventory-list-list"),
            data={"identifier": related_uri.identifier},
            format="json")

        self.assertEqual(inventory_list.id,
                         response.data[0]["id"])
        self.assertEqual(inventory_list.name,
                         response.data[0]["name"])
        self.assertEqual(inventory_list.description,
                         response.data[0]["description"])
        self.assertEqual(inventory_list.status,
                         response.data[0]["status"])
        self.assertTrue(are_datetime_and_json_date_equal(inventory_list.creation_date,
                                                         response.data[0]["creation_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(inventory_list.last_update_date,
                                                         response.data[0]["last_update_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(inventory_list.expiry_date,
                                                         response.data[0]["expiry_date"]))

    def test_list_with_non_existing_identifier_returns_http_code_200(self):
        """
        Test list with a non existing identifier must
        return a http code 200
        It sounds a bit strange, but we are using the default list filtering mechanism and
        this is the default behavior
        """
        non_existing_identifier = "akouloukoukou"

        response = self.client.get(
            reverse.reverse(
                "inventory-list-list"),
            data={"identifier": non_existing_identifier},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_with_non_existing_identifier_returns_empty_result(self):
        """
        Test list with a non existing identifier must
        return an empty result
        It sounds a bit strange, but we are using the default list filtering mechanism and
        this is the default behavior
        """
        non_existing_identifier = "akouloukoukou"

        response = self.client.get(
            reverse.reverse(
                "inventory-list-list"),
            data={"identifier": non_existing_identifier},
            format="json")

        self.assertEqual(response.data, [])

    def test_list_without_identifier_returns_empty_result(self):
        """
        Test list without identifier must
        return an empty result
        It sounds a bit strange, but we are using the default list filtering mechanism and
        this is the default behavior
        """
        response = self.client.get(
            reverse.reverse(
                "inventory-list-list"),
            format="json")

        self.assertEqual(response.data, [])

    def test_list_without_identifier_returns_status_200(self):
        """
        Test list without identifier must
        return a http code 200
        It sounds a bit strange, but we are using the default list filtering mechanism and
        this is the default behavior
        """
        response = self.client.get(
            reverse.reverse(
                "inventory-list-list"),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
