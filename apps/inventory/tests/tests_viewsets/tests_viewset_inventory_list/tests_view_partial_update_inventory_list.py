# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import InventoryList
from core.models.enums.enum_status import Status
from core.utils import are_datetime_and_json_date_equal


class PartialUpdateInventoryListViewsetTestCase(APITestCase):
    """
    Test the partial update inventory view
    """

    def test_partially_update_with_valid_data_returns_http_code_200(self):
        """
        Test partially update an inventory list with valid data must return a http code 200
        """
        inventory_list_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list_to_partially_update = InventoryList.objects.create()

        response = self.client.patch(
            reverse.reverse(
                "inventory-list-detail", args=[inventory_list_to_partially_update.id]),
            data={**inventory_list_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partially_update_with_valid_data_returns_valid_data(self):
        """
        Test partially update an inventory list with valid data must return valid data
        """
        inventory_list_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list_to_partially_update = InventoryList.objects.create()

        response = self.client.patch(
            reverse.reverse(
                "inventory-list-detail", args=[inventory_list_to_partially_update.id]),
            data={**inventory_list_data},
            format="json")

        updated_inventory_list = InventoryList.objects.get(
            id=inventory_list_to_partially_update.id)
        self.assertEqual(response.data["id"],
                         updated_inventory_list.id)
        self.assertEqual(response.data["name"],
                         updated_inventory_list.name)
        self.assertEqual(response.data["description"],
                         updated_inventory_list.description)
        self.assertEqual(response.data["status"],
                         updated_inventory_list.status)
        self.assertTrue(are_datetime_and_json_date_equal(
            updated_inventory_list.creation_date,
            response.data["creation_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(
            updated_inventory_list.last_update_date,
            response.data["last_update_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(
            updated_inventory_list.expiry_date,
            response.data["expiry_date"]))

    def test_partially_update_with_valid_data_returns_really_update_it(self):
        """
        Test partially update an inventory list with valid data must update it
        """
        inventory_list_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list_to_partially_update = InventoryList.objects.create()

        self.client.patch(
            reverse.reverse(
                "inventory-list-detail", args=[inventory_list_to_partially_update.id]),
            data={**inventory_list_data},
            format="json")

        updated_inventory_list = InventoryList.objects.get(
            id=inventory_list_to_partially_update.id)
        self.assertEqual(updated_inventory_list.name,
                         inventory_list_data["name"])
        self.assertEqual(updated_inventory_list.description,
                         inventory_list_data["description"])

    def test_partially_update_an_non_existing_inventory_list_returns_http_code_404(self):
        """
        Test partially update a non existing inventory list must return an http code 404
        """
        inventory_list_data = {
            "name": "Hey",
            "description": "coucou",
        }
        non_existing_inventory_list_id_to_partially_update = 999

        response = self.client.patch(
            reverse.reverse(
                "inventory-list-detail", args=[non_existing_inventory_list_id_to_partially_update]),
            data={**inventory_list_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_partially_update_with_status_open_returns_http_code_200(self):
        """
        Test partially update an inventory list with valid data
        and status open must return a http code 200
        """
        inventory_list_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list_to_partially_update = InventoryList.objects.create(
            status=Status.OPEN)

        response = self.client.patch(
            reverse.reverse(
                "inventory-list-detail", args=[inventory_list_to_partially_update.id]),
            data={**inventory_list_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partially_update_with_valid_data_with_status_open_really_update_data(self):
        """
        Test partially update an inventory list with valid data and status open must it
        """
        inventory_list_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list_to_partially_update = InventoryList.objects.create(
            status=Status.OPEN)

        self.client.patch(
            reverse.reverse(
                "inventory-list-detail", args=[inventory_list_to_partially_update.id]),
            data={**inventory_list_data},
            format="json")

        updated_inventory_list = InventoryList.objects.get(
            id=inventory_list_to_partially_update.id)
        self.assertEqual(updated_inventory_list.name,
                         inventory_list_data["name"])
        self.assertEqual(updated_inventory_list.description,
                         inventory_list_data["description"])

    def test_partially_update_with_status_closed_returns_http_code_400(self):
        """
        Test partially update an inventory list with valid data
        and status closed must return a http code 400
        """
        inventory_list_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list_to_partially_update = InventoryList.objects.create(
            status=Status.CLOSED)

        response = self.client.patch(
            reverse.reverse(
                "inventory-list-detail", args=[inventory_list_to_partially_update.id]),
            data={**inventory_list_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partially_update_with_valid_data_with_status_closed_dont_update_data(self):
        """
        Test partially update an inventory list with valid data and status closed must not update it
        """
        inventory_list_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list_to_partially_update = InventoryList.objects.create(
            status=Status.CLOSED)

        self.client.patch(
            reverse.reverse(
                "inventory-list-detail", args=[inventory_list_to_partially_update.id]),
            data={**inventory_list_data},
            format="json")

        updated_inventory_list = InventoryList.objects.get(
            id=inventory_list_to_partially_update.id)
        self.assertNotEqual(updated_inventory_list.name,
                            inventory_list_data["name"])
        self.assertNotEqual(updated_inventory_list.description,
                            inventory_list_data["description"])

    def test_partially_update_with_status_archived_returns_http_code_400(self):
        """
        Test partially update an inventory list with valid data
        and status archived must return a http code 400
        """
        inventory_list_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list_to_partially_update = InventoryList.objects.create(
            status=Status.ARCHIVED)

        response = self.client.patch(
            reverse.reverse(
                "inventory-list-detail", args=[inventory_list_to_partially_update.id]),
            data={**inventory_list_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partially_update_with_status_archived_dont_update_data(self):
        """
        Test partially update an inventory list with valid data
        and status archived must not update it
        """
        inventory_list_data = {
            "name": "Hey",
            "description": "coucou",
        }
        inventory_list_to_partially_update = InventoryList.objects.create(
            status=Status.ARCHIVED)

        self.client.patch(
            reverse.reverse(
                "inventory-list-detail", args=[inventory_list_to_partially_update.id]),
            data={**inventory_list_data},
            format="json")

        updated_inventory_list = InventoryList.objects.get(
            id=inventory_list_to_partially_update.id)
        self.assertNotEqual(updated_inventory_list.name,
                            inventory_list_data["name"])
        self.assertNotEqual(updated_inventory_list.description,
                            inventory_list_data["description"])
