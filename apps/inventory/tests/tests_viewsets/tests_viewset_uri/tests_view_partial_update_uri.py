# pylint: disable=missing-module-docstring
import json
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import Uri
from core.models.enums.enum_status import Status
from core.models.model_inventory_list import InventoryList
from core.utils import are_datetime_and_json_date_equal


class PartialUpdateUriViewsetTestCase(APITestCase):
    """
    Test the partial update uri view
    """

    def test_partial_update_with_valid_identifier_returns_http_code_200(self):
        """
        Test partially update an uri with a valid identifier must return a http code 200
        """
        inventory_list = InventoryList.objects.create()
        uri_to_update = Uri.objects.create(
            inventory_list=inventory_list, identifier="hello")
        new_valid_identifier = "hello2"

        response = self.client.patch(
            reverse.reverse(
                "uri-detail", args=[uri_to_update.id]),
            data={"identifier": new_valid_identifier},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partial_update_with_valid_identifier_update_uri(self):
        """
        Test partially update an uri with a valid identifier must update the stored uri
        """
        inventory_list = InventoryList.objects.create()
        uri_to_update = Uri.objects.create(
            inventory_list=inventory_list, identifier="hello")
        new_valid_identifier = "hello2"

        self.client.patch(
            reverse.reverse(
                "uri-detail", args=[uri_to_update.id]),
            data={"identifier": new_valid_identifier},
            format="json")

        self.assertEqual(Uri.objects.filter(
            identifier=new_valid_identifier).count(), 1)

    def test_partial_update_with_valid_identifier_returns_valid_data(self):
        """
        Test partially update an uri with a valid identifier must return valid data
        """
        inventory_list = InventoryList.objects.create()
        uri_to_update = Uri.objects.create(
            inventory_list=inventory_list, identifier="hello")
        new_valid_identifier = "hello2"

        response = self.client.patch(
            reverse.reverse(
                "uri-detail", args=[uri_to_update.id]),
            data={"identifier": new_valid_identifier},
            format="json")
        updated_uri = Uri.objects.get(id=uri_to_update.id)

        self.assertEqual(updated_uri.id,
                         response.data["id"])
        self.assertEqual(updated_uri.inventory_list.id,
                         response.data["inventory_list"])
        self.assertEqual(updated_uri.identifier,
                         response.data["identifier"])
        self.assertTrue(are_datetime_and_json_date_equal(updated_uri.creation_date,
                                                         response.data["creation_date"]))

    def test_partial_update_with_already_used_identifier_returns_http_code_400(self):
        """
        Test partially update an uri with an invalid identifier must return a http code 400
        """
        inventory_list = InventoryList.objects.create()
        uri_to_update = Uri.objects.create(
            inventory_list=inventory_list, identifier="hello")
        new_invalid_identifier = "1"

        response = self.client.patch(
            reverse.reverse(
                "uri-detail", args=[uri_to_update.id]),
            data=json.dumps(
                {"identifier": new_invalid_identifier}),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partial_update_a_non_existing_uri_returns_http_code_404(self):
        """
        Test partially update a non existing uri must return a http code 404
        """
        non_existing_uri_id = 999

        response = self.client.patch(
            reverse.reverse(
                "uri-detail", args=[non_existing_uri_id]),
            data={"identifier": "randomidentifierbecausewedontcare"},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_partial_update_when_status_open_with_valid_identifier_returns_http_code_200(self):
        """
        Test partially update uri with a valid identifier
        when the linked inventory list"s status is open must return a http code 200
        """
        open_inventory_list = InventoryList.objects.create(status=Status.OPEN)
        uri_to_update = Uri.objects.create(
            inventory_list=open_inventory_list, identifier="hello")
        new_valid_identifier = "hello2"

        response = self.client.patch(
            reverse.reverse(
                "uri-detail", args=[uri_to_update.id]),
            data={"identifier": new_valid_identifier},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partial_update_when_status_open_with_valid_identifier_update_uri(self):
        """
        Test partially update uri with a valid identifier
        when the linked inventory list"s status is open must update the stored uri
        """
        open_inventory_list = InventoryList.objects.create(status=Status.OPEN)
        uri_to_update = Uri.objects.create(
            inventory_list=open_inventory_list, identifier="hello")
        new_valid_identifier = "hello2"

        self.client.patch(
            reverse.reverse(
                "uri-detail", args=[uri_to_update.id]),
            data={"identifier": new_valid_identifier},
            format="json")
        updated_uri = Uri.objects.get(id=uri_to_update.id)

        self.assertEqual(updated_uri.identifier, new_valid_identifier)

    def test_partial_update_when_status_closed_with_valid_identifier_returns_http_code_400(self):
        """
        Test partially update uri with a valid identifier
        when the linked inventory list"s status is closed must return a http code 400
        """
        closed_inventory_list = InventoryList.objects.create(
            status=Status.CLOSED)
        uri_to_update = Uri.objects.create(
            inventory_list=closed_inventory_list, identifier="hello")
        new_valid_identifier = "hello2"

        response = self.client.patch(
            reverse.reverse(
                "uri-detail", args=[uri_to_update.id]),
            data={"identifier": new_valid_identifier},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partial_update_when_status_closed_with_valid_identifier_dont_update_uri(self):
        """
        Test partially update uri with a valid identifier
        when the linked inventory list"s status is closed mustn"t update the stored uri
        """
        closed_inventory_list = InventoryList.objects.create(
            status=Status.CLOSED)
        uri_to_update = Uri.objects.create(
            inventory_list=closed_inventory_list, identifier="hello")
        new_valid_identifier = "hello2"

        self.client.patch(
            reverse.reverse(
                "uri-detail", args=[uri_to_update.id]),
            data={"identifier": new_valid_identifier},
            format="json")
        updated_uri = Uri.objects.get(id=uri_to_update.id)

        self.assertNotEqual(updated_uri.identifier, new_valid_identifier)

    def test_partial_update_when_status_archived_with_valid_identifier_returns_http_code_400(self):
        """
        Test partially update uri with a valid identifier
        when the linked inventory list"s status is archived must return a http code 400
        """
        archived_inventory_list = InventoryList.objects.create(
            status=Status.ARCHIVED)
        uri_to_update = Uri.objects.create(
            inventory_list=archived_inventory_list, identifier="hello")
        new_valid_identifier = "hello2"

        response = self.client.patch(
            reverse.reverse(
                "uri-detail", args=[uri_to_update.id]),
            data={"identifier": new_valid_identifier},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partial_update_when_status_archived_with_valid_identifier_dont_update_uri(self):
        """
        Test partially update uri with a valid identifier
        when the linked inventory list"s status is archived mustn"t update the stored uri
        """
        archived_inventory_list = InventoryList.objects.create(
            status=Status.ARCHIVED)
        uri_to_update = Uri.objects.create(
            inventory_list=archived_inventory_list, identifier="hello")
        new_valid_identifier = "hello2"

        self.client.patch(
            reverse.reverse(
                "uri-detail", args=[uri_to_update.id]),
            data={"identifier": new_valid_identifier},
            format="json")
        updated_uri = Uri.objects.get(id=uri_to_update.id)

        self.assertNotEqual(updated_uri.identifier, new_valid_identifier)

    def test_partially_update_inventory_list_fk_dont_change_it(self):
        """
        Test partially update an uri related inventory list id must not change it
        """
        related_inventory_list = InventoryList.objects.create()
        unrelated_inventory_list = InventoryList.objects.create()
        uri_to_partially_update = Uri.objects.create(
            inventory_list=related_inventory_list, identifier="coucou")
        uri_to_partially_update_data = {
            "inventory_list": unrelated_inventory_list.id
        }

        self.client.patch(
            reverse.reverse(
                "uri-detail", args=[uri_to_partially_update.id]),
            data={**uri_to_partially_update_data},
            format="json")

        self.assertNotEqual(Uri.objects.get(
            id=uri_to_partially_update.id).inventory_list,
            uri_to_partially_update_data["inventory_list"])
