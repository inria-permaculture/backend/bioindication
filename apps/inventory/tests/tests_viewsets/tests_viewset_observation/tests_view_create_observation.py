# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import Observation, Taxon, InventoryList, Inventory
from core.models.enums import Status, Alive, Spontaneous, Lifestage, Gender, Quantification


class CreateObservationViewsetTestCase(APITestCase):
    """
    Test the create observation view
    """

    @staticmethod
    def get_observation_data(related_inventory, observed_taxon):
        """
        Create a dic of observation data
        """
        return {
            "inventory": related_inventory.id,
            "taxon": observed_taxon.id,
            "comment": "coucou les crocodiles",
            "observator_nickname": "jonnhy john johnson",
            "number_of_individuals": 50,
            "soil_coverage": 0.5,
            "alive": Alive.YES,
            "spontaneous": Spontaneous.YES,
            "gender": Gender.ALL,
            "lifestage": Lifestage.ALL,
            "quantification": Quantification.COUNT,
            "user_scientific_name": "Mentha piperita",
            "user_vernacular_name": "Menthe poivrée"
        }

    def test_create_with_valid_data_returns_http_code_201(self):
        """
        Test create an observation with valid data must return a http code 201
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create()
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list)
        observation_data = self.get_observation_data(
            related_inventory, observed_taxon)

        response = self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_with_valid_data_returns_valid_data(self):
        """
        Test create an observation with valid data must return valid data
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create()
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list)
        observation_data = self.get_observation_data(
            related_inventory, observed_taxon)

        response = self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(observation_data["taxon"],
                         response.data["taxon"])
        self.assertEqual(observation_data["inventory"],
                         response.data["inventory"])
        self.assertEqual(observation_data["comment"],
                         response.data["comment"])
        self.assertEqual(observation_data["observator_nickname"],
                         response.data["observator_nickname"])
        self.assertEqual(observation_data["number_of_individuals"],
                         response.data["number_of_individuals"])
        self.assertEqual(observation_data["soil_coverage"],
                         response.data["soil_coverage"])
        self.assertEqual(observation_data["alive"],
                         response.data["alive"])
        self.assertEqual(observation_data["spontaneous"],
                         response.data["spontaneous"])
        self.assertEqual(observation_data["gender"],
                         response.data["gender"])
        self.assertEqual(observation_data["lifestage"],
                         response.data["lifestage"])
        self.assertEqual(observation_data["quantification"],
                         response.data["quantification"])
        self.assertEqual(observation_data["user_scientific_name"],
                         response.data["user_scientific_name"])
        self.assertEqual(observation_data["user_vernacular_name"],
                         response.data["user_vernacular_name"])

    def test_create_with_invalid_data_returns_http_code_400(self):
        """
        Test create an observation with invalid data must return a http code 400
        """
        observation_data = {
        }

        response = self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_with_valid_data_on_open_inventory_inventory_list_returns_http_code_201(self):
        """
        Test create an observation with valid data on open inventory
        and inventorylist must return a http code 201
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.OPEN)
        observation_data = self.get_observation_data(
            related_inventory, observed_taxon)

        response = self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_with_valid_data_on_open_inventory_inventory_list_create_observation(self):
        """
        Test create an observation with valid data on open inventory
        and inventorylist must create a new observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.OPEN)
        observation_data = self.get_observation_data(
            related_inventory, observed_taxon)

        self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(Observation.objects.count(), 1)

    def test_create_on_closed_inventory_open_inventory_list_returns_http_code_400(self):
        """
        Test create an observation with valid data on closed inventory
        and open inventorylist must return a http code 400
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.CLOSED)
        observation_data = self.get_observation_data(
            related_inventory, observed_taxon)

        response = self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_on_closed_inventory_open_inventory_list_dont_create_observation(self):
        """
        Test create an observation with valid data on closed inventory
        and open inventorylist must not create a new observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.CLOSED)
        observation_data = self.get_observation_data(
            related_inventory, observed_taxon)

        self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(Observation.objects.count(), 0)

    def test_create_data_on_open_inventory_closed_inventory_list_returns_http_code_400(self):
        """
        Test create an observation with valid data on open inventory
        and closed inventorylist must return a http code 400
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.CLOSED)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.OPEN)
        observation_data = self.get_observation_data(
            related_inventory, observed_taxon)

        response = self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_on_open_inventory_closed_inventory_list_dont_create_observation(self):
        """
        Test create an observation with valid data on open inventory
        and closed inventorylist must not create a new observation,
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.CLOSED)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.OPEN)
        observation_data = self.get_observation_data(
            related_inventory, observed_taxon)

        self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(Observation.objects.count(), 0)

    def test_create_on_archived_inventory_open_inventory_list_returns_http_code_400(self):
        """
        Test create an observation with valid data on archived inventory
        and open inventorylist must return a http code 400
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.ARCHIVED)
        observation_data = self.get_observation_data(
            related_inventory, observed_taxon)

        response = self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_on_archived_inventory_open_inventory_list_dont_create_observation(self):
        """
        Test create an observation with valid data on archived inventory
        and open inventorylist must not create a new observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.ARCHIVED)
        observation_data = self.get_observation_data(
            related_inventory, observed_taxon)

        self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(Observation.objects.count(), 0)

    def test_create_on_open_inventory_archived_inventory_list_returns_http_code_400(self):
        """
        Test create an observation with valid data on open inventory
        and archived inventorylist must return a http code 400
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.ARCHIVED)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.OPEN)
        observation_data = self.get_observation_data(
            related_inventory, observed_taxon)

        response = self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_on_open_inventory_archived_inventory_list_dont_create_observation(self):
        """
        Test create an observation with valid data on open inventory
        and archived inventorylist must not create a new observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.ARCHIVED)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.OPEN)
        observation_data = self.get_observation_data(
            related_inventory, observed_taxon)

        self.client.post(
            reverse.reverse(
                "observation-list"),
            data={**observation_data},
            format="json")

        self.assertEqual(Observation.objects.count(), 0)
