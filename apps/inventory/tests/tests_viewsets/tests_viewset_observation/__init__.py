# pylint: disable=line-too-long
"""
Test classes for observation viewset
"""

from inventory.tests.tests_viewsets.tests_viewset_observation.tests_view_create_observation import *
from inventory.tests.tests_viewsets.tests_viewset_observation.tests_view_destroy_observation import *
from inventory.tests.tests_viewsets.tests_viewset_observation.tests_view_partial_update_observation import *
from inventory.tests.tests_viewsets.tests_viewset_observation.tests_view_retrieve_observation import *
