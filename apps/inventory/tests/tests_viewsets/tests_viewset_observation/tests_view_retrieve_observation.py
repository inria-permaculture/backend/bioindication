# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import Observation, Taxon, InventoryList, Inventory
from core.models.enums import Alive, Spontaneous, Lifestage, Gender, Quantification
from core.utils import are_datetime_and_json_date_equal


class RetrieveObservationViewsetTestCase(APITestCase):
    """
    Test the observation retrieve view
    """

    def test_get_observation_returns_http_code_200(self):
        """
        Test get an existing observation must return a http code 200
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create()
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list)
        observation_to_retrieve = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        response = self.client.get(
            reverse.reverse(
                "observation-detail", args=[observation_to_retrieve.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_observation_returns_valid_data(self):
        """
        Test get an existing observation must return valid data
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create()
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list)
        observation_to_retrieve_data = {
            "comment": "coucou les toto",
            "observator_nickname": "jonnhy johnn",
            "number_of_individuals": 50,
            "soil_coverage": 0.5,
            "alive": Alive.YES,
            "spontaneous": Spontaneous.YES,
            "gender": Gender.ALL,
            "lifestage": Lifestage.ALL,
            "quantification": Quantification.COUNT,
            "user_scientific_name": "Mentha piperita",
            "user_vernacular_name": "Menthe poivrée"
        }
        observation_to_retrieve = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon, **observation_to_retrieve_data)

        response = self.client.get(
            reverse.reverse(
                "observation-detail", args=[observation_to_retrieve.id]),
            format="json")

        self.assertEqual(observation_to_retrieve.id,
                         response.data["id"])
        self.assertEqual(observation_to_retrieve.taxon_id,
                         response.data["taxon"])
        self.assertEqual(observation_to_retrieve.inventory_id,
                         response.data["inventory"])
        self.assertEqual(observation_to_retrieve.comment,
                         response.data["comment"])
        self.assertEqual(observation_to_retrieve.observator_nickname,
                         response.data["observator_nickname"])
        self.assertEqual(observation_to_retrieve.number_of_individuals,
                         response.data["number_of_individuals"])
        self.assertEqual(observation_to_retrieve.soil_coverage,
                         response.data["soil_coverage"])
        self.assertEqual(observation_to_retrieve.alive,
                         response.data["alive"])
        self.assertEqual(observation_to_retrieve.spontaneous,
                         response.data["spontaneous"])
        self.assertEqual(observation_to_retrieve.gender,
                         response.data["gender"])
        self.assertEqual(observation_to_retrieve.lifestage,
                         response.data["lifestage"])
        self.assertEqual(observation_to_retrieve.quantification,
                         response.data["quantification"])
        self.assertEqual(observation_to_retrieve.user_scientific_name,
                         response.data["user_scientific_name"])
        self.assertEqual(observation_to_retrieve.user_vernacular_name,
                         response.data["user_vernacular_name"])
        self.assertTrue(are_datetime_and_json_date_equal(
            observation_to_retrieve.creation_date,
            response.data["creation_date"]))
        self.assertTrue(are_datetime_and_json_date_equal(
            observation_to_retrieve.last_update_date,
            response.data["last_update_date"]))

    def test_get_non_existing_observation_returns_http_code_404(self):
        """
        Test get a non existing observation must return a http code 404
        """
        non_existing_observation_id_to_retrieve = 999

        response = self.client.get(
            reverse.reverse(
                "observation-detail", args=[non_existing_observation_id_to_retrieve]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
