# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models.enums import Status
from core.models import Observation, Taxon, InventoryList, Inventory


class DestroyObservationViewsetTestCase(APITestCase):
    """
    Test the observation destroy view
    """

    def test_destroy_observation_returns_http_code_204(self):
        """
        Test destroy an existing observation must return a http code 204
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create()
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list)
        observation_to_destroy = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        response = self.client.delete(
            reverse.reverse(
                "observation-detail", args=[observation_to_destroy.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_destroy_observation_destroy_observation(self):
        """
        Test destroy an existing observation must destroy the target observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create()
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list)
        observation_to_destroy = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        self.client.delete(
            reverse.reverse(
                "observation-detail", args=[observation_to_destroy.id]),
            format="json")

        self.assertEqual(Observation.objects.filter(
            id=observation_to_destroy.id).count(), 0)

    def test_destroy_non_existing_observation_returns_http_code_404(self):
        """
        Test destroy a non existing observation must return a http code 400
        """
        non_existing_observation_id_to_destroy = 999

        response = self.client.delete(
            reverse.reverse(
                "observation-detail", args=[non_existing_observation_id_to_destroy]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_destroy_on_open_inventory_inventory_list_returns_http_code_204(self):
        """
        Test destroy an observation on open inventory/inventorylist must return a http code 204
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list, status=Status.OPEN)
        observation_to_destroy = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        response = self.client.delete(
            reverse.reverse(
                "observation-detail", args=[observation_to_destroy.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_destroy_on_open_inventory_inventory_list_really_destroys_observation(self):
        """
        Test destroy an observation on open inventory/inventorylist must delete the observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list, status=Status.OPEN)
        observation_to_destroy = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        self.client.delete(
            reverse.reverse(
                "observation-detail", args=[observation_to_destroy.id]),
            format="json")

        self.assertEqual(Observation.objects.count(), 0)

    def test_destroy_on_open_inventory_closed_inventory_list_returns_http_code_400(self):
        """
        Test destroy an observation on open inventory
        and closed inventorylist must return a http code 400
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.CLOSED)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list, status=Status.OPEN)
        observation_to_destroy = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        response = self.client.delete(
            reverse.reverse(
                "observation-detail", args=[observation_to_destroy.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_destroy_on_open_inventory_closed_inventory_list_dont_destroys_observation(self):
        """
        Test destroy an observation on open inventory
        and closed inventorylist must delete the observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.CLOSED)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list, status=Status.OPEN)
        observation_to_destroy = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        self.client.delete(
            reverse.reverse(
                "observation-detail", args=[observation_to_destroy.id]),
            format="json")

        self.assertEqual(Observation.objects.count(), 1)

    def test_destroy_on_closed_inventory_open_inventory_list_returns_http_code_400(self):
        """
        Test destroy an observation on closed inventory
        and open inventorylist must return a http code 400
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list, status=Status.CLOSED)
        observation_to_destroy = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        response = self.client.delete(
            reverse.reverse(
                "observation-detail", args=[observation_to_destroy.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_destroy_on_closed_inventory_open_inventory_list_dont_destroys_observation(self):
        """
        Test destroy an observation on closed inventory
        and open inventorylist must delete the observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list, status=Status.CLOSED)
        observation_to_destroy = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        self.client.delete(
            reverse.reverse(
                "observation-detail", args=[observation_to_destroy.id]),
            format="json")

        self.assertEqual(Observation.objects.count(), 1)

    def test_destroy_on_open_inventory_archived_inventory_list_returns_http_code_400(self):
        """
        Test destroy an observation on open inventory
        and archived inventorylist must return a http code 400
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.ARCHIVED)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list, status=Status.OPEN)
        observation_to_destroy = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        response = self.client.delete(
            reverse.reverse(
                "observation-detail", args=[observation_to_destroy.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_destroy_on_open_inventory_archived_inventory_list_dont_destroys_observation(self):
        """
        Test destroy an observation on open inventory
        and archived inventorylist must delete the observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.ARCHIVED)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list, status=Status.OPEN)
        observation_to_destroy = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        self.client.delete(
            reverse.reverse(
                "observation-detail", args=[observation_to_destroy.id]),
            format="json")

        self.assertEqual(Observation.objects.count(), 1)

    def test_destroy_on_archived_inventory_open_inventory_list_returns_http_code_400(self):
        """
        Test destroy an observation on archived inventory
        and open inventorylist must return a http code 400
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list, status=Status.ARCHIVED)
        observation_to_destroy = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        response = self.client.delete(
            reverse.reverse(
                "observation-detail", args=[observation_to_destroy.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_destroy_on_archived_inventory_open_inventory_list_dont_destroys_observation(self):
        """
        Test destroy an observation on archived inventory
        and open inventorylist must delete the observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list, status=Status.ARCHIVED)
        observation_to_destroy = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)

        self.client.delete(
            reverse.reverse(
                "observation-detail", args=[observation_to_destroy.id]),
            format="json")

        self.assertEqual(Observation.objects.count(), 1)
