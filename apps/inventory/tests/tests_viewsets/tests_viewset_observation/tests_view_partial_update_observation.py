# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models.enums import Status, Alive, Spontaneous, Lifestage, Gender, Quantification
from core.models import Observation, Taxon, InventoryList, Inventory


class PartialUpdateObservationViewsetTestCase(APITestCase):
    """
    Test the partial update observation list view
    """

    @staticmethod
    def get_observation_data(new_observed_taxon):
        """
        Create a dic of observation data
        """
        return {
            "taxon": new_observed_taxon.id,
            "comment": "coucou les oiseaux",
            "observator_nickname": "jonnhy john john",
            "number_of_individuals": 50,
            "soil_coverage": 0.25,
            "alive": Alive.YES,
            "spontaneous": Spontaneous.YES,
            "gender": Gender.ALL,
            "lifestage": Lifestage.ALL,
            "quantification": Quantification.COUNT,
            "user_scientific_name": "Mentha piperita",
            "user_vernacular_name": "Menthe poivrée"
        }

    def test_partially_update_with_valid_data_returns_http_code_200(self):
        """
        Test partially update an observation with valid data must return a http code 200
        """
        observed_taxon = Taxon.objects.create(id=0)
        new_observed_taxon = Taxon.objects.create(id=1)
        related_inventory_list = InventoryList.objects.create()
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = self.get_observation_data(
            new_observed_taxon)

        response = self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partially_update_with_valid_data_returns_updated_data(self):
        """
        Test partially update an observation with valid data must return updated data
        """
        observed_taxon = Taxon.objects.create(id=0)
        new_observed_taxon = Taxon.objects.create(id=1)
        related_inventory_list = InventoryList.objects.create()
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = self.get_observation_data(
            new_observed_taxon)

        response = self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        self.assertEqual(observation_to_partially_update_data["taxon"],
                         response.data["taxon"])
        self.assertEqual(observation_to_partially_update_data["comment"],
                         response.data["comment"])
        self.assertEqual(observation_to_partially_update_data["observator_nickname"],
                         response.data["observator_nickname"])
        self.assertEqual(observation_to_partially_update_data["number_of_individuals"],
                         response.data["number_of_individuals"])
        self.assertEqual(observation_to_partially_update_data["soil_coverage"],
                         response.data["soil_coverage"])
        self.assertEqual(observation_to_partially_update_data["alive"],
                         response.data["alive"])
        self.assertEqual(observation_to_partially_update_data["spontaneous"],
                         response.data["spontaneous"])
        self.assertEqual(observation_to_partially_update_data["gender"],
                         response.data["gender"])
        self.assertEqual(observation_to_partially_update_data["lifestage"],
                         response.data["lifestage"])
        self.assertEqual(observation_to_partially_update_data["quantification"],
                         response.data["quantification"])
        self.assertEqual(observation_to_partially_update_data["user_scientific_name"],
                         response.data["user_scientific_name"])
        self.assertEqual(observation_to_partially_update_data["user_vernacular_name"],
                         response.data["user_vernacular_name"])

    def test_partially_update_with_valid_data_really_updates_data(self):
        """
        Test partially update an observation with valid data must update data in database
        """
        observed_taxon = Taxon.objects.create(id=0)
        new_observed_taxon = Taxon.objects.create(id=1)
        related_inventory_list = InventoryList.objects.create()
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = self.get_observation_data(
            new_observed_taxon)

        response = self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        updated_observation = Observation.objects.get(
            id=observation_to_partially_update.id)
        self.assertEqual(updated_observation.taxon.id,
                         response.data["taxon"])
        self.assertEqual(updated_observation.comment,
                         response.data["comment"])
        self.assertEqual(updated_observation.observator_nickname,
                         response.data["observator_nickname"])
        self.assertEqual(updated_observation.number_of_individuals,
                         response.data["number_of_individuals"])
        self.assertEqual(updated_observation.soil_coverage,
                         response.data["soil_coverage"])
        self.assertEqual(updated_observation.alive,
                         response.data["alive"])
        self.assertEqual(updated_observation.spontaneous,
                         response.data["spontaneous"])
        self.assertEqual(updated_observation.gender,
                         response.data["gender"])
        self.assertEqual(updated_observation.lifestage,
                         response.data["lifestage"])
        self.assertEqual(updated_observation.quantification,
                         response.data["quantification"])
        self.assertEqual(updated_observation.user_scientific_name,
                         response.data["user_scientific_name"])
        self.assertEqual(updated_observation.user_vernacular_name,
                         response.data["user_vernacular_name"])

    def test_partially_update_inventory_fk_dont_change_it(self):
        """
        Test partially update an observation related inventory must not change it
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create()
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list)
        unrelated_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = {
            "inventory": unrelated_inventory.id
        }

        self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        self.assertNotEqual(Observation.objects.get(
            id=observation_to_partially_update.id).inventory, unrelated_inventory.id)

    def test_partially_update_non_existing_returns_http_code_404(self):
        """
        Test partially update an non existing observation must return a http code 404
        """
        non_existing_observation_id = 999
        observation_to_partially_update_data = {
            "comment": "hello"
        }

        response = self.client.patch(
            reverse.reverse(
                "observation-detail", args=[non_existing_observation_id]),
            data={**observation_to_partially_update_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_partially_update_on_open_inventory_inventory_list_returns_http_code_200(self):
        """
        Test partially update an observation with valid data and
        open inventory/inventory list must return a http code 200
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.OPEN)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = {
            "comment": "coucou les amis"
        }

        response = self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partially_update_on_open_inventory_inventory_list_really_updates_data(self):
        """
        Test partially update an observation with valid data
        and open inventory/inventory list must update data in database
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.OPEN)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = {
            "comment": "coucou les titi",
        }

        response = self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        updated_observation = Observation.objects.get(
            id=observation_to_partially_update.id)
        self.assertEqual(updated_observation.comment,
                         response.data["comment"])

    def test_partially_update_on_open_inventory_archived_inventory_list_returns_http_code_400(self):
        """
        Test partially update an observation with valid data
        and open inventory and archived inventory list must return a http code 400
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.ARCHIVED)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.OPEN)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = {
            "comment": "coucou les amis"
        }

        response = self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partially_update_on_open_inventory_archived_inventory_list_dont_update(self):
        """
        Test partially update an observation with valid data
        and open inventory and archived inventory list dont update observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.ARCHIVED)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.OPEN)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = {
            "comment": "coucou les amis"
        }

        self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        updated_observation = Observation.objects.get(
            id=observation_to_partially_update.id)

        self.assertNotEqual(updated_observation.comment,
                            observation_to_partially_update_data["comment"])

    def test_partially_update_on_archived_inventory_open_inventory_list_returns_http_code_400(self):
        """
        Test partially update an observation with valid data
        and archived inventory and open inventory list must return a http code 400
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.ARCHIVED)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = {
            "comment": "coucou les amis"
        }

        response = self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partially_update_on_archived_inventory_open_inventory_list_dont_update(self):
        """
        Test partially update an observation with valid data
        and archived inventory and open inventory list dont update observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.ARCHIVED)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = {
            "comment": "coucou les amis"
        }

        self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        updated_observation = Observation.objects.get(
            id=observation_to_partially_update.id)

        self.assertNotEqual(updated_observation.comment,
                            observation_to_partially_update_data["comment"])

    def test_partially_update_on_open_inventory_closed_inventory_list_returns_http_code_400(self):
        """
        Test partially update an observation with valid data
        and open inventory and closed inventory list must return a http code 400
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.CLOSED)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.OPEN)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = {
            "comment": "coucou les amis"
        }

        response = self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partially_update_on_open_inventory_closed_inventory_list_dont_update(self):
        """
        Test partially update an observation with valid data
        and open inventory and closed inventory list dont update observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.CLOSED)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.OPEN)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = {
            "comment": "coucou les amis"
        }

        self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        updated_observation = Observation.objects.get(
            id=observation_to_partially_update.id)

        self.assertNotEqual(updated_observation.comment,
                            observation_to_partially_update_data["comment"])

    def test_partially_update_on_closed_inventory_open_inventory_list_returns_http_code_400(self):
        """
        Test partially update an observation with valid data
        and closed inventory and open inventory list must return a http code 400
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.CLOSED)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = {
            "comment": "coucou les amis"
        }

        response = self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partially_update_on_closed_inventory_open_inventory_list_dont_update(self):
        """
        Test partially update an observation with valid data
        and closed inventory and open inventory list dont update observation
        """
        observed_taxon = Taxon.objects.create(id=0)
        related_inventory_list = InventoryList.objects.create(
            status=Status.OPEN)
        related_inventory = Inventory.objects.create(
            inventory_list=related_inventory_list,
            status=Status.CLOSED)
        observation_to_partially_update = Observation.objects.create(
            inventory=related_inventory, taxon=observed_taxon)
        observation_to_partially_update_data = {
            "comment": "coucou les amis"
        }

        self.client.patch(
            reverse.reverse(
                "observation-detail", args=[observation_to_partially_update.id]),
            data={**observation_to_partially_update_data},
            format="json")

        updated_observation = Observation.objects.get(
            id=observation_to_partially_update.id)

        self.assertNotEqual(updated_observation.comment,
                            observation_to_partially_update_data["comment"])
