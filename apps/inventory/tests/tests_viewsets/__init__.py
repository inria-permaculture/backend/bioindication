"""
Test classes for viewsets
"""

from inventory.tests.tests_viewsets.tests_mixins import *
from inventory.tests.tests_viewsets.tests_viewset_inventory import *
from inventory.tests.tests_viewsets.tests_viewset_inventory_list import *
from inventory.tests.tests_viewsets.tests_viewset_observation import *
from inventory.tests.tests_viewsets.tests_viewset_uri import *
