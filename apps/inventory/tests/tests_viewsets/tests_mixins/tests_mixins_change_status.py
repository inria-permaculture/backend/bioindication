# pylint: disable=missing-module-docstring
from datetime import datetime
from django.test import TestCase
from django.utils import timezone
from core.models.enums import Status
from inventory.viewsets.mixins.mixins_change_status import (
    check_status_transition_is_valid, get_deletion_date)


class ChangeStatusInventoryMixinsTestCase(TestCase):
    """
    Test the change status mixins functions
    The actions are tested in the views that integrate them
    """

    def test_check_transition_target_open_when_state_is_open_returns_false(self):
        """
        Test check transition for target open with state open must return false
        """
        transition_is_valid = check_status_transition_is_valid(
            Status.OPEN, Status.OPEN)
        self.assertEqual(transition_is_valid, False)

    def test_check_transition_target_open_when_state_is_closed_returns_true(self):
        """
        Test check transition for target open with state closed must return true
        """
        transition_is_valid = check_status_transition_is_valid(
            Status.CLOSED, Status.OPEN)
        self.assertEqual(transition_is_valid, True)

    def test_check_transition_target_open_when_state_is_archived_returns_false(self):
        """
        Test check transition for target open with state archived must return false
        """
        transition_is_valid = check_status_transition_is_valid(
            Status.ARCHIVED, Status.OPEN)
        self.assertEqual(transition_is_valid, False)

    def test_check_transition_target_closed_when_state_is_open_returns_true(self):
        """
        Test check transition for target closed with state open must return true
        """
        transition_is_valid = check_status_transition_is_valid(
            Status.OPEN, Status.CLOSED)
        self.assertEqual(transition_is_valid, True)

    def test_check_transition_target_closed_when_state_is_closed_returns_false(self):
        """
        Test check transition for target closed with state closed must return false
        """
        transition_is_valid = check_status_transition_is_valid(
            Status.CLOSED, Status.CLOSED)
        self.assertEqual(transition_is_valid, False)

    def test_check_transition_target_closed_when_state_is_archived_returns_true(self):
        """
        Test check transition for target closed with state archived must return true
        """
        transition_is_valid = check_status_transition_is_valid(
            Status.ARCHIVED, Status.CLOSED)
        self.assertEqual(transition_is_valid, True)

    def test_check_transition_target_archived_when_state_is_open_returns_true(self):
        """
        Test check transition for target archived with state open must return true
        """
        transition_is_valid = check_status_transition_is_valid(
            Status.OPEN, Status.ARCHIVED)
        self.assertEqual(transition_is_valid, True)

    def test_check_transition_target_archived_when_state_is_closed_returns_true(self):
        """
        Test check transition for target archived with state closed must return true
        """
        transition_is_valid = check_status_transition_is_valid(
            Status.CLOSED, Status.ARCHIVED)
        self.assertEqual(transition_is_valid, True)

    def test_check_transition_target_archived_when_state_is_archived_returns_false(self):
        """
        Test check transition for target archived with state archived must return false
        """
        transition_is_valid = check_status_transition_is_valid(
            Status.ARCHIVED, Status.ARCHIVED)
        self.assertEqual(transition_is_valid, False)

    def test_check_transition_target_random_value_returns_false(self):
        """
        Test check transition for target random must return false
        """
        transition_is_valid = check_status_transition_is_valid(
            Status.OPEN, "randomNonStatusValue")
        self.assertEqual(transition_is_valid, False)

    def test_when_state_is_random_value_returns_false(self):
        """
        Test check transition with a random value as state must return false
        """
        transition_is_valid = check_status_transition_is_valid("randomNonStatusValue",
                                                               Status.OPEN)
        self.assertEqual(transition_is_valid, False)

    def test_get_deletion_date_returns_a_valid_date(self):
        """
        Test get date must return a date
        """
        deletion_date = get_deletion_date()
        self.assertTrue(isinstance(deletion_date, datetime))

    def test_get_deletion_date_returns_a_future_date(self):
        """
        Test get date must return a date in the future
        """
        deletion_date = get_deletion_date()
        current_date = timezone.now()
        self.assertGreater(deletion_date, current_date)
