"""
Test the inventory serializer classes and related
"""
from django.test import TestCase
from inventory.serializers.validators import InventoryListValidators


class InventoryValidatorsTestCase(TestCase):
    """
    Test the inventory serializer fields validators
    """

    def test_validate_name_with_simple_string_returns_same_string(self):
        """
        Test validating a simple string as name without any
        character requiring to be escape must return the same string
        """
        simple_string = "test"

        simple_string_validated = InventoryListValidators.validate_name(
            self, value=simple_string)
        self.assertEqual(simple_string_validated, simple_string)

    def test_validate_name_with_dangerous_string_returns_escaped_string(self):
        """
        Test validating a dangerous string as name must return an escaped string
        """
        string_that_need_some_escaping = "<script/>"
        string_that_need_some_escaping_validated = InventoryListValidators.validate_name(
            self, value=string_that_need_some_escaping)
        self.assertEqual(
            string_that_need_some_escaping_validated, "&lt;script/&gt;")

    def test_validate_description_with_simple_string_returns_same_string(self):
        """
        Test validating a simple string as description without any
        character requiring to be escape must return the same string
        """
        simple_string = "test"

        simple_string_validated = InventoryListValidators.validate_description(
            self, value=simple_string)
        self.assertEqual(simple_string_validated, simple_string)

    def test_validate_description_with_dangerous_string_returns_escaped_string(self):
        """
        Test validating a dangerous string as description must return an escaped string
        """
        string_that_need_some_escaping = "<script/>"
        string_that_need_some_escaping_validated = InventoryListValidators.validate_description(
            self, value=string_that_need_some_escaping)
        self.assertEqual(
            string_that_need_some_escaping_validated, "&lt;script/&gt;")
