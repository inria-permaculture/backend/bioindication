"""
Test the uri serializer classes and related
"""
from django.test import TestCase
from inventory.serializers.validators import UriValidators


class UriValidatorsTestCase(TestCase):
    """
    Test the uri serializer fields validators
    """

    def test_validate_identifier_with_simple_string_returns_same_string(self):
        """
        Test validating a simple string as identifier without any
        character requiring to be escape must return the same string
        """
        simple_string = "test"

        simple_string_validated = UriValidators.validate_identifier(
            self, value=simple_string)
        self.assertEqual(simple_string_validated, simple_string)

    def test_validate_identifier_with_dangerous_string_returns_escaped_string(self):
        """
        Test validating a dangerous string as identifier must return an escaped string
        """
        string_that_need_some_escaping = "<script/>"
        string_that_need_some_escaping_validated = UriValidators.validate_identifier(
            self, value=string_that_need_some_escaping)
        self.assertEqual(
            string_that_need_some_escaping_validated, "&lt;script/&gt;")
