"""
Test classes for serializers
"""

from inventory.tests.tests_serializers.tests_serializers_inventory_list import *
from inventory.tests.tests_serializers.tests_serializers_inventory import *
from inventory.tests.tests_serializers.tests_serializers_observation import *
from inventory.tests.tests_serializers.tests_serializers_uri import *
