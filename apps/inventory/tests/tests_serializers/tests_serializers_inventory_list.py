"""
Test the inventory list serializer classes and related
"""
from django.test import TestCase, TransactionTestCase
from django.db import IntegrityError
from core.models import InventoryList, Inventory, Uri
from inventory.serializers import CreateInventoryListSerializer
from inventory.serializers.validators import InventoryListValidators


class InventoryListValidatorsTestCase(TestCase):
    """
    Test the inventory list serializer fields validators
    """

    def test_validate_name_with_simple_string_returns_same_string(self):
        """
        Test validating a simple string as name without any character requiring to be escaped
        must return the same string
        """
        simple_string = "test"

        simple_string_validated = InventoryListValidators.validate_name(
            self, value=simple_string)
        self.assertEqual(simple_string_validated, simple_string)

    def test_validate_name_with_dangerous_string_returns_escaped_string(self):
        """
        Test validating a dangerous string as name must return an escaped string
        """
        dangerous_string = "<script/>"
        dangerous_string_validated = InventoryListValidators.validate_name(
            self, value=dangerous_string)
        self.assertEqual(
            dangerous_string_validated, "&lt;script/&gt;")

    def test_validate_description_with_simple_string_returns_same_string(self):
        """
        Test validating a simple string as description without any character requiring to be escaped
        must return the same string
        """
        simple_string = "test"

        simple_string_validated = InventoryListValidators.validate_description(
            self, value=simple_string)
        self.assertEqual(simple_string_validated, simple_string)

    def test_validate_description_with_dangerous_string_returns_escaped_string(self):
        """
        Test validating a dangerous string as description must return an escaped string
        """
        dangerous_string = "<script/>"
        dangerous_string_validated = InventoryListValidators.validate_description(
            self, value=dangerous_string)
        self.assertEqual(
            dangerous_string_validated, "&lt;script/&gt;")


class CreateInventoryListSerializerTestCase(TransactionTestCase):
    """
    Test the inventory list serializer used to create a new entry
    Inherit TransactionTestCase to allow testing db transactions
    """

    def test_create_with_an_inventory_and_uri_create_them(self):
        """
        Test creating an inventory list + inventory + uri must create all of them
        """
        validated_data = {
            "name": "MalisteInventaire",
            "description": "Une liste pas comme les autreuhs",
            "uri": {
                "identifier": "myinventoriees"
            },
            "inventories": [
                {
                    "name": "Inventaire Jardin",
                    "description": "Le jardin en biodiversitéay"
                }
            ]
        }
        CreateInventoryListSerializer.create(
            self, validated_data)
        self.assertEqual(InventoryList.objects.all().count(), 1)
        self.assertEqual(Inventory.objects.all().count(), 1)
        self.assertEqual(Uri.objects.all().count(), 1)

    def test_create_with_an_uri_and_many_inventories_create_them(self):
        """
        Test creating an inventory list + inventories + uri  must create all of them
        """
        validated_data = {
            "name": "MalisteInventaire",
            "description": "Une liste pas comme les autreés",
            "uri": {
                "identifier": "myinventoriezs"
            },
            "inventories": [
                {
                    "name": "Inventaire Jardin",
                    "description": "Le jardin en biodiversitay"
                },
                {
                    "name": "Inventaire Jardin 2",
                    "description": "Le jardin en biodiversité 2"
                }
            ]
        }
        CreateInventoryListSerializer.create(
            self, validated_data)
        self.assertEqual(InventoryList.objects.all().count(), 1)
        self.assertEqual(Inventory.objects.all().count(), 2)
        self.assertEqual(Uri.objects.all().count(), 1)

    def test_create_same_inventory_list_twice_create_only_one(self):
        """
        Test creating an inventory list + inventory + uri twice
        And validate entities are created in db only once
        """
        validated_data = {
            "name": "MalisteInventaire",
            "description": "Une liste pas comme les autreas",
            "uri": {
                "identifier": "myinventoriets"
            },
            "inventories": [
                {
                    "name": "Inventaire Jardin",
                    "description": "Le jardin en biodiversity"
                }
            ]
        }

        # Duplicate of validated_data
        redundant_validated_data = {
            "name": "MalisteInventaire",
            "description": "Une liste pas comme les autress",
            "uri": {
                "identifier": "myinventoriesvs"
            },
            "inventories": [
                {
                    "name": "Inventaire Jardin",
                    "description": "Le jardin en biodiversityé"
                }
            ]
        }

        # Create an inventory list, with an uri and inventory
        CreateInventoryListSerializer.create(
            self, validated_data)

        # Try creating the same inventory list and related twice
        # To force throwing an exception in the create function
        try:
            CreateInventoryListSerializer.create(
                self, redundant_validated_data)
        except IntegrityError:
            # Validate entities are added in db only one time
            self.assertEqual(InventoryList.objects.all().count(), 1)
            self.assertEqual(Inventory.objects.all().count(), 1)
            self.assertEqual(Uri.objects.all().count(), 1)
