"""
Test the observation serializer classes and related
"""
from django.test import TestCase
from inventory.serializers.validators import ObservationValidators


class ObservationValidatorsTestCase(TestCase):
    """
    Test the observation serializer fields validators
    """

    def test_validate_name_with_simple_string_returns_same_string(self):
        """
        Test validating a simple string as name without any
        character requiring to be escape must return the same string
        """
        simple_string = "test"

        simple_string_validated = ObservationValidators.validate_name(
            self, value=simple_string)
        self.assertEqual(simple_string_validated, simple_string)

    def test_validate_name_with_dangerous_string_returns_escaped_string(self):
        """
        Test validating a dangerous string as name must return an escaped string
        """
        string_that_need_some_escaping = "<script/>"
        string_that_need_some_escaping_validated = ObservationValidators.validate_name(
            self, value=string_that_need_some_escaping)
        self.assertEqual(
            string_that_need_some_escaping_validated, "&lt;script/&gt;")

    def test_validate_comment_with_simple_string_returns_same_string(self):
        """
        Test validating a simple string as comment without any
        character requiring to be escape must return the same string
        """
        simple_string = "test"

        simple_string_validated = ObservationValidators.validate_comment(
            self, value=simple_string)
        self.assertEqual(simple_string_validated, simple_string)

    def test_validate_comment_with_dangerous_string_returns_escaped_string(self):
        """
        Test validating a dangerous string as comment must return an escaped string
        """
        string_that_need_some_escaping = "<script/>"
        string_that_need_some_escaping_validated = ObservationValidators.validate_comment(
            self, value=string_that_need_some_escaping)
        self.assertEqual(
            string_that_need_some_escaping_validated, "&lt;script/&gt;")

    def test_validate_observator_nickname_with_simple_string_returns_same_string(self):
        """
        Test validating a simple string as observator_nickname without any
        character requiring to be escape must return the same string
        """
        simple_string = "test"

        simple_string_validated = ObservationValidators.validate_observator_nickname(
            self, value=simple_string)
        self.assertEqual(simple_string_validated, simple_string)

    def test_validate_observator_nickname_with_dangerous_string_returns_escaped_string(self):
        """
        Test validating a dangerous string as observator_nickname
        must return an escaped string
        """
        string_that_need_some_escaping = "<script/>"
        string_that_need_some_escaping_validated = (
            ObservationValidators.validate_observator_nickname(
                self, value=string_that_need_some_escaping))
        self.assertEqual(
            string_that_need_some_escaping_validated, "&lt;script/&gt;")
