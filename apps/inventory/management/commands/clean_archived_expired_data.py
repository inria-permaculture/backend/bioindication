"""
Delete archived and expired inventory lists and inventories
This command is designed to be scheduled for automatic cleaning
You can redirect stdout to a file to keep track of deletions/errors during exec
"""

from django.utils import timezone
from django.core.management.base import BaseCommand
from core.models import InventoryList, Inventory, Status


class Command(BaseCommand):
    """Delete archived and expired inventory lists and inventories"""
    help = "Delete archived and expired data"

    def handle(self, *args, **options):
        print(
            f"Starting clean_archived_and_expired_data command {timezone.now()}")

        nb_inventory_list_destroyed = self.__clean_archived_and_expired_data(
            InventoryList.objects.all())

        nb_inventory_destroyed = self.__clean_archived_and_expired_data(
            Inventory.objects.all())

        print((
            f"Finishing clean_archived_and_expired_data command {timezone.now()}. "
            f"Total inventory lists destroyed {nb_inventory_list_destroyed} and "
            f"total inventories destroyed {nb_inventory_destroyed}"))

    @staticmethod
    def __clean_archived_and_expired_data(queryset):
        # pylint: disable=broad-except
        """
        Take a queryset of items(inventory list or inventory) with status and expiry_date fields
        Remove from database all of them with status = archived and an outdated expiry_date
        """
        # filter archived and outdated items
        data = queryset.filter(
            status=Status.ARCHIVED,
            expiry_date__lte=timezone.now())
        count_items_destroyed = 0
        for item in data:
            try:
                item_id = item.id
                item_name = item.name
                item.delete()
                # Increment total number of items destroyed
                count_items_destroyed += 1
                # display a message to keep a trace about deleted items
                print(f"{queryset.__str__()} {item_id} {item_name} deleted")

            # if an error occur, just log it
            except Exception as error:
                print(
                    f"Error deleting inventory list {item.id}, error {error} occur")

            return count_items_destroyed
