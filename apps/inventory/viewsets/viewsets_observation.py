# pylint: disable=too-many-ancestors,missing-module-docstring
from rest_framework import viewsets, mixins
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from drf_spectacular.utils import extend_schema, extend_schema_view, OpenApiResponse
from core.models import Observation
from core.utils import http_codes_description
from inventory.serializers import (
    ObservationSerializer, PartialUpdateObservationSerializer)
from inventory.viewsets.mixins import (RELATED_INVENTORY_IS_ARCHIVED_OR_CLOSED,
                                       does_state_allow_create_update_delete,
                                       RELATED_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED)


# Defining view serializers mapping
# Most views have the same input/output serializer, or only output
# But some of them have a different input serializer
OBSERVATION_VIEW_SERIALIZER_MAPPING_DIC = {
    "default":
    {
        "create": ObservationSerializer,
        "retrieve": ObservationSerializer,
        "destroy": None,
        "partial_update": ObservationSerializer,
    },
    "extended-input":
    {
        "partial_update": PartialUpdateObservationSerializer,
    }
}


# Generic and specific actions have an extended open api schema by
# using the extend_schema_view decorator on top of the related viewset
@extend_schema_view(create=extend_schema(responses={
    201: OpenApiResponse(response=OBSERVATION_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("create"),
                         description=http_codes_description.CREATED),
    400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(retrieve=extend_schema(responses={
    200: OpenApiResponse(OBSERVATION_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("retrieve"),
                         description=http_codes_description.SUCCESS),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(destroy=extend_schema(responses={
    204: OpenApiResponse(description=http_codes_description.SUCCESS_NO_CONTENT),
    400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(partial_update=extend_schema(
    request=OBSERVATION_VIEW_SERIALIZER_MAPPING_DIC
    .get("extended-input").get("partial_update"),
    responses={200: OpenApiResponse(response=OBSERVATION_VIEW_SERIALIZER_MAPPING_DIC
                                    .get("default").get("partial_update"),
                                    description=http_codes_description.SUCCESS),
               400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
               404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}
))
class ObservationViewSet(
        mixins.CreateModelMixin,
        mixins.RetrieveModelMixin,
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet):
    """
    Views related to observations manipulations
    """
    default_serializer = ObservationSerializer
    queryset = Observation.objects.all()

    def get_serializer_class(self):
        """
        Override the GenericAPIView method
        Get the serializer used for generics actions
        The selected serializer is detected as input and output serializer by drf-specatcular
        For specifics actions, input/output serializers are defined inside the method
        and with the extend_schema decorator
        """
        response_serializer_dic = OBSERVATION_VIEW_SERIALIZER_MAPPING_DIC.get(
            "default")
        selected_serializer = response_serializer_dic.get(
            self.action, self.default_serializer)
        return selected_serializer

    def perform_create(self, serializer):
        """
        Override the CreateModelMixin method
        Allow creating observation only if it"s inventory and
        inventory list parent have the good status
        """
        inventory_status = serializer.validated_data["inventory"].status
        if not does_state_allow_create_update_delete(inventory_status):
            raise ValidationError(RELATED_INVENTORY_IS_ARCHIVED_OR_CLOSED)
        inventory_list_status = serializer.validated_data["inventory"].inventory_list.status
        if not does_state_allow_create_update_delete(inventory_list_status):
            raise ValidationError(RELATED_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED)
        serializer.save()

    def perform_destroy(self, instance):
        """
        Override the DestroyModelMixin method
        Allow deleting observation only if it"s inventory and
        inventory list parent have the good status
        """
        if not does_state_allow_create_update_delete(instance.inventory.status):
            raise ValidationError(RELATED_INVENTORY_IS_ARCHIVED_OR_CLOSED)
        if (not
                does_state_allow_create_update_delete(instance.inventory.inventory_list.status)):
            raise ValidationError(RELATED_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED)
        instance.delete()

    def partial_update(self, request, pk):
        """
        Partially update an observation instance
        Offer a way to use different serializer as input and output
        Unused argument pk but still define because it"s passed by default by the caller
        """
        # pylint: disable=unused-argument
        instance = self.get_object()
        if not does_state_allow_create_update_delete(instance.inventory.status):
            raise ValidationError(RELATED_INVENTORY_IS_ARCHIVED_OR_CLOSED)
        if (not
                does_state_allow_create_update_delete(instance.inventory.inventory_list.status)):
            raise ValidationError(RELATED_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED)
        serializer = OBSERVATION_VIEW_SERIALIZER_MAPPING_DIC.get("extended-input").get(
            "partial_update")(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        complete_observation_data_serializer = self.get_serializer(instance)
        return Response(complete_observation_data_serializer.data)
