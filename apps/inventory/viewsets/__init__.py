"""
Viewsets to combine views into api endpoints
"""

from inventory.viewsets.viewsets_inventory_list import *
from inventory.viewsets.viewsets_inventory import *
from inventory.viewsets.viewsets_observation import *
from inventory.viewsets.viewsets_uri import *
from inventory.viewsets.mixins import *
