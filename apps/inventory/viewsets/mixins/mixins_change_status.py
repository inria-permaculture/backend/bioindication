# pylint: disable=missing-module-docstring,unused-argument
from datetime import timedelta
from django.utils import timezone
from django.conf import settings
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from drf_spectacular.utils import extend_schema
from core.models.enums import Status


# Status-related error messages returned by views
CANT_BE_OPEN = "Ne peut pas être mis en état ouvert"
CANT_BE_CLOSED = "Ne peut pas être mis en état fermé"
CANT_BE_ARCHIVED = "Ne peut pas être mis en état archivé"
THIS_INVENTORY_IS_ARCHIVED_OR_CLOSED = "Cet inventaire est archivé ou fermé"
THIS_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED = "Cette liste d'inventaires est archivée ou fermée"
RELATED_INVENTORY_IS_ARCHIVED_OR_CLOSED = "L'inventaire associée est archivé ou fermé"
RELATED_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED = "La liste d'inventaires associée est archivée \
    ou fermée"


class ChangeStatusMixin:
    """
    Add generic actions to operate on the status field
    Allow switching status to OPEN | CLOSED | ARCHIVED state
    """

    @extend_schema(
        request=None,
    )
    @action(methods=["post"], detail=True, url_path="open", url_name="open")
    def set_status_open(self, request, pk):
        """Set the status of an entity as open"""
        queryset = self.get_object()
        if check_status_transition_is_valid(queryset.status, Status.OPEN):
            queryset.status = Status.OPEN
            queryset.expiry_date = None
            queryset.full_clean()
            queryset.save()
        else:
            return Response(CANT_BE_OPEN,
                            status.HTTP_400_BAD_REQUEST)
        serializer = self.get_serializer(queryset)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        request=None,
    )
    @action(methods=["post"], detail=True, url_path="close", url_name="close")
    def set_status_closed(self, request, pk):
        """Set the status of an entity as closed"""
        queryset = self.get_object()
        if check_status_transition_is_valid(queryset.status, Status.CLOSED):
            queryset.status = Status.CLOSED
            queryset.expiry_date = None
            queryset.full_clean()
            queryset.save()
        else:
            return Response(CANT_BE_CLOSED,
                            status.HTTP_400_BAD_REQUEST)
        serializer = self.get_serializer(queryset)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        request=None,
    )
    @action(methods=["post"], detail=True, url_path="archive", url_name="archive")
    def set_status_archived(self, request, pk):
        """Set the status of an entity as archived"""
        queryset = self.get_object()
        if check_status_transition_is_valid(queryset.status, Status.ARCHIVED):
            queryset.status = Status.ARCHIVED
            queryset.expiry_date = get_deletion_date()
            queryset.full_clean()
            queryset.save()
        else:
            return Response(CANT_BE_ARCHIVED,
                            status.HTTP_400_BAD_REQUEST)
        serializer = self.get_serializer(queryset)
        return Response(serializer.data, status=status.HTTP_200_OK)


def check_status_transition_is_valid(old_status, target_status):
    """
    Manage the status state machine transitions
    """
    if target_status == Status.OPEN:
        if old_status == Status.CLOSED:
            return True
    if target_status == Status.CLOSED:
        if old_status in (Status.OPEN, Status.ARCHIVED):
            return True
    if target_status == Status.ARCHIVED:
        if old_status in (Status.OPEN, Status.CLOSED):
            return True
    return False


def get_deletion_date():
    """
    Get a date at which deletion is allowed on the target object
    The duration in days is defined in the settings.py file
    """
    current_date = timezone.now()
    deletion_date = current_date + \
        timedelta(
            days=settings.CGB["DAYS_BEFORE_ARCHIVED_ITEM_IS_DELETED"])
    return deletion_date


def does_state_allow_create_update_delete(state_to_test):
    """
    Check if the given state allow performing
    create, update and delete operations
    """
    if state_to_test in (Status.ARCHIVED, Status.CLOSED):
        return False
    return True
