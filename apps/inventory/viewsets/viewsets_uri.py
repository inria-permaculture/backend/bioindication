# pylint: disable=missing-module-docstring
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from drf_spectacular.utils import extend_schema, extend_schema_view, OpenApiResponse
from core.models import Uri
from core.utils import http_codes_description
from inventory.serializers import PartialUpdateUriSerializer, UriSerializer
from inventory.viewsets.mixins import (does_state_allow_create_update_delete,
                                       RELATED_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED)


# Defining view serializers mapping
# Most views have the same input/output serializer, or only output
# But some of them have a different input serializer
URI_VIEW_SERIALIZER_MAPPING_DIC = {
    "default":
    {
        "partial_update": UriSerializer,
    },
    "extended-input":
    {
        "partial_update": PartialUpdateUriSerializer,
    },
}


# Generic and specific actions have an extended open api schema by
# using the extend_schema_view decorator on top of the related viewset
@extend_schema_view(partial_update=extend_schema(
    request=URI_VIEW_SERIALIZER_MAPPING_DIC.get("extended-input")
    .get("partial_update"),
    responses={
        200: OpenApiResponse(response=URI_VIEW_SERIALIZER_MAPPING_DIC.
                             get("default").get("partial_update"),
                             description=http_codes_description.SUCCESS),
        400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
        404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
class UriViewSet(viewsets.GenericViewSet):
    """
    Views related to uris manipulations
    """
    default_serializer = UriSerializer
    queryset = Uri.objects.all()

    def get_serializer_class(self):
        """
        Override the GenericAPIView method
        Get the serializer used for generics actions
        The selected serializer is detected as input and output serializer by drf-specatcular
        For specifics actions, input/output serializers are defined inside the method
        and with the extend_schema decorator
        """
        response_serializer_dic = URI_VIEW_SERIALIZER_MAPPING_DIC.get(
            "default")
        selected_serializer = response_serializer_dic.get(
            self.action, self.default_serializer)
        return selected_serializer

    def partial_update(self, request, *args, **kwargs):
        # pylint: disable=unused-argument
        """
        Partially update an uri instance
        Offer a way to use different serializer as input and output
        """
        instance = self.get_object()
        if not does_state_allow_create_update_delete(instance.inventory_list.status):
            raise ValidationError(RELATED_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED)
        serializer = URI_VIEW_SERIALIZER_MAPPING_DIC.get("extended-input").get(
            "partial_update")(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        complete_uri_data_serializer = self.get_serializer(instance)
        return Response(complete_uri_data_serializer.data)
