# pylint: disable=too-many-ancestors,missing-module-docstring
from rest_framework import viewsets, status, mixins
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from drf_spectacular.utils import (
    extend_schema, extend_schema_view, OpenApiParameter,  OpenApiResponse)
from core.models import InventoryList, Uri, Inventory
from core.utils import http_codes_description
from inventory.serializers import (
    InventoryListSerializer, UriSerializer, InventorySerializer,
    PartialUpdateInventoryListSerializer, CreateInventoryListSerializer)
from inventory.viewsets.mixins import (
    ChangeStatusMixin,
    does_state_allow_create_update_delete,
    THIS_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED)


# Defining view serializers mapping
# Most views have the same input/output serializer, or only output
# But some of them have a different input serializer
INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC = {
    "default":
    {
        "create": CreateInventoryListSerializer,
        "retrieve": InventoryListSerializer,
        "list": InventoryListSerializer,
        "set_status_open": InventoryListSerializer,
        "set_status_closed": InventoryListSerializer,
        "set_status_archived": InventoryListSerializer,
        "partial_update": InventoryListSerializer,
        "get_uri": UriSerializer,
        "get_inventories": InventorySerializer,
    },
    "extended-input":
    {
        "partial_update": PartialUpdateInventoryListSerializer,
    }
}


# Generic and specific actions have an extended open api schema by
# using the extend_schema_view decorator on top of the related viewset
@extend_schema_view(create=extend_schema(responses={
    201: OpenApiResponse(response=INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("create"),
                         description=http_codes_description.CREATED),
    400: OpenApiResponse(description=http_codes_description.BAD_REQUEST)}))
@extend_schema_view(retrieve=extend_schema(responses={
    200: OpenApiResponse(response=INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("retrieve"),
                         description=http_codes_description.SUCCESS),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(list=extend_schema(responses={
    200: OpenApiResponse(response=INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("list"),
                         description=http_codes_description.SUCCESS)},
    parameters=[OpenApiParameter(name="identifier")]))
@extend_schema_view(set_status_open=extend_schema(request=None, responses={
    200: OpenApiResponse(response=INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("set_status_open"),
                         description=http_codes_description.SUCCESS),
    400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(set_status_closed=extend_schema(request=None, responses={
    200: OpenApiResponse(response=INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("set_status_closed"),
                         description=http_codes_description.SUCCESS),
    400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(set_status_archived=extend_schema(request=None, responses={
    200: OpenApiResponse(response=INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("set_status_archived"),
                         description=http_codes_description.SUCCESS),
    400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(partial_update=extend_schema(
    request=INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC
    .get("extended-input").get("partial_update"),
    responses={
        200: OpenApiResponse(response=INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC
                             .get("default").get("partial_update"),
                             description=http_codes_description.SUCCESS),
        400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
        404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(get_uri=extend_schema(responses={
    200: OpenApiResponse(response=INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("get_uri"),
                         description=http_codes_description.SUCCESS),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(get_inventories=extend_schema(responses={
    200: OpenApiResponse(response=INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("get_inventories")(many=True),
                         description=http_codes_description.SUCCESS),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}
))
class InventoryListViewSet(ChangeStatusMixin,
                           mixins.CreateModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    """
    Views related to inventory lists manipulations
    """
    default_serializer = InventoryListSerializer
    queryset = InventoryList.objects.all()

    def get_serializer_class(self):
        """
        Override the GenericAPIView method
        Get the serializer used for generics actions
        The selected serializer is detected as input and output serializer by drf-specatcular
        For specifics actions, input/output serializers are defined inside the method
        and with the extend_schema decorator
        """
        response_serializer_dic = INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC.get(
            "default")
        selected_serializer = response_serializer_dic.get(
            self.action, self.default_serializer)
        return selected_serializer

    def get_queryset(self):
        # Return a list of inventory lists filtered by uri.identifier
        """
        Override the GenericAPIView method
        Get a list of inventory list
        Provide different list depending of the calling action
        """
        if self.action == "list":
            identifier = self.request.query_params.get("identifier")
            if identifier:
                return self.queryset.filter(
                    uri__identifier=identifier)
            return InventoryList.objects.none()
        return super().get_queryset()

    def partial_update(self, request, pk):
        # pylint: disable=unused-argument
        """
        Partially update an inventory list instance
        Offer a way to use different serializer as input and output
        Unused argument pk but still define because it"s passed by default by the caller
        """
        instance = self.get_object()
        if not does_state_allow_create_update_delete(instance.status):
            raise ValidationError(THIS_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED)
        serializer = INVENTORY_LIST_VIEW_SERIALIZER_MAPPING_DIC.get("extended-input").get(
            "partial_update")(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        complete_inventory_list_data_serializer = self.get_serializer(instance)
        return Response(complete_inventory_list_data_serializer.data)

    @action(methods=["get"], detail=True, url_path="uri", url_name="uri")
    def get_uri(self, request, pk):
        # pylint: disable=no-self-use,unused-argument
        """
        Retrieve inventory list"s uri
        """
        instance = self.get_object()
        try:
            queryset = Uri.objects.get(inventory_list=instance.id)
        except Uri.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = self.get_serializer(queryset)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=["get"], detail=True, url_path="inventories", url_name="inventories")
    def get_inventories(self, request, pk):
        # pylint: disable=no-self-use,unused-argument
        """
        Retrieve related inventory list"s inventories
        """
        instance = self.get_object()
        queryset = Inventory.objects.filter(inventory_list=instance.id)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
