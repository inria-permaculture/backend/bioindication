# pylint: disable=too-many-ancestors,missing-module-docstring
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from drf_spectacular.utils import extend_schema, extend_schema_view, OpenApiResponse
from core.models import Inventory, Observation
from core.utils import http_codes_description
from inventory.serializers import (
    InventorySerializer, ObservationSerializer, PartialUpdateInventorySerializer)
from inventory.viewsets.mixins import (ChangeStatusMixin,
                                       does_state_allow_create_update_delete,
                                       RELATED_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED,
                                       THIS_INVENTORY_IS_ARCHIVED_OR_CLOSED)


# Defining view serializers mapping
# Most views have the same input/output serializer, or only output
# But some of them have a different input serializer
INVENTORY_VIEW_SERIALIZER_MAPPING_DIC = {
    "default":
    {
        "create": InventorySerializer,
        "retrieve": InventorySerializer,
        "set_status_open": InventorySerializer,
        "set_status_closed": InventorySerializer,
        "set_status_archived": InventorySerializer,
        "partial_update": InventorySerializer,
        "get_observations": ObservationSerializer,
        "get_publicly_visible_inventories": InventorySerializer,
    },
    "extended-input":
    {
        "partial_update": PartialUpdateInventorySerializer,
    }
}


# Generic and specific actions have an extended open api schema by
# using the extend_schema_view decorator on top of the related viewset
@extend_schema_view(create=extend_schema(responses={
    201: OpenApiResponse(response=INVENTORY_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("create"),
                         description=http_codes_description.CREATED),
    400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(retrieve=extend_schema(responses={
    200: OpenApiResponse(response=INVENTORY_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("retrieve"),
                         description=http_codes_description.SUCCESS),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(set_status_open=extend_schema(request=None, responses={
    200: OpenApiResponse(response=INVENTORY_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("set_status_open"),
                         description=http_codes_description.SUCCESS),
    400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(set_status_closed=extend_schema(request=None, responses={
    200: OpenApiResponse(response=INVENTORY_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("set_status_closed"),
                         description=http_codes_description.SUCCESS),
    400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(set_status_archived=extend_schema(request=None, responses={
    200: OpenApiResponse(response=INVENTORY_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("set_status_archived"),
                         description=http_codes_description.SUCCESS),
    400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(partial_update=extend_schema(
    request=INVENTORY_VIEW_SERIALIZER_MAPPING_DIC
    .get("extended-input").get("partial_update"),
    responses={
        200: OpenApiResponse(response=INVENTORY_VIEW_SERIALIZER_MAPPING_DIC
                             .get("default").get("partial_update"),
                             description=http_codes_description.SUCCESS),
        400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
        404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(get_observations=extend_schema(request=None, responses={
    200: OpenApiResponse(response=INVENTORY_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default"). get("get_observations")(many=True),
                         description=http_codes_description.SUCCESS),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(get_publicly_visible_inventories=extend_schema(request=None, responses={
    200: OpenApiResponse(response=INVENTORY_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default"). get("get_publicly_visible_inventories")(many=True),
                         description=http_codes_description.SUCCESS), }))
class InventoryViewSet(ChangeStatusMixin,
                       mixins.CreateModelMixin,
                       mixins.RetrieveModelMixin,
                       viewsets.GenericViewSet):
    """
    Views related to inventories manipulations
    """
    default_serializer = InventorySerializer
    queryset = Inventory.objects.all()

    def get_serializer_class(self):
        """
        Override the GenericAPIView method
        Get the serializer used for generics actions
        The selected serializer is detected as input and output serializer by drf-specatcular
        For specifics actions, input/output serializers are defined inside the method
        and with the extend_schema decorator
        """
        response_serializer_dic = INVENTORY_VIEW_SERIALIZER_MAPPING_DIC.get(
            "default")
        selected_serializer = response_serializer_dic.get(
            self.action, self.default_serializer)
        return selected_serializer

    def perform_create(self, serializer):
        """
        Override the CreateModelMixin method
        Allow creating inventory only if it"s inventory list parent have the good status
        """
        inventory_list_status = serializer.validated_data["inventory_list"].status
        if not does_state_allow_create_update_delete(inventory_list_status):
            raise ValidationError(RELATED_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED)
        serializer.save()

    def partial_update(self, request, pk):
        # pylint: disable=unused-argument
        """
        Partially update an inventory instance
        Offer a way to use different serializer as input and output
        Unused argument pk but still define because it"s passed by default by the caller
        """
        instance = self.get_object()
        if not does_state_allow_create_update_delete(instance.status):
            raise ValidationError(THIS_INVENTORY_IS_ARCHIVED_OR_CLOSED)
        if not does_state_allow_create_update_delete(instance.inventory_list.status):
            raise ValidationError(RELATED_INVENTORY_LIST_IS_ARCHIVED_OR_CLOSED)
        serializer = INVENTORY_VIEW_SERIALIZER_MAPPING_DIC.get("extended-input").get(
            "partial_update")(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        complete_inventory_data_serializer = self.get_serializer(instance)
        return Response(complete_inventory_data_serializer.data)

    @action(methods=["get"], detail=True, url_path="observations", url_name="observations")
    def get_observations(self, request, pk):
        # pylint: disable=no-self-use, unused-argument
        """
        Retrieve related inventory's observations
        """
        instance = self.get_object()
        queryset = Observation.objects.filter(inventory=instance.pk)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=["get"], detail=False, url_path="public", url_name="public")
    def get_publicly_visible_inventories(self, request):
        # pylint: disable=no-self-use
        """
        Retrieve all publicly visible inventories
        """
        queryset = Inventory.objects.filter(is_publicly_visible=True)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
