# Bioindication - backend

## Architecture
If you want to understand this repository architecture, here the detailled folder structure :

    serializers -> Data transformations folder
    management -> Specifics commands implementations folder for manage.py
    tests -> Unit/Integration tests folder
    viewsets -> REST endpoints and related
    __init__.py -> Make this directory considered as a Python package
    urls.py -> The URL declarations for this Django application
