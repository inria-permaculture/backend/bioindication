"""
API endpoints routes definitions
"""
from rest_framework.routers import DefaultRouter
from inventory.viewsets import (
    InventoryListViewSet, InventoryViewSet, ObservationViewSet, UriViewSet)

router = DefaultRouter()

router.register(r"inventory-lists", InventoryListViewSet,
                basename="inventory-list")
router.register(r"inventories", InventoryViewSet, basename="inventory")
router.register(r"observations", ObservationViewSet, basename="observation")
router.register(r"uris", UriViewSet, basename="uri")

urlpatterns = router.urls
