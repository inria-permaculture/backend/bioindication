# pylint: disable=no-self-use,too-few-public-methods
"""
Uri fields validations
"""
from core.utils import escape_string


class UriValidators():
    """
    Field level validation used by uri serializers
    They are automatically discovered and called by the is_valid serializer method
    """

    def validate_identifier(self, value):
        """Escape identifier"""
        return escape_string(value)
