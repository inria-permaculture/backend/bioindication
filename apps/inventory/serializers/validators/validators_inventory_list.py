# pylint: disable=no-self-use
"""
Inventory list fields validations
"""
from core.utils import escape_string


class InventoryListValidators():
    """
    Field level validation used by inventory list serializers
    They are automatically discovered and called by the is_valid serializer method
    """

    def validate_name(self, value):
        """
        Escape name
        """
        return escape_string(value)

    def validate_description(self, value):
        """
        Escape description
        """
        return escape_string(value)
