# pylint: disable=no-self-use
"""
Observation fields validations
"""
from core.utils import escape_string


class ObservationValidators():
    """
    Field level validation used by observation serializers
    They are automatically discovered and called by the is_valid serializer method
    """

    def validate_name(self, value):
        """
        Escape name
        """
        return escape_string(value)

    def validate_comment(self, value):
        """
        Escape comment
        """
        return escape_string(value)

    def validate_observator_nickname(self, value):
        """
        Escape observator_nickname
        """
        return escape_string(value)
