# pylint: disable=no-self-use
"""
Inventory fields validations
"""
from core.utils import escape_string


class InventoryValidators():
    """
    Field level validation used by inventory serializers
    They are automatically discovered and called by the is_valid serializer method
    """

    def validate_name(self, value):
        """
        Escape name
        """
        return escape_string(value)

    def validate_description(self, value):
        """
        Escape description
        """
        return escape_string(value)
