# pylint: disable=no-member, no-self-use
"""
Uri model transformations
"""
from rest_framework import serializers
from core.models import Uri
from inventory.serializers.validators import UriValidators


class UriSerializer(serializers.ModelSerializer):
    """Uri serializer"""

    class Meta:
        model = Uri
        fields = "__all__"


class PartialUpdateUriSerializer(serializers.ModelSerializer, UriValidators):
    """Uri Identifier serializer"""

    class Meta:
        model = Uri
        fields = ("identifier",)


class CreateUriSerializer(serializers.ModelSerializer, UriValidators):
    """Uri Identifier serializer"""

    class Meta:
        model = Uri
        fields = ("id", "identifier",)
