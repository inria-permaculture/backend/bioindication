# pylint: disable=no-member, no-self-use
"""
Observation model transformations
"""
from rest_framework import serializers
from drf_spectacular.utils import extend_schema_field
from core.models import Observation
from inventory.serializers.validators import ObservationValidators


class ObservationSerializer(serializers.ModelSerializer, ObservationValidators):
    """
    Observation serializer
    """
    have_indicator_values = serializers.SerializerMethodField()

    @extend_schema_field(field=serializers.BooleanField())
    def get_have_indicator_values(self, obj):
        # pylint: disable=invalid-name
        """
        Indicate that the observed taxon has indicator value data
        """
        if getattr(obj.taxon, "indicatorvalues", False):
            return True
        return False

    class Meta:
        model = Observation
        fields = "__all__"


class PartialUpdateObservationSerializer(serializers.ModelSerializer, ObservationValidators):
    """
    Update Observation serializer
    """

    class Meta:
        model = Observation
        fields = ("taxon",  "comment",
                  "observator_nickname", "number_of_individuals",
                  "soil_coverage", "alive",
                  "spontaneous", "gender", "lifestage",
                  "quantification", "user_scientific_name",
                  "user_vernacular_name")
