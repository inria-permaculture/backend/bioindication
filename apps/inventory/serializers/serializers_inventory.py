# pylint: disable=no-member, no-self-use
"""
Inventory model transformations
"""
from rest_framework import serializers
from core.models import Inventory
from inventory.serializers.validators import InventoryValidators


class InventorySerializer(serializers.ModelSerializer):
    """
    Inventory serializer
    """

    class Meta:
        model = Inventory
        fields = "__all__"


class CreateInventorySerializerWithoutInventoryListBinding(
        serializers.ModelSerializer,
        InventoryValidators):
    """
    Inventory serializer for create action without inventory_list field
    """

    class Meta:
        model = Inventory
        fields = ("id", "name", "description")


class PartialUpdateInventorySerializer(serializers.ModelSerializer, InventoryValidators):
    """
    Inventory serializer for update action
    """

    class Meta:
        model = Inventory
        fields = ("name", "description", "is_publicly_visible",
                  "area", "coordinates")
