# pylint: disable=no-member, no-self-use
"""
Inventory list model transformations
"""
from django.db import transaction
from rest_framework import serializers
from core.models import InventoryList, Inventory, Uri
from inventory.serializers.serializers_uri import (CreateUriSerializer)
from inventory.serializers.serializers_inventory import (
    CreateInventorySerializerWithoutInventoryListBinding)
from inventory.serializers.validators import (
    InventoryListValidators)


class InventoryListSerializer(serializers.ModelSerializer):
    """
    InventoryList serializer
    """

    class Meta:
        model = InventoryList
        fields = "__all__"


class CreateInventoryListSerializer(serializers.ModelSerializer, InventoryListValidators):
    """
    Serializer to represent a future inventory list
    with uri and at least one inventory
    """
    uri = CreateUriSerializer(many=False, required=True)
    inventories = CreateInventorySerializerWithoutInventoryListBinding(
        many=True, required=True)

    class Meta:
        model = InventoryList
        fields = ("id", "name", "description", "uri", "inventories")

    def create(self, validated_data):
        """
        Create a new inventory list with his uri, and at least one inventory
        Rollback the db if part of the create logic failed
        """
        # Everything inside the atomic transaction is commit to
        # database only if no exception is raised. Otherwise everything is rollback
        with transaction.atomic():
            # extract uri data
            uri_data = validated_data.pop("uri")
            # extract inventories data
            inventories_data = validated_data.pop("inventories")
            # create new inventory list with remaining data
            inventory_list = InventoryList.objects.create(**validated_data)
            # create and bind uri to inventory list
            Uri.objects.create(inventory_list=inventory_list, **uri_data)
            # iterate over the inventories data
            for inventory_data in inventories_data:
                # create and bind new inventory
                Inventory.objects.create(
                    inventory_list=inventory_list, **inventory_data)
            return inventory_list


class PartialUpdateInventoryListSerializer(serializers.ModelSerializer,
                                           InventoryListValidators):
    """
    InventoryList serializer for update action
    """

    class Meta:
        model = InventoryList
        fields = ("name", "description")
