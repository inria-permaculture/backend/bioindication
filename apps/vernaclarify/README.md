# Bioindication - backend

## Architecture
If you want to understand this repository architecture, here the detailled folder structure :

    serializers -> Data transformations folder
    tests -> Unit tests folder
    views -> REST API functions folder
    __init__.py -> Make this directory considered as a Python package
    urls.py -> The URL declarations for this Django application
