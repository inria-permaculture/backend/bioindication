"""
Data models transformations
"""
# pylint: disable=R0201
from rest_framework import serializers
from core.models import ScientificEntry
from core.utils import escape_string


class ScientificSerializer(serializers.ModelSerializer):
    """Scientific serializer"""
    class Meta:
        model = ScientificEntry
        fields = ("id", "taxon", "name", "source", "comment")

    def validate_name(self, value):
        """Escape name"""
        return escape_string(value)

    def validate_source(self, value):
        """Escape source"""
        return escape_string(value)

    def validate_comment(self, value):
        """Escape comment"""
        return escape_string(value)
