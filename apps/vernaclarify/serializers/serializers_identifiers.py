"""
Data models transformations
"""
# pylint: disable=R0201
from rest_framework import serializers
from core.models import Identifiers
from core.utils import escape_string


class IdentifiersSerializer(serializers.ModelSerializer):
    """Identifiers serializer"""
    class Meta:
        model = Identifiers
        fields = ("taxon_id", "gbif", "ncbi", "inaturalist",
                  "iucn", "wikidata", "wikispecies", "wikipedia", "taxref", )

    def validate_gbif(self, value):
        """Escape gbif"""
        return escape_string(value)

    def validate_ncbi(self, value):
        """Escape ncbi"""
        return escape_string(value)

    def validate_inaturalist(self, value):
        """Escape inaturalist"""
        return escape_string(value)

    def validate_iucn(self, value):
        """Escape iucn"""
        return escape_string(value)

    def validate_wikidata(self, value):
        """Escape wikidata"""
        return escape_string(value)

    def validate_wikispecies(self, value):
        """Escape wikispecies"""
        return escape_string(value)

    def validate_wikipedia(self, value):
        """Escape wikipedia"""
        return escape_string(value)

    def validate_taxref(self, value):
        """Escape taxref"""
        return escape_string(value)
