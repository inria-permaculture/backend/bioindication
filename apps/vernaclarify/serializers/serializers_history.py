"""
Data models transformations
"""
# pylint: disable=no-member
from rest_framework import serializers
from core.models import Identifiers, VernacularEntry, ScientificEntry


class TaxonIdentifiersHistorySerializer(serializers.ModelSerializer):
    """Taxon"s identifiers history serializer"""
    class Meta:
        model = Identifiers.history.model
        fields = ("history_date", "history_type", "gbif", "ncbi",
                  "inaturalist", "iucn", "wikidata", "wikispecies", "wikipedia", "taxref",)


class TaxonVernacularsHistorySerializer(serializers.ModelSerializer):
    """Taxon"s vernaculars entries history serializer"""
    class Meta:
        model = VernacularEntry.history.model
        fields = ("history_date", "history_type", "id", "name", "language",
                  "locality", "source", "gender", "lifestage", "comment")


class TaxonScientificsHistorySerializer(serializers.ModelSerializer):
    """Taxon"s scientifics entries history serializerr"""
    class Meta:
        model = ScientificEntry.history.model
        fields = ("history_date", "history_type",
                  "id", "name", "source", "comment")
