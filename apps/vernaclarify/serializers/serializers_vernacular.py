"""
Data models transformations
"""
# pylint: disable=R0201
from rest_framework import serializers
from core.models import VernacularEntry
from core.utils import escape_string


class VernacularSerializer(serializers.ModelSerializer):
    """Vernacular serializer"""
    class Meta:
        model = VernacularEntry
        fields = ("id", "taxon", "name", "language", "locality",
                  "source", "gender", "lifestage", "comment")

    def validate_name(self, value):
        """Escape name"""
        return escape_string(value)

    def validate_language(self, value):
        """Escape language"""
        return escape_string(value)

    def validate_locality(self, value):
        """Escape locality"""
        return escape_string(value)

    def validate_source(self, value):
        """Escape source"""
        return escape_string(value)

    def validate_gender(self, value):
        """Escape gender"""
        return escape_string(value)

    def validate_lifestage(self, value):
        """Escape lifestage"""
        return escape_string(value)

    def validate_comment(self, value):
        """Escape comment"""
        return escape_string(value)
