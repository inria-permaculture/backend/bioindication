"""
Data models transformations
"""
# pylint: disable=R0201
from rest_framework import serializers
from core.models import Taxon


class TaxonSerializer(serializers.ModelSerializer):
    """Taxon serializer"""
    name = serializers.CharField(
        source="main_scientific_entry.name", read_only=True)

    class Meta:
        model = Taxon
        fields = ("id", "parent", "rank", "name",
                  "is_a_plant", "is_domesticated")
        read_only_fields = ("id", "parent", "rank", "is_a_plant")


class TaxonParentsSerializer(serializers.ModelSerializer):
    """Taxon"s parents serializer"""
    name = serializers.CharField(source="main_scientific_entry.name")
    parent = serializers.SerializerMethodField()

    class Meta:
        model = Taxon
        fields = ("id", "name", "parent",)

    def get_parent(self, obj):
        """Get taxon"s parent"""
        if obj.parent and obj.parent.id > 0:
            return TaxonParentsSerializer(obj.parent).data
        return None


class MatchingTaxonsSerializer(serializers.Serializer):
    # pylint: disable=W0223
    """Scientifics/Vernaculars list serializer"""
    name = serializers.CharField()
    type_entry = serializers.CharField()
    taxon_id = serializers.IntegerField()
    main_name = serializers.CharField(
        source="taxon__main_scientific_entry__name")
    rank = serializers.CharField(source="taxon__rank")
