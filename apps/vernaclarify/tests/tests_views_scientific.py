"""
API endpoints test classes
"""
import json
from django.test import TestCase
from rest_framework import status, reverse
from core.models import Taxon, ScientificEntry
from vernaclarify.serializers import ScientificSerializer


class CreateNewScientificTestCase(TestCase):
    """Create new scientific name test class"""

    def setUp(self):
        """Setup test class data"""
        Taxon.objects.create(id=1)
        self.valid_scientific = {"taxon": 1, "name": "Hirondelite"}
        self.invalid_scientific = {"taxon": 1, }
        self.scientific_with_unwanted_characters = {"name": "Hirondelite__&"}
        self.valid_scientific_invalid_taxon = {
            "taxon": 10, "name": "Hirondelite"}

    def test_create_valid_scientific(self):
        """Test create a valid scientific name"""
        response = self.client.post(
            reverse.reverse("post_scientific"),
            data=json.dumps(self.valid_scientific),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_scientific(self):
        """Test create an invalid scientific name"""
        response = self.client.post(
            reverse.reverse("post_scientific"),
            data=json.dumps(self.invalid_scientific),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_valid_scientific_invalid_taxon(self):
        """Test create a valid scientific name with an invalid taxon"""
        response = self.client.post(
            reverse.reverse("post_scientific"),
            data=json.dumps(self.valid_scientific_invalid_taxon),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_scientific_with_unwanted_characters(self):
        """Test create scientific name with unwanted characters"""
        response = self.client.post(
            reverse.reverse("post_scientific"),
            data=json.dumps(self.scientific_with_unwanted_characters),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class GetPatchDeleteScientificTestCase(TestCase):
    """Get, patch or delete scientific name test class"""

    def setUp(self):
        """Setup test class data"""
        self.taxon = Taxon.objects.create(id=50)
        self.existing_scientific_id = 1
        self.non_existing_scientific_id = 10
        ScientificEntry.objects.create(
            id=self.existing_scientific_id, taxon=self.taxon, name="Menthe poivrée")
        self.scientific_updated_data = {
            "taxon_id": self.taxon.id, "name": "Menthe épicay"}
        self.scientific_updated_empty_name_data = {
            "taxon_id": self.taxon.id, "name": ""}
        # Create and attach a scientificentry to the main_scientificentry field
        self.existing_scientific_as_main_taxon_name_id = 100
        taxon_with_scientific_name_id = 75
        scientific_entry = ScientificEntry.objects.create(
            id=self.existing_scientific_as_main_taxon_name_id,
            taxon_id=taxon_with_scientific_name_id,
            name="Hirondelle It"
        )
        taxon_with_scientific_name = Taxon.objects.create(
            id=taxon_with_scientific_name_id)
        taxon_with_scientific_name.main_scientific_entry = scientific_entry
        taxon_with_scientific_name.save()
        self.scientific_with_unwanted_characters = {
            "taxon_id": self.taxon.id, "name": "Hirondelite__&"}

    def test_delete_existing_scientific(self):
        """Test delete an existing scientific name"""
        response = self.client.delete(
            reverse.reverse("get_patch_delete_scientific", kwargs={
                            "scientific_id": self.existing_scientific_id})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_non_existing_scientific(self):
        """Test delete a non existing scientific name"""
        response = self.client.delete(
            reverse.reverse("get_patch_delete_scientific", kwargs={
                            "scientific_id": self.non_existing_scientific_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_main_scientific_taxon_name(self):
        """Test delete a scientific name used as a primary taxon"s scientific name"""
        response = self.client.delete(
            reverse.reverse("get_patch_delete_scientific", kwargs={
                            "scientific_id": self.existing_scientific_as_main_taxon_name_id})
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_existing_scientific(self):
        """Test get an existing scientific name"""
        response = self.client.get(
            reverse.reverse("get_patch_delete_scientific", kwargs={
                            "scientific_id": self.existing_scientific_id})
        )
        serializer = ScientificSerializer(
            ScientificEntry.objects.get(id=self.existing_scientific_id))
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_non_existing_scientific(self):
        """Test get a non existing scientific name"""
        response = self.client.get(
            reverse.reverse("get_patch_delete_scientific", kwargs={
                            "scientific_id": self.non_existing_scientific_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_patch_scientific(self):
        """Test patch a scientific name"""
        response = self.client.patch(
            reverse.reverse("get_patch_delete_scientific", kwargs={
                            "scientific_id": self.existing_scientific_id}),
            data=json.dumps(self.scientific_updated_data),
            content_type="application/json"
        )
        serializer = ScientificSerializer(
            ScientificEntry.objects.get(id=self.existing_scientific_id))
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_patch_empty_name_scientific(self):
        """Test patch a scientific name with an empty name"""
        response = self.client.patch(
            reverse.reverse("get_patch_delete_scientific", kwargs={
                            "scientific_id": self.existing_scientific_id}),
            data=json.dumps(self.scientific_updated_empty_name_data),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_patch_non_existing_scientific(self):
        """Test patch a non existing scientific name"""
        response = self.client.patch(
            reverse.reverse("get_patch_delete_scientific", kwargs={
                            "scientific_id": self.non_existing_scientific_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_patch_main_scientific_taxon_name(self):
        """Test patch a non existing scientific name used as a primary taxon"s scientific name"""
        response = self.client.patch(
            reverse.reverse("get_patch_delete_scientific", kwargs={
                            "scientific_id": self.existing_scientific_as_main_taxon_name_id})
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_scientific_with_unwanted_characters(self):
        """Test create scientific name with unwanted characters"""
        response = self.client.post(
            reverse.reverse("post_scientific"),
            data=json.dumps(self.scientific_with_unwanted_characters),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class GetScientificsListPerTaxonTestCase(TestCase):
    """Get taxon"s scientifics list test class"""

    def setUp(self):
        """Setup test class data"""
        self.taxon_id_with_scientifics = 1
        self.taxon_id_without_scientific = 2
        self.non_existing_taxon_id = 3
        Taxon.objects.create(id=self.taxon_id_with_scientifics)
        Taxon.objects.create(id=self.taxon_id_without_scientific)
        ScientificEntry.objects.create(taxon_id=self.taxon_id_with_scientifics)
        ScientificEntry.objects.create(taxon_id=self.taxon_id_with_scientifics)
        ScientificEntry.objects.create(taxon_id=self.taxon_id_with_scientifics)

    def test_get_valid_scientifics_list(self):
        """Test get a taxon"s scientifics names list"""
        response = self.client.get(
            reverse.reverse("get_taxon_scientific_list", kwargs={
                            "taxon_id": self.taxon_id_with_scientifics})
        )
        serializer = ScientificSerializer(ScientificEntry.objects.filter(
            taxon_id=self.taxon_id_with_scientifics), many=True)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_empty_scientifics_list(self):
        """Test get a taxon"s empty scientifics names list"""
        response = self.client.get(
            reverse.reverse("get_taxon_scientific_list", kwargs={
                            "taxon_id": self.taxon_id_without_scientific})
        )
        self.assertEqual(response.data, [])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_non_existing_scientifics_taxon(self):
        """Test get a taxon"s scientifics names list for a non existing taxon"""
        response = self.client.get(
            reverse.reverse("get_taxon_scientific_list", kwargs={
                            "taxon_id": self.non_existing_taxon_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
