"""
API endpoints test classes
"""

import json
from django.test import TestCase
from rest_framework import status, reverse
from core.models import Taxon, Identifiers
from vernaclarify.serializers import IdentifiersSerializer


class GetPatchTaxonIdentifiersTestCase(TestCase):
    """Get, patch taxon"s identifiers test class"""

    def setUp(self):
        """Setup test class data"""
        self.existing_taxon_id = 1
        self.non_existing_taxon_id = 10
        self.identifiers_data = {"taxon_id": self.existing_taxon_id, "gbif": "1111",
                                 "ncbi": "1111",
                                 "inaturalist": "1111",
                                 "iucn": "1111/1112",
                                 "wikidata": "Q1111",
                                 "wikispecies": "1111",
                                 "wikipedia": "1111",
                                 "taxref": "1111",
                                 }
        self.identifiers_updated_data = {"taxon_id": self.existing_taxon_id, "gbif": "1112",
                                         "ncbi": "1112",
                                         "inaturalist": "1112",
                                         "iucn": "1112/1113",
                                         "wikidata": "Q1112",
                                         "wikispecies": "1112",
                                         "wikipedia": "1112",
                                         "taxref": "1112",
                                         }
        Taxon.objects.create(id=self.existing_taxon_id)
        Identifiers.objects.create(**self.identifiers_data)

    def test_get_taxon_identifiers(self):
        """Test get taxon"s identifiers"""
        response = self.client.get(
            reverse.reverse("get_patch_taxon_identifiers",
                            kwargs={"taxon_id": self.existing_taxon_id})
        )
        serializer = IdentifiersSerializer(
            Identifiers.objects.get(taxon_id=self.existing_taxon_id))
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_non_existing_taxon(self):
        """Test get non existing taxon"""
        response = self.client.get(
            reverse.reverse("get_patch_taxon_identifiers", kwargs={
                            "taxon_id": self.non_existing_taxon_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_patch_taxon_identifiers(self):
        """Test patch taxon"s identifiers"""
        response = self.client.patch(
            reverse.reverse("get_patch_taxon_identifiers", kwargs={
                            "taxon_id": self.existing_taxon_id}),
            data=json.dumps(self.identifiers_updated_data),
            content_type="application/json"
        )
        serializer = IdentifiersSerializer(
            Identifiers.objects.get(taxon_id=self.existing_taxon_id))
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_patch_non_existing_taxon_identifiers(self):
        """Test patch non existing taxon"""
        response = self.client.patch(
            reverse.reverse("get_patch_taxon_identifiers", kwargs={
                            "taxon_id": self.non_existing_taxon_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
