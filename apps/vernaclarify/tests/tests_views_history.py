"""
API endpoints test classes
"""
# pylint: disable=no-member
from django.test import TestCase
from rest_framework import status, reverse
from core.models import Taxon, Identifiers, VernacularEntry, ScientificEntry
from vernaclarify.serializers import (TaxonIdentifiersHistorySerializer,
                                      TaxonVernacularsHistorySerializer,
                                      TaxonScientificsHistorySerializer)


class GetTaxonIdentifiersHistoryTestCase(TestCase):
    """Get taxon"s identifiers history test class"""

    def setUp(self):
        """Setup test class data"""
        self.non_existing_taxon_id = 9999
        self.taxon_with_identifiers_history = Taxon.objects.create(id=1)
        self.taxon_with_empty_identifiers_history = Taxon.objects.create(id=2)

        self.identifiers = Identifiers.objects.create(
            taxon=self.taxon_with_identifiers_history)  # Create one history record
        self.identifiers.gbif = 666
        self.identifiers.save()  # Create another history record
        self.identifiers.ncbi = 666
        self.identifiers.save()  # Create another history record(total = 3)

    def test_get_empty_identifiers_history(self):
        """Test get empty identifiers history"""
        response = self.client.get(
            reverse.reverse("get_taxon_identifiers_history", kwargs={
                "taxon_id": self.taxon_with_empty_identifiers_history.id})
        )
        self.assertEqual(len(response.data), 0)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_identifiers_history(self):
        """Test get identifiers history"""
        response = self.client.get(
            reverse.reverse("get_taxon_identifiers_history", kwargs={
                "taxon_id": self.taxon_with_identifiers_history.id})
        )
        serializer = TaxonIdentifiersHistorySerializer(Identifiers.history.all().filter(
            taxon_id=self.taxon_with_identifiers_history.id), many=True)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_non_existing_taxon_identifiers_history(self):
        """Test get non existing taxon"s identifiers history"""
        response = self.client.get(
            reverse.reverse("get_taxon_identifiers_history",
                            kwargs={"taxon_id": self.non_existing_taxon_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GetTaxonVernacularsHistoryTestCase(TestCase):
    """Get taxon"s vernaculars history test class"""

    def setUp(self):
        """Setup test class data"""
        self.non_existing_taxon_id = 9999
        self.taxon_with_vernaculars_history = Taxon.objects.create(id=1)
        self.taxon_with_empty_vernaculars_history = Taxon.objects.create(id=2)
        self.vernaculars = VernacularEntry.objects.create(
            taxon=self.taxon_with_vernaculars_history)  # Create one history record
        self.vernaculars.name = "Panthère rose"
        self.vernaculars.save()  # Create another history record
        self.vernaculars.name = "Panthère blanche"
        self.vernaculars.save()  # Create another history record(total = 3)

    def test_get_empty_vernaculars_history(self):
        """Test get empty vernaculars history"""
        response = self.client.get(
            reverse.reverse("get_taxon_identifiers_history", kwargs={
                            "taxon_id": self.taxon_with_empty_vernaculars_history.id})
        )
        self.assertEqual(len(response.data), 0)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_vernaculars_history(self):
        """Test get vernaculars history"""
        response = self.client.get(
            reverse.reverse("get_taxon_vernaculars_history", kwargs={
                            "taxon_id": self.taxon_with_vernaculars_history.id})
        )
        serializer = TaxonVernacularsHistorySerializer(VernacularEntry.history.all(
        ).filter(taxon_id=self.taxon_with_vernaculars_history.id), many=True)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_non_existing_taxon_vernaculars_history(self):
        """Test get non existing taxon"s vernaculars history"""
        response = self.client.get(
            reverse.reverse("get_taxon_vernaculars_history",
                            kwargs={"taxon_id": self.non_existing_taxon_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GetTaxonScientificsHistoryTestCase(TestCase):
    """Get taxon"s scientifics history test class"""

    def setUp(self):
        """Setup test class data"""
        self.non_existing_taxon_id = 9999
        self.taxon_with_scientifics_history = Taxon.objects.create(id=1)
        self.taxon_with_empty_scientifics_history = Taxon.objects.create(id=2)
        self.scientifics = ScientificEntry.objects.create(
            taxon=self.taxon_with_scientifics_history)  # Create one history record
        self.scientifics.name = "Panthère rose"
        self.scientifics.save()  # Create another history record
        self.scientifics.name = "Panthère blanche"
        self.scientifics.save()  # Create another history record(total = 3)

    def test_get_empty_scientifics_history(self):
        """Test get empty scientifics history"""
        response = self.client.get(
            reverse.reverse("get_taxon_identifiers_history", kwargs={
                            "taxon_id": self.taxon_with_empty_scientifics_history.id})
        )
        self.assertEqual(len(response.data), 0)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_scientifics_history(self):
        """Test get scientifics history"""
        response = self.client.get(
            reverse.reverse("get_taxon_scientifics_history", kwargs={
                            "taxon_id": self.taxon_with_scientifics_history.id})
        )
        serializer = TaxonScientificsHistorySerializer(ScientificEntry.history.all(
        ).filter(taxon_id=self.taxon_with_scientifics_history.id), many=True)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_non_existing_taxon_scientifics_history(self):
        """Test get non existing taxon"s scientifics history"""
        response = self.client.get(
            reverse.reverse("get_taxon_scientifics_history",
                            kwargs={"taxon_id": self.non_existing_taxon_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
