
"""
API endpoints test classes
"""
import json
from django.test import TestCase
from rest_framework import status, reverse
from core.models import Taxon, VernacularEntry, ScientificEntry
from vernaclarify.serializers import (TaxonSerializer,
                                      TaxonParentsSerializer,
                                      ScientificSerializer)


class GetPatchTaxonTestCase(TestCase):
    """Get and patch taxon test class"""

    def setUp(self):
        """Setup test class data"""
        self.existing_taxon_id = 1
        self.non_existing_taxon_id = 10
        ScientificEntry.objects.create(
            id=self.existing_taxon_id, taxon_id=self.existing_taxon_id, name="Mentha piperita")
        Taxon.objects.create(id=self.existing_taxon_id,
                             main_scientific_entry_id=self.existing_taxon_id)
        self.taxon_updated_data = {
            "id": self.existing_taxon_id,
            "is_domesticated": True,
        }

    def test_get_existing_taxon(self):
        """Test get an existing taxon"""
        response = self.client.get(
            reverse.reverse("get_patch_taxon", kwargs={
                            "taxon_id": self.existing_taxon_id})
        )
        serializer = TaxonSerializer(
            Taxon.objects.get(id=self.existing_taxon_id))
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_non_existing_taxon(self):
        """Test get a non existing taxon"""
        response = self.client.get(
            reverse.reverse("get_patch_taxon", kwargs={
                            "taxon_id": self.non_existing_taxon_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_patch_taxon(self):
        """Test patch taxon"""
        response = self.client.patch(
            reverse.reverse("get_patch_taxon", kwargs={
                            "taxon_id": self.existing_taxon_id}),
            data=json.dumps(self.taxon_updated_data),
            content_type="application/json"
        )
        serializer = TaxonSerializer(
            Taxon.objects.get(id=self.existing_taxon_id))
        self.assertEqual(
            serializer.data["is_domesticated"], self.taxon_updated_data["is_domesticated"])
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_patch_non_existing_taxon(self):
        """Test patch non existing taxon"""
        response = self.client.patch(
            reverse.reverse("get_patch_taxon", kwargs={
                            "taxon_id": self.non_existing_taxon_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GetTaxonMainScientificNameTestCase(TestCase):
    """Get taxon"s main scientific name test class"""

    def setUp(self):
        """Setup test class data"""
        self.taxon_id = 1
        self.non_existing_taxon_id = 2

        ScientificEntry.objects.create(
            id=self.taxon_id, taxon_id=self.taxon_id, name="Menthae")
        Taxon.objects.create(id=self.taxon_id,
                             main_scientific_entry_id=self.taxon_id)

    def test_get_taxon_main_scientific_name(self):
        """Test get taxon"s main scientific name"""
        response = self.client.get(
            reverse.reverse("get_taxon_main_scientific_name", kwargs={
                            "taxon_id": self.taxon_id})
        )
        serializer = ScientificSerializer(
            Taxon.objects.get(id=self.taxon_id).main_scientific_entry)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_non_existing_taxon(self):
        """Test get non existing taxon"""
        response = self.client.get(
            reverse.reverse("get_taxon_main_scientific_name", kwargs={
                            "taxon_id": self.non_existing_taxon_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GetTaxonParentsTestCase(TestCase):
    """Get taxon"s parents test class"""

    def setUp(self):
        """Setup test class data"""
        self.taxon_id_parent = 1
        self.taxon_id_child = 2
        self.taxon_id_child_child = 3
        self.non_existing_taxon_id = 4

        ScientificEntry.objects.create(
            id=self.taxon_id_parent, taxon_id=self.taxon_id_parent, name="Menthae")
        ScientificEntry.objects.create(
            id=self.taxon_id_child, taxon_id=self.taxon_id_child, name="Mentha")
        ScientificEntry.objects.create(
            id=self.taxon_id_child_child,
            taxon_id=self.taxon_id_child_child,
            name="Mentha piperita")

        Taxon.objects.create(id=self.taxon_id_parent,
                             main_scientific_entry_id=self.taxon_id_parent)
        Taxon.objects.create(id=self.taxon_id_child,
                             main_scientific_entry_id=self.taxon_id_child)
        Taxon.objects.create(id=self.taxon_id_child_child,
                             main_scientific_entry_id=self.taxon_id_child_child)

    def test_get_taxon_parents_list(self):
        """Test get taxon"s parents list"""
        response = self.client.get(
            reverse.reverse("get_taxon_parents", kwargs={
                            "taxon_id": self.taxon_id_child_child})
        )
        serializer = TaxonParentsSerializer(
            Taxon.objects.get(id=self.taxon_id_child_child))
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_taxon_empty_parents_list(self):
        """Test get taxon"s empty parents list"""
        response = self.client.get(
            reverse.reverse("get_taxon_parents", kwargs={
                            "taxon_id": self.taxon_id_parent})
        )
        serializer = TaxonParentsSerializer(
            Taxon.objects.get(id=self.taxon_id_parent))
        self.assertEqual(serializer.data["parent"], None)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_non_existing_taxon(self):
        """Test get non existing taxon"""
        response = self.client.get(
            reverse.reverse("get_taxon_parents", kwargs={
                            "taxon_id": self.non_existing_taxon_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GetTaxonListByName(TestCase):
    """Test class for endpoint taxa/name/* endpoints"""

    def setUp(self):
        """Setup test data"""
        ScientificEntry.objects.create(id=1, taxon_id=1, name="menthe poivré")
        ScientificEntry.objects.create(id=2, taxon_id=2, name="menthe épié")
        ScientificEntry.objects.create(id=3, taxon_id=3, name="menthe karaoké")

        Taxon.objects.create(id=1, main_scientific_entry_id=1)
        Taxon.objects.create(id=2, main_scientific_entry_id=2)
        Taxon.objects.create(id=3, main_scientific_entry_id=3)

        VernacularEntry.objects.create(taxon_id=1, name="menthe poivray")
        VernacularEntry.objects.create(taxon_id=2, name="menthe épicay")
        VernacularEntry.objects.create(taxon_id=3, name="menthe karaokay")
        VernacularEntry.objects.create(taxon_id=3, name="menthe karaoky")
        VernacularEntry.objects.create(taxon_id=3, name="lorem ipsum")

        # Generate data to test ordering
        ScientificEntry.objects.create(id=4, taxon_id=4)
        Taxon.objects.create(id=4, main_scientific_entry_id=4)
        VernacularEntry.objects.create(taxon_id=4, name="koolos karalikis")
        VernacularEntry.objects.create(taxon_id=4, name="karalikis koolos")
        VernacularEntry.objects.create(taxon_id=4, name="karalikis")
        VernacularEntry.objects.create(taxon_id=4, name="karalik")

    def test_scientifics_and_vernaculars(self):
        """Results for both scientific and vernacular names"""
        response = self.client.get(
            reverse.reverse("get_taxon_list_by_name_contains", kwargs={
                            "partial_name": "menth"})
        )
        self.assertEqual(len(response.data), 7)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_empty(self):
        """Non existing name"""
        response = self.client.get(
            reverse.reverse("get_taxon_list_by_name_contains", kwargs={
                            "partial_name": "Courge coureuse"})
        )
        self.assertEqual(response.data, [])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_result_order(self):
        """Result ordering"""
        response = self.client.get(
            reverse.reverse("get_taxon_list_by_name_contains", kwargs={
                            "partial_name": "karali"})
        )
        self.assertEqual(len(response.data), 4)
        self.assertEqual(response.data[0]["name"], "karalik")
        self.assertEqual(response.data[1]["name"], "karalikis")
        self.assertEqual(response.data[2]["name"], "karalikis koolos")
        self.assertEqual(response.data[3]["name"], "koolos karalikis")

    def test_contains(self):
        """Contains vs Startswith: contains"""
        response = self.client.get(
            reverse.reverse("get_taxon_list_by_name_contains", kwargs={
                            "partial_name": "kara"})
        )
        self.assertEqual(len(response.data), 7)

    def test_startswith(self):
        """Contains vs Startswith: startswith"""
        response = self.client.get(
            reverse.reverse("get_taxon_list_by_name_startswith", kwargs={
                            "partial_name": "kara"})
        )
        self.assertEqual(len(response.data), 3)

    def test_not_enough_characters(self):
        """Not enought characters"""
        response = self.client.get(
            reverse.reverse("get_taxon_list_by_name_contains", kwargs={
                            "partial_name": "Co"})
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_taxon_list(self):
        """POST (not allowed)"""
        response = self.client.post(
            reverse.reverse("get_taxon_list_by_name_contains", kwargs={
                            "partial_name": "Whatever"})
        )
        self.assertEqual(response.status_code,
                         status.HTTP_405_METHOD_NOT_ALLOWED)
