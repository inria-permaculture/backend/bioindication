"""
API endpoints test classes
"""

import json
from django.test import TestCase
from rest_framework import status, reverse
from core.models import Taxon, VernacularEntry
from vernaclarify.serializers import VernacularSerializer


class CreateNewVernacularTestCase(TestCase):
    """Create new vernacular name test class"""

    def setUp(self):
        """Setup test class data"""
        Taxon.objects.create(id=1)
        self.valid_vernacular = {"taxon": 1, "name": "Hirondelite"}
        self.invalid_vernacular = {"taxon": 1, }
        self.unwanted_characters_vernacular = {
            "taxon": 1, "name": "Hirondelite__&"}
        self.valid_vernacular_invalid_taxon = {
            "taxon": 10, "name": "Hirondelite"}

    def test_create_valid_vernacular(self):
        """Test create a valid vernacular name"""
        response = self.client.post(
            reverse.reverse("post_vernacular"),
            data=json.dumps(self.valid_vernacular),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_vernacular(self):
        """Test create an invalid vernacular name"""
        response = self.client.post(
            reverse.reverse("post_vernacular"),
            data=json.dumps(self.invalid_vernacular),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_valid_vernacular_invalid_taxon(self):
        """Test create a valid vernacular name with an invalid taxon"""
        response = self.client.post(
            reverse.reverse("post_vernacular"),
            data=json.dumps(self.valid_vernacular_invalid_taxon),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class GetPatchDeleteVernacularTestCase(TestCase):
    """Get, patch or delete vernacular name test class"""

    def setUp(self):
        """Setup test class data"""
        self.taxon = Taxon.objects.create(id=50)
        self.existing_vernacular_id = 1
        self.non_existing_vernacular_id = 10
        VernacularEntry.objects.create(
            id=self.existing_vernacular_id, taxon=self.taxon, name="Menthe poivrée")
        self.vernacular_updated_data = {
            "taxon_id": self.taxon.id, "name": "Menthe épicay"}
        self.vernacular_updated_empty_name_data = {
            "taxon_id": self.taxon.id, "name": ""}
        self.vernacular_unwanted_characters = {
            "taxon_id": self.taxon.id, "name": "Hirondelite__&"}

    def test_delete_existing_vernacular(self):
        """Test delete an existing vernacular name"""
        response = self.client.delete(
            reverse.reverse("get_patch_delete_vernacular", kwargs={
                            "vernacular_id": self.existing_vernacular_id})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_non_existing_vernacular(self):
        """Test delete a non existing vernacular name"""
        response = self.client.delete(
            reverse.reverse("get_patch_delete_vernacular", kwargs={
                            "vernacular_id": self.non_existing_vernacular_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_existing_vernacular(self):
        """Test get an existing vernacular name"""
        response = self.client.get(
            reverse.reverse("get_patch_delete_vernacular", kwargs={
                            "vernacular_id": self.existing_vernacular_id})
        )
        serializer = VernacularSerializer(
            VernacularEntry.objects.get(id=self.existing_vernacular_id))
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_non_existing_vernacular(self):
        """Test get a non existing vernacular name"""
        response = self.client.get(
            reverse.reverse("get_patch_delete_vernacular", kwargs={
                            "vernacular_id": self.non_existing_vernacular_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_patch_vernacular(self):
        """Test patch a vernacular name"""
        response = self.client.patch(
            reverse.reverse("get_patch_delete_vernacular", kwargs={
                            "vernacular_id": self.existing_vernacular_id}),
            data=json.dumps(self.vernacular_updated_data),
            content_type="application/json"
        )
        serializer = VernacularSerializer(
            VernacularEntry.objects.get(id=self.existing_vernacular_id))
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_patch_empty_name_vernacular(self):
        """Test patch a vernacular name with an empty name"""
        response = self.client.patch(
            reverse.reverse("get_patch_delete_vernacular", kwargs={
                            "vernacular_id": self.existing_vernacular_id}),
            data=json.dumps(self.vernacular_updated_empty_name_data),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_patch_non_existing_vernacular(self):
        """Test patch a non existing vernacular name"""
        response = self.client.patch(
            reverse.reverse("get_patch_delete_vernacular", kwargs={
                            "vernacular_id": self.non_existing_vernacular_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GetVernacularsListPerTaxonTestCase(TestCase):
    """Get taxon"s vernaculars list test class"""

    def setUp(self):
        """Setup test class data"""
        self.taxon_id_with_vernaculars = 1
        self.taxon_id_without_vernacular = 2
        self.non_existing_taxon_id = 3
        Taxon.objects.create(id=self.taxon_id_with_vernaculars)
        Taxon.objects.create(id=self.taxon_id_without_vernacular)
        VernacularEntry.objects.create(taxon_id=self.taxon_id_with_vernaculars)
        VernacularEntry.objects.create(taxon_id=self.taxon_id_with_vernaculars)
        VernacularEntry.objects.create(taxon_id=self.taxon_id_with_vernaculars)

    def test_get_valid_vernaculars_list(self):
        """Test get a taxon"s vernacular names list"""
        response = self.client.get(
            reverse.reverse("get_taxon_vernacular_list", kwargs={
                            "taxon_id": self.taxon_id_with_vernaculars})
        )
        serializer = VernacularSerializer(VernacularEntry.objects.filter(
            taxon_id=self.taxon_id_with_vernaculars), many=True)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_empty_vernaculars_list(self):
        """Test get a taxon"s empty vernaculars names list"""
        response = self.client.get(
            reverse.reverse("get_taxon_vernacular_list", kwargs={
                            "taxon_id": self.taxon_id_without_vernacular})
        )
        self.assertEqual(response.data, [])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_non_existing_vernaculars_taxon(self):
        """Test get a taxon"s vernaculars names list for a non existing taxon"""
        response = self.client.get(
            reverse.reverse("get_taxon_vernacular_list", kwargs={
                            "taxon_id": self.non_existing_taxon_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
