"""
API endpoints test classes
"""

from vernaclarify.tests.tests_views_taxon import *
from vernaclarify.tests.tests_views_identifiers import *
from vernaclarify.tests.tests_views_history import *
from vernaclarify.tests.tests_views_scientific import *
