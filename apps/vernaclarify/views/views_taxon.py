"""
API endpoints functions
"""

from enum import Enum
from django.conf import settings
from django.db.models.functions import StrIndex
from django.db.models import Value, CharField
from django.contrib.postgres.search import TrigramSimilarity
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from drf_spectacular.utils import extend_schema
from core.models import Taxon, VernacularEntry, ScientificEntry
from vernaclarify.serializers import (TaxonSerializer, TaxonParentsSerializer,
                                      MatchingTaxonsSerializer,
                                      ScientificSerializer)


class TypeEntry(Enum):
    """TypeEntry enumeration"""
    SCIENTIFIC = "SCIENTIFIC"
    VERNACULAR = "VERNACULAR"


@extend_schema(
    request=TaxonSerializer,
    responses=TaxonSerializer
)
@api_view(["GET", "PATCH"])
def get_patch_taxon(request, taxon_id):
    """Get and patch a taxon"""
    try:
        taxon = Taxon.objects.get(id=taxon_id)
    except Taxon.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == "GET":
        serializer = TaxonSerializer(taxon)
        return Response(serializer.data, status=status.HTTP_200_OK)
    if request.method == "PATCH":
        serializer = TaxonSerializer(
            taxon, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@extend_schema(
    responses=ScientificSerializer
)
@api_view(["GET"])
def get_taxon_main_scientific_name(request, taxon_id):
    """Get taxon"s main scientific name"""
    try:
        taxon = Taxon.objects.get(id=taxon_id)
        main_scientific_name = taxon.main_scientific_entry
    except Taxon.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == "GET":
        serializer = ScientificSerializer(main_scientific_name)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@extend_schema(
    responses=TaxonParentsSerializer
)
@api_view(["GET"])
def get_taxon_parents(request, taxon_id):
    """Get taxon"s parents"""
    try:
        taxon = Taxon.objects.get(id=taxon_id)
    except Taxon.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == "GET":
        serializer = TaxonParentsSerializer(taxon)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@extend_schema(
    responses=MatchingTaxonsSerializer(many=True)
)
@api_view(["GET"])
def get_taxon_list_by_name(request, partial_name, filter_type, max_number_result=20):
    """Get partial taxons list filtered by scientifics and vernaculars names"""
    if request.method != "GET":
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    min_required_characters_for_filtering = settings.CGB["MINIMUM_CHARACTERS_FOR_FILTERING"]
    if len(partial_name) < min_required_characters_for_filtering:
        return Response(
            (f"Filtering string too short, minimum characters allowed : "
             f"{min_required_characters_for_filtering}"),
            status=status.HTTP_400_BAD_REQUEST
        )

    max_number_of_results = settings.CGB["MAXIMUM_NUMBER_RESULTS"]
    if max_number_result > max_number_of_results:
        error_message = (f"Number of maximum results returned overwhelmed. Max results number "
                         f"is : {max_number_of_results}")
        return Response(error_message, status=status.HTTP_400_BAD_REQUEST)

    taxons_from_scientifics = ScientificEntry.objects
    if filter_type == 'startswith':
        taxons_from_scientifics = taxons_from_scientifics.filter(
            name__istartswith=partial_name)
    elif filter_type == 'contains':
        taxons_from_scientifics = taxons_from_scientifics.filter(
            name__icontains=partial_name)
    else:
        taxons_from_scientifics = taxons_from_scientifics.none()
    taxons_from_scientifics = taxons_from_scientifics \
        .annotate(similarity=TrigramSimilarity("name", partial_name)) \
        .annotate(match_index=StrIndex("name", Value(partial_name))) \
        .annotate(type_entry=Value(TypeEntry.SCIENTIFIC.value, output_field=CharField()))

    taxons_from_vernaculars = VernacularEntry.objects
    if filter_type == 'startswith':
        taxons_from_vernaculars = taxons_from_vernaculars.filter(
            name__istartswith=partial_name)
    elif filter_type == 'contains':
        taxons_from_vernaculars = taxons_from_vernaculars.filter(
            name__icontains=partial_name)
    else:
        taxons_from_vernaculars = taxons_from_vernaculars.none()
    taxons_from_vernaculars = taxons_from_vernaculars \
        .annotate(similarity=TrigramSimilarity("name", partial_name)) \
        .annotate(match_index=StrIndex("name", Value(partial_name))) \
        .annotate(type_entry=Value(TypeEntry.VERNACULAR.value, output_field=CharField()))

    taxons = taxons_from_scientifics.values(
        "id",
        "taxon_id",
        "name",
        "type_entry",
        "taxon__main_scientific_entry__name",
        "taxon__rank",
        "match_index",
        "similarity")
    taxons = taxons.union(taxons_from_vernaculars.values(
        "id",
        "taxon_id",
        "name",
        "type_entry",
        "taxon__main_scientific_entry__name",
        "taxon__rank",
        "match_index",
        "similarity"))
    taxons = taxons.order_by("-similarity", "match_index")

    serializer = MatchingTaxonsSerializer(
        taxons[:max_number_result], many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)
