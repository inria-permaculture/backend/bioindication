"""
API endpoints functions
"""
# pylint: disable=no-member
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from drf_spectacular.utils import extend_schema
from core.models import Taxon, Identifiers, VernacularEntry, ScientificEntry
from vernaclarify.serializers import (TaxonIdentifiersHistorySerializer,
                                      TaxonVernacularsHistorySerializer,
                                      TaxonScientificsHistorySerializer)


@extend_schema(
    responses=TaxonIdentifiersHistorySerializer
)
@api_view(["GET"])
def get_taxon_identifiers_history(request, taxon_id):
    """Get taxon"s identifiers history"""
    try:
        taxon = Taxon.objects.get(id=taxon_id)
        history = Identifiers.history.all().filter(taxon=taxon)
    except Taxon.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == "GET":
        serializer = TaxonIdentifiersHistorySerializer(history, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@extend_schema(
    responses=TaxonVernacularsHistorySerializer
)
@api_view(["GET"])
def get_taxon_vernaculars_history(request, taxon_id):
    """Get taxon"s vernaculars history"""
    try:
        taxon = Taxon.objects.get(id=taxon_id)
        history = VernacularEntry.history.all().filter(taxon=taxon)
    except Taxon.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == "GET":
        serializer = TaxonVernacularsHistorySerializer(history, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@extend_schema(
    responses=TaxonScientificsHistorySerializer
)
@api_view(["GET"])
def get_taxon_scientifics_history(request, taxon_id):
    """Get taxon"s scientifics history"""
    try:
        taxon = Taxon.objects.get(id=taxon_id)
        history = ScientificEntry.history.all().filter(taxon=taxon)
    except Taxon.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == "GET":
        serializer = TaxonScientificsHistorySerializer(history, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
