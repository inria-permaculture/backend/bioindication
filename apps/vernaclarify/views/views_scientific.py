"""
API endpoints functions
"""

from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from drf_spectacular.utils import extend_schema
from core.models import ScientificEntry, Taxon
from vernaclarify.serializers import ScientificSerializer

# Status-related error messages returned by views
MAIN_SCIENTIFIC_ENTRY_CANT_BE_UPDATED_OR_DELETED = "Un nom scientifique utilisé comme \
    nom principal ne peut être modifié ni supprimé"


@extend_schema(
    request=ScientificSerializer,
    responses=ScientificSerializer
)
@api_view(["POST"])
def post_scientific(request):
    """Create a new scientific name"""
    if request.method == "POST":
        serializer = ScientificSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@extend_schema(
    request=ScientificSerializer,
    responses=ScientificSerializer
)
@api_view(["GET", "PATCH", "DELETE"])
def get_patch_delete_scientific(request, scientific_id):
    """Get, patch or delete a scientific name"""
    # pylint: disable=R0911
    # We accept more than 6 return statements here.
    # This endpoint could be divided into smaller pieces later
    try:
        scientific = ScientificEntry.objects.get(id=scientific_id)
    except ScientificEntry.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    # Get details of a single scientific name
    if request.method == "GET":
        serializer = ScientificSerializer(scientific)
        return Response(serializer.data, status=status.HTTP_200_OK)
    # Verify scientific isn"t used as a taxon"s main_scientific_entry
    # before update or delete it
    if not Taxon.objects.filter(main_scientific_entry=scientific).exists():
        # Update scientific data
        if request.method == "PATCH":
            serializer = ScientificSerializer(
                scientific, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        # Delete a single scientific name
        if request.method == "DELETE":
            scientific.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
    else:
        return Response(
            MAIN_SCIENTIFIC_ENTRY_CANT_BE_UPDATED_OR_DELETED,
            status=status.HTTP_400_BAD_REQUEST
        )
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@extend_schema(
    responses=ScientificSerializer(many=True)
)
@api_view(["GET"])
def get_taxon_scientific_list(request, taxon_id):
    """Get taxon"s scientifics list"""
    try:
        Taxon.objects.get(id=taxon_id)
    except Taxon.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    # Get list of vernacular entries for a selected taxon
    if request.method == "GET":
        scientific = ScientificEntry.objects.filter(taxon_id=taxon_id)
        serializer = ScientificSerializer(scientific, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
