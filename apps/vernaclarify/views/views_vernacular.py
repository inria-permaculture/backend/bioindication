"""
API endpoints functions
"""

from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from drf_spectacular.utils import extend_schema
from core.models import VernacularEntry, Taxon
from vernaclarify.serializers import VernacularSerializer


@extend_schema(
    request=VernacularSerializer,
    responses=VernacularSerializer
)
@api_view(["POST"])
def post_vernacular(request):
    """Create a new vernacular name"""
    if request.method == "POST":
        serializer = VernacularSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@extend_schema(
    request=VernacularSerializer,
    responses=VernacularSerializer
)
@api_view(["GET", "PATCH", "DELETE"])
def get_patch_delete_vernacular(request, vernacular_id):
    """Get, patch or delete a vernacular name"""
    try:
        vernacular = VernacularEntry.objects.get(id=vernacular_id)
    except VernacularEntry.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    # Get details of a single vernacular name
    if request.method == "GET":
        serializer = VernacularSerializer(vernacular)
        return Response(serializer.data, status=status.HTTP_200_OK)
    # Update vernacular data
    if request.method == "PATCH":
        serializer = VernacularSerializer(
            vernacular, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    # Delete a single vernacular name
    if request.method == "DELETE":
        vernacular.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@extend_schema(
    responses=VernacularSerializer(many=True)
)
@api_view(["GET"])
def get_taxon_vernacular_list(request, taxon_id):
    """Get taxon"s vernaculars list"""
    try:
        Taxon.objects.get(id=taxon_id)
    except Taxon.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    # Get list of vernacular entries for a selected taxon
    if request.method == "GET":
        vernaculars = VernacularEntry.objects.filter(taxon_id=taxon_id)
        serializer = VernacularSerializer(vernaculars, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
