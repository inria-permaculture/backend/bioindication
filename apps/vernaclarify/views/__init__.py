"""
API endpoints functions
"""
from vernaclarify.views.views_taxon import *
from vernaclarify.views.views_identifiers import *
from vernaclarify.views.views_history import *
from vernaclarify.views.views_vernacular import *
from vernaclarify.views.views_scientific import *
