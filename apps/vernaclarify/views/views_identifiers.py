"""
API endpoints functions
"""

from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from drf_spectacular.utils import extend_schema
from core.models import Identifiers
from vernaclarify.serializers import IdentifiersSerializer


@extend_schema(
    request=IdentifiersSerializer,
    responses=IdentifiersSerializer
)
@api_view(["GET", "PATCH"])
def get_patch_taxon_identifiers(request, taxon_id):
    """Get, patch a taxon"s identifiers"""
    try:
        identifiers = Identifiers.objects.get(taxon_id=taxon_id)
    except Identifiers.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    # Get identifiers for a specific taxon
    if request.method == "GET":
        serializer = IdentifiersSerializer(identifiers)
        return Response(serializer.data, status=status.HTTP_200_OK)
    # Update identfiers for a specific taxon
    if request.method == "PATCH":
        serializer = IdentifiersSerializer(
            identifiers, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
