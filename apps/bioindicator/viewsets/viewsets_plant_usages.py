# pylint: disable=too-many-ancestors,missing-module-docstring,no-member
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework.decorators import action
from drf_spectacular.utils import extend_schema, extend_schema_view, OpenApiResponse
from core.models import PlantUsages
from core.utils import http_codes_description
from bioindicator.serializers import (
    PlantUsagesSerializer, PartialUpdatePlantUsagesSerializer, PlantUsagesHistorySerializer)

# Defining view serializers mapping
# Most views have the same input/output serializer, or only output
# But some of them have a different input serializer
PLANT_USAGES_VALUES_VIEW_SERIALIZER_MAPPING_DIC = {
    "default":
    {
        "retrieve": PlantUsagesSerializer,
        "partial_update": PlantUsagesSerializer,
        "get_history": PlantUsagesHistorySerializer,
    },
    "extended-input":
    {
        "partial_update": PartialUpdatePlantUsagesSerializer,
    }
}


# Generic and specific actions have an extended open api schema by
# using the extend_schema_view decorator on top of the related viewset
@extend_schema_view(retrieve=extend_schema(responses={
    200: OpenApiResponse(PLANT_USAGES_VALUES_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("retrieve"),
                         description=http_codes_description.SUCCESS),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(partial_update=extend_schema(
    request=PLANT_USAGES_VALUES_VIEW_SERIALIZER_MAPPING_DIC
    .get("extended-input").get("partial_update"),
    responses={
        200: OpenApiResponse(response=PLANT_USAGES_VALUES_VIEW_SERIALIZER_MAPPING_DIC
                             .get("default").get("partial_update"),
                             description=http_codes_description.SUCCESS),
        400: OpenApiResponse(description=http_codes_description.BAD_REQUEST),
        404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
@extend_schema_view(retrieve=extend_schema(responses={
    200: OpenApiResponse(PLANT_USAGES_VALUES_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("history"),
                         description=http_codes_description.SUCCESS),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
class PlantUsagesViewSet(
        mixins.RetrieveModelMixin,
        viewsets.GenericViewSet):
    """
    Views related to plant usages manipulations
    """
    default_serializer = PlantUsagesSerializer
    queryset = PlantUsages.objects.all()

    def get_serializer_class(self):
        """
        Override the GenericAPIView method
        Get the serializer used for generics actions
        The selected serializer is detected as input and output serializer by drf-specatcular
        For specifics actions, input/output serializers are defined inside the method
        and with the extend_schema decorator
        """
        response_serializer_dic = PLANT_USAGES_VALUES_VIEW_SERIALIZER_MAPPING_DIC.get(
            "default")
        selected_serializer = response_serializer_dic.get(
            self.action, self.default_serializer)
        return selected_serializer

    def partial_update(self, request, pk):
        # pylint: disable=unused-argument
        """
        Partially update a plant usages instance
        Offer a way to use different serializer as input and output
        Unused argument pk but still define because it"s passed by default by the caller
        """
        instance = self.get_object()
        serializer = PLANT_USAGES_VALUES_VIEW_SERIALIZER_MAPPING_DIC.get("extended-input").get(
            "partial_update")(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        plant_usages_data_serializer = self.get_serializer(instance)
        return Response(plant_usages_data_serializer.data)

    @action(methods=["get"], detail=True, url_path="history", url_name="history")
    def get_plant_usages_history(self, request, pk):
        # pylint: disable=unused-argument
        """Get plant usages history"""
        instance = self.get_object()
        history = PlantUsages.history.all().filter(taxon=instance.taxon)
        serializer = PlantUsagesHistorySerializer(history, many=True)
        return Response(serializer.data)
