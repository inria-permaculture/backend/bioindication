# pylint: disable=too-many-ancestors,missing-module-docstring
from rest_framework import viewsets, mixins
from drf_spectacular.utils import extend_schema, extend_schema_view, OpenApiResponse
from core.models import IndicatorValues
from core.utils import http_codes_description
from bioindicator.serializers import IndicatorValuesSerializer


# Defining view serializers mapping
# Most views have the same input/output serializer, or only output
# But some of them have a different input serializer
INDICATOR_VALUES_VIEW_SERIALIZER_MAPPING_DIC = {
    "default":
    {
        "retrieve": IndicatorValuesSerializer,
    },
}


# Generic and specific actions have an extended open api schema by
# using the extend_schema_view decorator on top of the related viewset
@extend_schema_view(retrieve=extend_schema(responses={
    200: OpenApiResponse(INDICATOR_VALUES_VIEW_SERIALIZER_MAPPING_DIC
                         .get("default").get("retrieve"),
                         description=http_codes_description.SUCCESS),
    404: OpenApiResponse(description=http_codes_description.NOT_FOUND)}))
class IndicatorValuesViewSet(
        mixins.RetrieveModelMixin,
        viewsets.GenericViewSet):
    """
    Views related to indicator values manipulations
    """
    default_serializer = IndicatorValuesSerializer
    queryset = IndicatorValues.objects.all()

    def get_serializer_class(self):
        """
        Override the GenericAPIView method
        Get the serializer used for generics actions
        The selected serializer is detected as input and output serializer by drf-specatcular
        For specifics actions, input/output serializers are defined inside the method
        and with the extend_schema decorator
        """
        response_serializer_dic = INDICATOR_VALUES_VIEW_SERIALIZER_MAPPING_DIC.get(
            "default")
        selected_serializer = response_serializer_dic.get(
            self.action, self.default_serializer)
        return selected_serializer
