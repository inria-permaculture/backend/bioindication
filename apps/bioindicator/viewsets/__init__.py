"""
Viewsets to combine views into api endpoints
"""

from bioindicator.viewsets.viewsets_indicator_values import *
from bioindicator.viewsets.viewsets_plant_usages import *
