"""
Django command to import indicator values from a csv file
"""
import csv
from decimal import Decimal, InvalidOperation
import time
from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
from core.models import Taxon, IndicatorValues

INDICATOR_VALUES_ALREADY_EXISTS_ERROR_MESSAGE = \
    "Un objet Indicator values avec ce champ Taxon existe déjà."
NO_MATCHING_TAXON_FOUND = "NO_MATCHING_TAXON_FOUND"
SAME_TAXON_MATCHED_BY_SEVERAL_INDICATOR_VALUES = "SAME_TAXON_MATCHED_BY_SEVERAL_INDICATOR_VALUES"
INDICATOR_VALUES_MATCH_SEVERAL_TAXA = "INDICATOR_VALUES_MATCH_SEVERAL_TAXA"


class Command(BaseCommand):
    """
    Import indicator values data from a csv file
    """
    help = "Import indicator values data from a csv file."
    indicator_values_csv_header = ("identifier", "scientific_name",
                                   "light", "light_stdev",
                                   "temperature", "temperature_stdev",
                                   "continentality",  "continentality_stdev",
                                   "atmospheric_moisture", "atmospheric_moisture_stdev",
                                   "edaphic_moisture", "edaphic_moisture_stdev",
                                   "soil_reaction", "soil_reaction_stdev",
                                   "nutrient", "nutrient_stdev",
                                   "salinity", "salinity_stdev",
                                   "soil_texture", "soil_texture_stdev",
                                   "organic_matter", "organic_matter_stdev",
                                   )
    invalid_rows_output_csv_header = ("identifier", "scientific_name",
                                      "light", "light_stdev",
                                      "temperature", "temperature_stdev",
                                      "continentality",  "continentality_stdev",
                                      "atmospheric_moisture", "atmospheric_moisture_stdev",
                                      "edaphic_moisture", "edaphic_moisture_stdev",
                                      "soil_reaction", "soil_reaction_stdev",
                                      "nutrient", "nutrient_stdev",
                                      "salinity", "salinity_stdev",
                                      "soil_texture", "soil_texture_stdev",
                                      "organic_matter", "organic_matter_stdev",
                                      "importation_error")
    invalid_rows_output_csv_name = "not_imported_indicator_values"
    indicator_values_and_stdev_fields = [field.name for field in IndicatorValues._meta.fields
                                         if field.name not in ("taxon", "history")]

    def add_arguments(self, parser):
        parser.add_argument("file_path", nargs="+", type=str)
        parser.add_argument('--reset', action='store_true',
                            help='Delete all indicator values from database')
        parser.add_argument('--insert', action='store_true',
                            help='Insert only new indicator values without updating existing ones')
        parser.add_argument('--output', action='store_true',
                            help='Generate an output csv file storing all invalid indicator values')

    def handle(self, *args, **options):
        t_0 = time.process_time()

        if options['reset']:
            self.__delete_indicator_values_from_database()

        with open(options["file_path"][0], encoding="utf8") as csvfile:
            reader = csv.DictReader(
                csvfile, self.indicator_values_csv_header)
            reader.__next__()  # skip the csv header
            indicator_values_models = self.__get_indicator_values_models_from_valid_rows(
                reader, options['output'])
            if options['insert']:
                self.__insert_indicator_values_in_database(
                    indicator_values_models["new"])
            else:
                self.__insert_indicator_values_in_database(
                    indicator_values_models["new"])
                self.__update_indicator_values_in_database(
                    indicator_values_models["existing"])
        t_1 = time.process_time()
        print(f"Indicator values data imported in {t_1 - t_0:.0f} seconds")

    @staticmethod
    def __delete_indicator_values_from_database():
        """
        Delete all existing indicator values from database
        """
        print("Deleting indicator values from database...")
        indicator_values = IndicatorValues.objects.all()
        total_nb_indicator_values = indicator_values.count()
        indicator_values.delete()
        deleted_indicator_values = total_nb_indicator_values - indicator_values.count()
        print(
            f"Successfully deleted {deleted_indicator_values} indicator values")

    @classmethod
    def __get_indicator_values_models_from_valid_rows(cls, reader, save_invalid_rows):
        """
        Get a list of indicator values models from all valid rows of the
        input csv. Save not valid rows into an output csv.
        """
        indicator_values_models = {"existing": [], "new": []}
        invalid_rows = []

        if Taxon.objects.all().count() == 0:
            print("No taxon in database to try assigning indicator values with")
            return indicator_values_models

        id_of_taxa_already_targeted = [0] * Taxon.objects.last().id

        print("Loading data from provided file...")
        for row in reader:
            matching_taxa = Taxon.objects.filter(
                main_scientific_entry__name=row['scientific_name'])
            nb_of_matching_taxa = len(matching_taxa)
            if nb_of_matching_taxa == 0:
                invalid_rows.append(
                    cls.__get_row_with_embed_error(
                        row,
                        NO_MATCHING_TAXON_FOUND))
            elif nb_of_matching_taxa == 1:
                matching_taxon = matching_taxa.first()
                if id_of_taxa_already_targeted[matching_taxon.id] == 1:
                    invalid_rows.append(
                        cls.__get_row_with_embed_error(
                            row,
                            SAME_TAXON_MATCHED_BY_SEVERAL_INDICATOR_VALUES))
                else:
                    id_of_taxa_already_targeted[matching_taxon.id] = 1
                    indicator_values = IndicatorValues(
                        taxon=matching_taxon)
                    for indicator_key in cls.indicator_values_and_stdev_fields:
                        value = cls.__get_dict_decimal_value_or_none(
                            row, indicator_key)
                        setattr(indicator_values,
                                indicator_key, value)
                    try:
                        indicator_values.full_clean()
                        indicator_values_models["new"].append(
                            indicator_values)
                    except ValidationError as error:
                        if (len(error.messages) == 1 and
                                INDICATOR_VALUES_ALREADY_EXISTS_ERROR_MESSAGE in error.messages[0]):
                            indicator_values_models["existing"].append(
                                indicator_values)
                        else:
                            invalid_rows.append(
                                cls.__get_row_with_embed_error(row, error.messages))
            else:
                invalid_rows.append(
                    cls.__get_row_with_embed_error(
                        row,
                        INDICATOR_VALUES_MATCH_SEVERAL_TAXA))

        print(
            f"Found {len(indicator_values_models['existing'])} indicator values rows to update")
        print(
            f"Found {len(indicator_values_models['new'])} indicator values rows to insert")
        print(
            f"Found {len(invalid_rows)} invalid indicator values rows")

        if save_invalid_rows:
            cls.__save_invalid_rows_to_csv(invalid_rows)

        return indicator_values_models

    @staticmethod
    def __get_dict_decimal_value_or_none(dictionnary, field_name):
        """
        Get the decimal value of the specified field from
        the dictionary or none if it's not an decimal
        """
        try:
            Decimal(dictionnary[field_name])
            return dictionnary[field_name]
        except InvalidOperation:
            return None

    @classmethod
    def __save_invalid_rows_to_csv(cls, invalid_rows):
        """
        Export all not imported rows into a csv
        """
        with open(f'{cls.invalid_rows_output_csv_name}.csv',
                  'w',
                  encoding='UTF8') as csvfile:
            writer = csv.DictWriter(
                csvfile, cls.invalid_rows_output_csv_header)
            writer.writeheader()
            writer.writerows(invalid_rows)

    @staticmethod
    def __get_row_with_embed_error(invalid_row, importation_error_message):
        """
        Embed an importation error message into an invalid row
        """
        invalid_row.update({"importation_error": importation_error_message})
        return invalid_row

    @staticmethod
    def __update_indicator_values_in_database(indicator_values_models):
        """
        Update indicator values in database
        from a given list of models
        """
        nb_updated = 0
        print("Updating indicators values in database")
        for indicator_values in indicator_values_models:
            indicator_values.save(force_update=True)
            nb_updated += 1
        print(f"Successfully updated {nb_updated} indicator values")

    @staticmethod
    def __insert_indicator_values_in_database(indicator_values_models):
        """
        Insert new indicator values in database
        from a given list of models
        """
        nb_inserted = 0
        print("Inserting new indicators values in database")
        for indicator_values in indicator_values_models:
            indicator_values.save(force_insert=True)
            nb_inserted += 1
        print(f"Successfully inserted {nb_inserted} new indicator values")
