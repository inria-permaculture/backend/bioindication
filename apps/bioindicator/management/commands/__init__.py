"""
Django specific commands
"""

from bioindicator.management.commands.import_indicators_values import *
from bioindicator.management.commands.import_plants_usages import *
