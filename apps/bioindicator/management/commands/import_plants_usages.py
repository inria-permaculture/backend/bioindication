"""
Django command to import plants usages from a csv file
"""
import csv
import time
from django.core.management.base import BaseCommand
from core.models import Taxon, PlantUsages


NO_MATCHING_TAXON_FOUND = "NO_MATCHING_TAXON_FOUND"
SEVERAL_MATCHING_TAXA_FOUND = "SEVERAL_MATCHING_TAXA_FOUND"
NO_MATCHING_USAGE_FOUND = "NO_MATCHING_USAGE_FOUND"


class Command(BaseCommand):
    """
    Import plants usages data from a csv file.
    """
    help = "Import plants usages data from a csv file."
    plants_usages_csv_header = ("identifier", "scientific_name", "usage")

    invalid_rows_output_csv_header = ("identifier", "scientific_name", "usage",
                                      "importation_error")
    invalid_rows_output_csv_name = "not_imported_plants_usages"

    plant_usages_fields_translations = {
        "alimentaire": "food",
        "mellifère": "honey",
        "ornemental": "ornamental",
        "medicinal": "medicine",
        "chauffage": "fuelwood",
        "construction": "construction",
        "psychotrope": "psychotropic",
        "toxique": "poisonous",
        "engrais_vert": "green_manure",
        "vannerie": "basketry",
    }

    def add_arguments(self, parser):
        parser.add_argument("file_path", nargs="+", type=str)
        parser.add_argument('--output', action='store_true',
                            help='Generate an output csv file storing all invalid plants usages')

    def handle(self, *args, **options):
        t_0 = time.process_time()

        with open(options["file_path"][0], encoding="utf8") as csvfile:
            reader = csv.DictReader(
                csvfile, self.plants_usages_csv_header)
            reader.__next__()  # skip the csv header
            self.__get_plants_usages_models_from_valid_rows(
                reader, options['output'])

        t_1 = time.process_time()
        print(f"Plants usages data imported in {t_1 - t_0:.0f} seconds")

    @classmethod
    def __get_plants_usages_models_from_valid_rows(cls, reader, save_invalid_rows):
        """
        Import plants usages from all valid rows of the
        input csv. Save invalid rows(with 0 or > 1 match with taxon)
        into an output csv if asked.
        """
        invalid_rows = []
        nb_usages_updated = 0

        if Taxon.objects.all().count() == 0:
            print("No taxon in database to try assigning plants usages with")
            return

        print("Loading data from provided file...")
        for row in reader:
            matching_taxa = Taxon.objects.filter(is_a_plant=True,
                                                 main_scientific_entry__name=row['scientific_name'])
            nb_of_matching_taxa = len(matching_taxa)
            if nb_of_matching_taxa == 1:
                matching_taxon = matching_taxa.first()
                plant_usages = PlantUsages.objects.get(
                    taxon=matching_taxon)
                usage_in_french = row['usage']
                if usage_in_french in cls.plant_usages_fields_translations:
                    setattr(plant_usages,
                            cls.plant_usages_fields_translations[usage_in_french], True)
                    plant_usages.save(
                        update_fields=[cls.plant_usages_fields_translations[usage_in_french]])
                    nb_usages_updated += 1
                else:
                    invalid_rows.append(
                        cls.__get_row_with_embed_error(
                            row,
                            NO_MATCHING_USAGE_FOUND))
            elif nb_of_matching_taxa == 0:
                invalid_rows.append(
                    cls.__get_row_with_embed_error(
                        row,
                        NO_MATCHING_TAXON_FOUND))
            else:
                invalid_rows.append(
                    cls.__get_row_with_embed_error(
                        row,
                        SEVERAL_MATCHING_TAXA_FOUND))

        print(
            f"Found {len(invalid_rows)} invalid rows")
        print(f"Successfully updated {nb_usages_updated} plant usages")

        if save_invalid_rows:
            cls.__save_invalid_rows_to_csv(invalid_rows)

    @classmethod
    def __save_invalid_rows_to_csv(cls, invalid_rows):
        """
        Export all not imported rows into a csv.
        """
        with open(f'{cls.invalid_rows_output_csv_name}.csv',
                  'w',
                  encoding='UTF8') as csvfile:
            writer = csv.DictWriter(
                csvfile, cls.invalid_rows_output_csv_header)
            writer.writeheader()
            writer.writerows(invalid_rows)

    @staticmethod
    def __get_row_with_embed_error(invalid_row, importation_error_message):
        """
        Embed an importation error message into an invalid row.
        """
        invalid_row.update({"importation_error": importation_error_message})
        return invalid_row
