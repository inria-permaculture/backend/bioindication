# pylint: disable=no-member
"""
Plant usages model transformations
"""
from rest_framework import serializers
from core.models import PlantUsages


class PlantUsagesSerializer(serializers.ModelSerializer):
    """
    Plant usages serializer
    """

    class Meta:
        model = PlantUsages
        fields = '__all__'


class PartialUpdatePlantUsagesSerializer(serializers.ModelSerializer):
    """
    Plant usages serializer for update action
    """

    class Meta:
        model = PlantUsages
        fields = ("food", "honey", "ornamental",
                  "medicine", "fuelwood", "construction",
                  "psychotropic", "poisonous", "green_manure",
                  "basketry", "depolluting", "textile",
                  "paper", "tinctorial", "fragrance",
                  "stratum", "invasive")


class PlantUsagesHistorySerializer(serializers.ModelSerializer):
    """Plant usages entries history serializer"""
    class Meta:
        model = PlantUsages.history.model
        fields = ("history_date", "history_type", "food", "honey",
                  "ornamental", "medicine", "fuelwood", "construction",
                  "psychotropic", "poisonous", "green_manure", "basketry",
                  "depolluting", "textile", "paper", "tinctorial",
                  "fragrance", "stratum", "invasive")
