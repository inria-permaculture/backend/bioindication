"""
Inventories analysis data transformations
"""
from rest_framework import serializers


class InventoriesAnalysisSerializerIndicatorSerializer(serializers.Serializer):
    # pylint: disable=abstract-method
    """
    Inventories analysis indicator value serializer
    """
    mean = serializers.DecimalField(
        max_digits=3, decimal_places=1, allow_null=True)
    standard_deviation = serializers.DecimalField(
        max_digits=4, decimal_places=2, allow_null=True)


class InventoriesAnalysisSerializer(serializers.Serializer):
    # pylint: disable=abstract-method
    """
    Inventories analysis serializer
    """
    light = InventoriesAnalysisSerializerIndicatorSerializer()
    temperature = InventoriesAnalysisSerializerIndicatorSerializer()
    continentality = InventoriesAnalysisSerializerIndicatorSerializer()
    edaphic_moisture = InventoriesAnalysisSerializerIndicatorSerializer()
    atmospheric_moisture = InventoriesAnalysisSerializerIndicatorSerializer()
    soil_reaction = InventoriesAnalysisSerializerIndicatorSerializer()
    nutrient = InventoriesAnalysisSerializerIndicatorSerializer()
    salinity = InventoriesAnalysisSerializerIndicatorSerializer()
    soil_texture = InventoriesAnalysisSerializerIndicatorSerializer()
    organic_matter = InventoriesAnalysisSerializerIndicatorSerializer()
