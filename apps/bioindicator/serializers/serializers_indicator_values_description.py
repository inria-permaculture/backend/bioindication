"""
Indicator values description model transformations
"""
from rest_framework import serializers
from core.models import IndicatorValuesDescription


class IndicatorValuesDescriptionSerializer(serializers.ModelSerializer):
    """
    Indicator values description serializer
    """

    class Meta:
        model = IndicatorValuesDescription
        fields = '__all__'
