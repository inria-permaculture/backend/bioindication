"""
Indicator values model transformations
"""
from rest_framework import serializers
from core.models import IndicatorValues


class IndicatorValuesSerializer(serializers.ModelSerializer):
    """
    Indicator values serializer
    """

    class Meta:
        model = IndicatorValues
        fields = '__all__'
