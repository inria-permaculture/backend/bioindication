"""
Inventories analysis data transformations
"""
from rest_framework import serializers


class InventoryBiodiversitySerializer(serializers.Serializer):
    # pylint: disable=abstract-method
    """
    Inventory biodiversity serializer
    """
    index = serializers.IntegerField()
