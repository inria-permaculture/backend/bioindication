"""
Inventory suggestions data transformations
"""
from rest_framework import serializers
from core.models import Taxon, PlantUsages, VernacularEntry


class SuggestedTaxonVernaculars(serializers.RelatedField):
    # pylint: disable=abstract-method, too-few-public-methods, missing-class-docstring
    """
    Suggested taxon vernacular serializer
    """

    def to_representation(self, value):
        return value.name

    class Meta:
        model = VernacularEntry


class SuggestedTaxonUsages(serializers.ModelSerializer):
    """
    Suggested taxon usages serializer
    """
    class Meta:
        model = PlantUsages
        fields = ("food", "honey", "ornamental",
                  "medicine", "fuelwood", "construction",
                  "psychotropic", "poisonous", "green_manure",
                  "basketry", "depolluting", "textile",
                  "paper", "tinctorial", "fragrance",
                  "stratum", "invasive")


class SuggestedTaxon(serializers.ModelSerializer):
    """
    Suggested taxon serializer
    """
    name = serializers.CharField(source="main_scientific_entry.name")
    vernacular_entries = SuggestedTaxonVernaculars(read_only=True, many=True)
    plant_usages = SuggestedTaxonUsages(read_only=True, many=False)

    class Meta:
        model = Taxon
        fields = ("id", "rank", "name", "vernacular_entries",
                  "plant_usages", "is_domesticated")


class InventorySuggestionsSerializer(serializers.Serializer):
    # pylint: disable=abstract-method
    """
    Inventory suggestions serializer
    """
    taxon = SuggestedTaxon(read_only=True, many=False)
    distance = serializers.FloatField()
    score = serializers.DecimalField(None, 2)


class InventorySuggestionsFiltersQuerySerializer(serializers.Serializer):
    # pylint: disable=abstract-method
    """
    Inventory suggestions filters query serializer
    """
    food = serializers.BooleanField(required=False)
    honey = serializers.BooleanField(required=False)
    ornamental = serializers.BooleanField(required=False)
    medicine = serializers.BooleanField(required=False)
    fuelwood = serializers.BooleanField(required=False)
    construction = serializers.BooleanField(required=False)
    psychotropic = serializers.BooleanField(required=False)
    poisonous = serializers.BooleanField(required=False)
    green_manure = serializers.BooleanField(required=False)
    basketry = serializers.BooleanField(required=False)
    depolluting = serializers.BooleanField(required=False)
    textile = serializers.BooleanField(required=False)
    paper = serializers.BooleanField(required=False)
    tinctorial = serializers.BooleanField(required=False)
    fragrance = serializers.BooleanField(required=False)
    stratum = serializers.CharField(required=False)
    invasive = serializers.BooleanField(required=False)
    is_domesticated = serializers.BooleanField(required=False)
