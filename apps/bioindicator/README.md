# Bioindication - backend

## Architecture
If you want to understand this repository architecture, here the detailled folder structure :

    fixtures -> Data for the pre-filling of the database
    management -> Specifics commands implementations folder for manage.py
    serializers -> Data transformations folder
    services -> Business logic wrappers
    tests -> Unit/Integration tests folder
    views -> REST endpoints and related
    viewsets -> REST endpoints and related
    __init__.py -> Make this directory considered as a Python package
    urls.py -> The URL declarations for this Django application
