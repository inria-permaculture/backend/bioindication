"""
Test classes for viewsets
"""

from bioindicator.tests.tests_viewsets.tests_viewset_indicator_values import *
from bioindicator.tests.tests_viewsets.tests_viewset_plant_usages import *
