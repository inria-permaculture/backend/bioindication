# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import IndicatorValues, Taxon


class RetrieveIndicatorValuesViewsetTestCase(APITestCase):
    """
    Test the retrieve indicator values view
    """

    def test_get_an_existing_indicator_values_returns_http_code_200(self):
        """
        Test get an existing indicator values must return a http code 200
        """
        taxon = Taxon.objects.create(id=1)
        IndicatorValues.objects.create(taxon=taxon)

        response = self.client.get(
            reverse.reverse(
                "indicator-values-detail", args=[taxon.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_an_existing_indicator_values_returns_valid_data(self):
        """
        Test get an existing indicator values must return all data
        """
        taxon = Taxon.objects.create(id=1)
        indicator_values = IndicatorValues.objects.create(
            taxon=taxon,
            light=1,
            light_stdev=0,
            temperature=1.5,
            temperature_stdev=0,
            continentality=2,
            continentality_stdev=0,
            edaphic_moisture=2.5,
            edaphic_moisture_stdev=0,
            atmospheric_moisture=3,
            atmospheric_moisture_stdev=0,
            soil_reaction=3.5,
            soil_reaction_stdev=0,
            nutrient=4,
            nutrient_stdev=0,
            salinity=4.5,
            salinity_stdev=0,
            soil_texture=5,
            soil_texture_stdev=0,
            organic_matter=5.5,
            organic_matter_stdev=0,
        )

        response = self.client.get(
            reverse.reverse(
                "indicator-values-detail", args=[taxon.id]),
            format="json")

        self.assertEqual(indicator_values.taxon.id,
                         response.data["taxon"])
        self.assertEqual(indicator_values.light,
                         response.data["light"])
        self.assertEqual(indicator_values.light_stdev,
                         response.data["light_stdev"])
        self.assertEqual(indicator_values.temperature,
                         response.data["temperature"])
        self.assertEqual(indicator_values.temperature_stdev,
                         response.data["temperature_stdev"])
        self.assertEqual(indicator_values.continentality,
                         response.data["continentality"])
        self.assertEqual(indicator_values.continentality_stdev,
                         response.data["continentality_stdev"])
        self.assertEqual(indicator_values.edaphic_moisture,
                         response.data["edaphic_moisture"])
        self.assertEqual(indicator_values.edaphic_moisture_stdev,
                         response.data["edaphic_moisture_stdev"])
        self.assertEqual(indicator_values.atmospheric_moisture,
                         response.data["atmospheric_moisture"])
        self.assertEqual(indicator_values.atmospheric_moisture_stdev,
                         response.data["atmospheric_moisture_stdev"])
        self.assertEqual(indicator_values.soil_reaction,
                         response.data["soil_reaction"])
        self.assertEqual(indicator_values.soil_reaction_stdev,
                         response.data["soil_reaction_stdev"])
        self.assertEqual(indicator_values.nutrient,
                         response.data["nutrient"])
        self.assertEqual(indicator_values.nutrient_stdev,
                         response.data["nutrient_stdev"])
        self.assertEqual(indicator_values.salinity,
                         response.data["salinity"])
        self.assertEqual(indicator_values.salinity_stdev,
                         response.data["salinity_stdev"])
        self.assertEqual(indicator_values.soil_texture,
                         response.data["soil_texture"])
        self.assertEqual(indicator_values.soil_texture_stdev,
                         response.data["soil_texture_stdev"])
        self.assertEqual(indicator_values.organic_matter,
                         response.data["organic_matter"])
        self.assertEqual(indicator_values.organic_matter_stdev,
                         response.data["organic_matter_stdev"])

    def test_get_a_non_existing_indicator_values_returns_http_code_404(self):
        """
        Test get a non existing indicator values must return a http code 404
        """
        non_existing_indicator_values_id = 999
        response = self.client.get(
            reverse.reverse(
                "indicator-values-detail", args=[non_existing_indicator_values_id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
