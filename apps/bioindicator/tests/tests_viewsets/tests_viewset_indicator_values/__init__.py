"""
Test classes for indicator values viewsets
"""

from bioindicator.tests.tests_viewsets.tests_viewset_indicator_values.\
    tests_view_retrieve_indicator_values import *
