# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import PlantUsages, Taxon


class PartialUpdatePlantUsagesViewsetTestCase(APITestCase):
    """
    Test the partial update plant usages view
    """

    def test_partially_update_with_valid_data_returns_http_code_200(self):
        """
        Test partially update a plant usages with valid data must return a http code 200
        """
        plant_usages_data = {
            "food": True,
            "honey": False,
            "ornamental": True,
            "medicine": False,
            "fuelwood": True,
            "construction": False,
            "psychotropic": True,
            "poisonous": False,
            "green_manure": True,
            "basketry": False
        }
        taxon = Taxon.objects.create(id=1)
        PlantUsages.objects.create(
            taxon=taxon)

        response = self.client.patch(
            reverse.reverse(
                "plant-usages-detail", args=[taxon.id]),
            data={**plant_usages_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partially_update_with_valid_data_returns_valid_data(self):
        """
        Test partially update a plant usages with valid data must return valid data
        """
        plant_usages_data = {
            "food": True,
            "honey": False,
            "ornamental": True,
            "medicine": False,
            "fuelwood": True,
            "construction": False,
            "psychotropic": True,
            "poisonous": False,
            "green_manure": True,
            "basketry": False
        }
        taxon = Taxon.objects.create(id=1)
        PlantUsages.objects.create(
            taxon=taxon)

        response = self.client.patch(
            reverse.reverse(
                "plant-usages-detail", args=[taxon.id]),
            data={**plant_usages_data},
            format="json")

        updated_plant_usages = PlantUsages.objects.get(
            taxon=taxon)
        self.assertEqual(response.data["food"],
                         updated_plant_usages.food)
        self.assertEqual(response.data["honey"],
                         updated_plant_usages.honey)
        self.assertEqual(response.data["ornamental"],
                         updated_plant_usages.ornamental)
        self.assertEqual(response.data["medicine"],
                         updated_plant_usages.medicine)
        self.assertEqual(response.data["fuelwood"],
                         updated_plant_usages.fuelwood)
        self.assertEqual(response.data["construction"],
                         updated_plant_usages.construction)
        self.assertEqual(response.data["psychotropic"],
                         updated_plant_usages.psychotropic)
        self.assertEqual(response.data["poisonous"],
                         updated_plant_usages.poisonous)
        self.assertEqual(response.data["green_manure"],
                         updated_plant_usages.green_manure)
        self.assertEqual(response.data["basketry"],
                         updated_plant_usages.basketry)

    def test_partially_update_with_valid_data_really_update_it(self):
        """
        Test partially update a plant usages with valid data must update it
        """
        plant_usages_data = {
            "food": True,
            "honey": False,
            "ornamental": True,
            "medicine": False,
            "fuelwood": True,
            "construction": False,
            "psychotropic": True,
            "poisonous": False,
            "green_manure": True,
            "basketry": False
        }
        taxon = Taxon.objects.create(id=1)
        PlantUsages.objects.create(
            taxon=taxon)

        self.client.patch(
            reverse.reverse(
                "plant-usages-detail", args=[taxon.id]),
            data={**plant_usages_data},
            format="json")

        updated_plant_usages = PlantUsages.objects.get(
            taxon=taxon)
        self.assertEqual(updated_plant_usages.food,
                         plant_usages_data["food"])
        self.assertEqual(updated_plant_usages.honey,
                         plant_usages_data["honey"])
        self.assertEqual(updated_plant_usages.ornamental,
                         plant_usages_data["ornamental"])
        self.assertEqual(updated_plant_usages.medicine,
                         plant_usages_data["medicine"])
        self.assertEqual(updated_plant_usages.fuelwood,
                         plant_usages_data["fuelwood"])
        self.assertEqual(updated_plant_usages.construction,
                         plant_usages_data["construction"])
        self.assertEqual(updated_plant_usages.psychotropic,
                         plant_usages_data["psychotropic"])
        self.assertEqual(updated_plant_usages.poisonous,
                         plant_usages_data["poisonous"])
        self.assertEqual(updated_plant_usages.green_manure,
                         plant_usages_data["green_manure"])
        self.assertEqual(updated_plant_usages.basketry,
                         plant_usages_data["basketry"])

    def test_partially_update_a_non_existing_plant_usages_returns_http_code_404(self):
        """
        Test partially update a non existing plant usages must return a http code 404
        """
        plant_usages_data = {
            "food": True,
        }
        non_existing_plant_usages_id_to_partially_update = 999

        response = self.client.patch(
            reverse.reverse(
                "inventory-detail", args=[non_existing_plant_usages_id_to_partially_update]),
            data={**plant_usages_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_partially_update_a_plant_usages_with_a_non_bool_value_returns_http_code_400(self):
        """
        Test partially update a plant usages with a non bool value must return a http code 400
        """
        plant_usages_data = {
            "food": True,
            "honey": "nonBoolValue"
        }
        taxon = Taxon.objects.create(id=1)
        PlantUsages.objects.create(
            taxon=taxon)

        response = self.client.patch(
            reverse.reverse(
                "plant-usages-detail", args=[taxon.id]),
            data={**plant_usages_data},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
