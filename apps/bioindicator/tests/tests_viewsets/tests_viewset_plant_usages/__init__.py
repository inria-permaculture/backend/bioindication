"""
Test classes for indicator values viewsets
"""

from bioindicator.tests.tests_viewsets.tests_viewset_plant_usages\
    .tests_view_partial_update_plant_usages import *
from bioindicator.tests.tests_viewsets.tests_viewset_plant_usages\
    .tests_view_retrieve_plant_usages import *
