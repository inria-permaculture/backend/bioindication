# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import PlantUsages, Taxon


class RetrievePlantUsagesViewsetTestCase(APITestCase):
    """
    Test the retrieve indicator values view
    """

    def test_get_an_existing_plant_usages_returns_http_code_200(self):
        """
        Test get an existing indicator values must return a http code 200
        """
        taxon = Taxon.objects.create(id=1)
        PlantUsages.objects.create(taxon=taxon)

        response = self.client.get(
            reverse.reverse(
                "plant-usages-detail", args=[taxon.id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_an_existing_plant_usages_returns_valid_data(self):
        """
        Test get an existing indicator values must return all data
        """
        taxon = Taxon.objects.create(id=1)
        plant_usages = PlantUsages.objects.create(
            taxon=taxon,
            food=True,
            honey=False,
            ornamental=True,
            medicine=False,
            fuelwood=True,
            construction=False,
            psychotropic=True,
            poisonous=False,
            green_manure=True,
            basketry=False
        )

        response = self.client.get(
            reverse.reverse(
                "plant-usages-detail", args=[taxon.id]),
            format="json")

        self.assertEqual(plant_usages.taxon.id,
                         response.data["taxon"])
        self.assertEqual(plant_usages.food,
                         response.data["food"])
        self.assertEqual(plant_usages.honey,
                         response.data["honey"])
        self.assertEqual(plant_usages.ornamental,
                         response.data["ornamental"])
        self.assertEqual(plant_usages.medicine,
                         response.data["medicine"])
        self.assertEqual(plant_usages.fuelwood,
                         response.data["fuelwood"])
        self.assertEqual(plant_usages.construction,
                         response.data["construction"])
        self.assertEqual(plant_usages.psychotropic,
                         response.data["psychotropic"])
        self.assertEqual(plant_usages.poisonous,
                         response.data["poisonous"])
        self.assertEqual(plant_usages.green_manure,
                         response.data["green_manure"])
        self.assertEqual(plant_usages.basketry,
                         response.data["basketry"])

    def test_get_a_non_existing_plant_usages_returns_http_code_404(self):
        """
        Test get a non existing plant usages must return a http code 404
        """
        non_existing_plant_usages_id = 999
        response = self.client.get(
            reverse.reverse(
                "plant-usages-detail", args=[non_existing_plant_usages_id]),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
