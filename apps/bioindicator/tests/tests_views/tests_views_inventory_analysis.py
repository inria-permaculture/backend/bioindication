# pylint: disable=missing-module-docstring
from decimal import Decimal
from rest_framework import reverse, status
from rest_framework.test import APITestCase
from core.models import InventoryList, Inventory, Taxon, Observation, IndicatorValues


class TestInventoryAnalysisApiViewTestCase(APITestCase):
    """
    Test the inventory analysis view
    """

    def test_inventory_analys_on_a_non_existing_inventory_returns_http_code_404(self):
        """
        Test generating an analysis of a non existing inventory must
        return http code 404
        """
        non_existing_inventory_id = 999

        analysis = self.client.get(
            reverse.reverse(
                "inventory-analysis",
                args=[non_existing_inventory_id]),
            format="json")

        self.assertEqual(analysis.status_code, status.HTTP_404_NOT_FOUND)

    def test_inv_analys_without_observation_returns_empty_analys(self):
        """
        Test generating an analysis on an inventory without any observation
        must return an empty analysis
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)

        expected_analys = {
            "light": {"mean": None, "standard_deviation": None},
            "temperature": {"mean": None, "standard_deviation": None},
            "continentality": {"mean": None, "standard_deviation": None},
            "edaphic_moisture": {"mean": None, "standard_deviation": None},
            "atmospheric_moisture": {"mean": None, "standard_deviation": None},
            "soil_reaction": {"mean": None, "standard_deviation": None},
            "nutrient": {"mean": None, "standard_deviation": None},
            "salinity": {"mean": None, "standard_deviation": None},
            "soil_texture": {"mean": None, "standard_deviation": None},
            "organic_matter": {"mean": None, "standard_deviation": None},
        }

        analysis = self.client.get(
            reverse.reverse(
                "inventory-analysis",
                args=[inventory.id]),
            format="json")

        self.assertEqual(analysis.data, expected_analys)
        self.assertEqual(analysis.status_code, status.HTTP_200_OK)

    def test_inv_analys_without_indic_values_returns_empty_analys(self):
        """
        Test generating an analysis on an inventory with observations but
        without any related indicator values must return an empty analysis
        """
        taxon1 = Taxon.objects.create(id=1, is_a_plant=True)
        taxon2 = Taxon.objects.create(id=2, is_a_plant=True)
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)
        Observation.objects.create(
            inventory=inventory, taxon=taxon1)
        Observation.objects.create(
            inventory=inventory, taxon=taxon2)
        expected_analys = {
            "light": {"mean": None, "standard_deviation": None},
            "temperature": {"mean": None, "standard_deviation": None},
            "continentality": {"mean": None, "standard_deviation": None},
            "edaphic_moisture": {"mean": None, "standard_deviation": None},
            "atmospheric_moisture": {"mean": None, "standard_deviation": None},
            "soil_reaction": {"mean": None, "standard_deviation": None},
            "nutrient": {"mean": None, "standard_deviation": None},
            "salinity": {"mean": None, "standard_deviation": None},
            "soil_texture": {"mean": None, "standard_deviation": None},
            "organic_matter": {"mean": None, "standard_deviation": None},
        }

        analysis = self.client.get(
            reverse.reverse(
                "inventory-analysis",
                args=[inventory.id]),
            format="json")

        self.assertEqual(analysis.data, expected_analys)
        self.assertEqual(analysis.status_code, status.HTTP_200_OK)

    def test_inv_analys_with_only_non_plants_taxa_returns_empty_analys(self):
        """
        Test generating an analysis on an inventory with observations but
        only of non plants taxa must return an empty analysis
        """
        taxon1 = Taxon.objects.create(id=1, is_a_plant=False)
        taxon2 = Taxon.objects.create(id=2, is_a_plant=False)
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)
        Observation.objects.create(
            inventory=inventory, taxon=taxon1)
        Observation.objects.create(
            inventory=inventory, taxon=taxon2)
        expected_analys = {
            "light": {"mean": None, "standard_deviation": None},
            "temperature": {"mean": None, "standard_deviation": None},
            "continentality": {"mean": None, "standard_deviation": None},
            "edaphic_moisture": {"mean": None, "standard_deviation": None},
            "atmospheric_moisture": {"mean": None, "standard_deviation": None},
            "soil_reaction": {"mean": None, "standard_deviation": None},
            "nutrient": {"mean": None, "standard_deviation": None},
            "salinity": {"mean": None, "standard_deviation": None},
            "soil_texture": {"mean": None, "standard_deviation": None},
            "organic_matter": {"mean": None, "standard_deviation": None},
        }

        analysis = self.client.get(
            reverse.reverse(
                "inventory-analysis",
                args=[inventory.id]),
            format="json")

        self.assertEqual(analysis.data, expected_analys)
        self.assertEqual(analysis.status_code, status.HTTP_200_OK)

    def test_inv_analys_with_quantif_obs_and_all_indic_values_returns_valid_analys(self):
        """
        Test generating an analysis on an inventory with quantified observations
        and with all indicator values must return a valid analysis
        """
        taxon1 = Taxon.objects.create(id=1, is_a_plant=True)
        taxon2 = Taxon.objects.create(id=2, is_a_plant=True)
        IndicatorValues.objects.create(
            taxon=taxon1,
            light=6,
            temperature=6,
            continentality=6,
            edaphic_moisture=6,
            atmospheric_moisture=6,
            soil_reaction=6,
            nutrient=6,
            salinity=6,
            soil_texture=6,
            organic_matter=6,
        )
        IndicatorValues.objects.create(
            taxon=taxon2,
            light=4,
            temperature=4,
            continentality=4,
            edaphic_moisture=4,
            atmospheric_moisture=4,
            soil_reaction=4,
            nutrient=4,
            salinity=4,
            soil_texture=4,
            organic_matter=4,
        )
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)
        Observation.objects.create(
            inventory=inventory, taxon=taxon1, soil_coverage=0.95)
        Observation.objects.create(
            inventory=inventory, taxon=taxon2, soil_coverage=0.25)
        expected_analys = {
            "light": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "temperature": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "continentality": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "edaphic_moisture": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "atmospheric_moisture": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "soil_reaction": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "nutrient": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "salinity": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "soil_texture": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "organic_matter": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
        }

        analysis = self.client.get(
            reverse.reverse(
                "inventory-analysis",
                args=[inventory.id]),
            format="json")

        self.assertEqual(analysis.data, expected_analys)
        self.assertEqual(analysis.status_code, status.HTTP_200_OK)

    def test_inv_analys_with_some_not_quantif_obs_but_all_indic_values_returns_valid_analys(self):
        """
        Test generating an analysis on an inventory with some not quantified observations
        but with all indicator values must return a valid analysis
        """
        taxon1 = Taxon.objects.create(id=1, is_a_plant=True)
        taxon2 = Taxon.objects.create(id=2, is_a_plant=True)
        taxon3 = Taxon.objects.create(id=3, is_a_plant=True)
        IndicatorValues.objects.create(
            taxon=taxon1,
            light=6,
            temperature=6,
            continentality=6,
            edaphic_moisture=6,
            atmospheric_moisture=6,
            soil_reaction=6,
            nutrient=6,
            salinity=6,
            soil_texture=6,
            organic_matter=6,
        )
        IndicatorValues.objects.create(
            taxon=taxon2,
            light=4,
            temperature=4,
            continentality=4,
            edaphic_moisture=4,
            atmospheric_moisture=4,
            soil_reaction=4,
            nutrient=4,
            salinity=4,
            soil_texture=4,
            organic_matter=4,
        )
        IndicatorValues.objects.create(
            taxon=taxon3,
            light=2,
            temperature=2,
            continentality=2,
            edaphic_moisture=2,
            atmospheric_moisture=2,
            soil_reaction=2,
            nutrient=2,
            salinity=2,
            soil_texture=2,
            organic_matter=2,
        )
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)
        Observation.objects.create(
            inventory=inventory, taxon=taxon1, soil_coverage=0.95)
        Observation.objects.create(
            inventory=inventory, taxon=taxon2, soil_coverage=0.25)
        # no soil_coverage defined for this observation
        Observation.objects.create(
            inventory=inventory, taxon=taxon3)
        expected_analys = {
            "light": {"mean": Decimal("4.4"), "standard_deviation": Decimal("2.22")},
            "temperature": {"mean": Decimal("4.4"), "standard_deviation": Decimal("2.22")},
            "continentality": {"mean": Decimal("4.4"), "standard_deviation": Decimal("2.22")},
            "edaphic_moisture": {"mean": Decimal("4.4"), "standard_deviation": Decimal("2.22")},
            "atmospheric_moisture": {"mean": Decimal("4.4"), "standard_deviation": Decimal("2.22")},
            "soil_reaction": {"mean": Decimal("4.4"), "standard_deviation": Decimal("2.22")},
            "nutrient": {"mean": Decimal("4.4"), "standard_deviation": Decimal("2.22")},
            "salinity": {"mean": Decimal("4.4"), "standard_deviation": Decimal("2.22")},
            "soil_texture": {"mean": Decimal("4.4"), "standard_deviation": Decimal("2.22")},
            "organic_matter": {"mean": Decimal("4.4"), "standard_deviation": Decimal("2.22")},
        }

        analysis = self.client.get(
            reverse.reverse(
                "inventory-analysis",
                args=[inventory.id]),
            format="json")

        self.assertEqual(analysis.data, expected_analys)
        self.assertEqual(analysis.status_code, status.HTTP_200_OK)

    def test_inv_analys_with_zero_quantif_obs_but_all_indic_values_returns_valid_analys(self):
        """
        Test generating an analysis on an inventory with 0 quantified observations
        but with all indicator values must return a valid analysis
        """
        taxon1 = Taxon.objects.create(id=1, is_a_plant=True)
        taxon2 = Taxon.objects.create(id=2, is_a_plant=True)
        taxon3 = Taxon.objects.create(id=3, is_a_plant=True)
        IndicatorValues.objects.create(
            taxon=taxon1,
            light=6,
            temperature=6,
            continentality=6,
            edaphic_moisture=6,
            atmospheric_moisture=6,
            soil_reaction=6,
            nutrient=6,
            salinity=6,
            soil_texture=6,
            organic_matter=6,
        )
        IndicatorValues.objects.create(
            taxon=taxon2,
            light=4,
            temperature=4,
            continentality=4,
            edaphic_moisture=4,
            atmospheric_moisture=4,
            soil_reaction=4,
            nutrient=4,
            salinity=4,
            soil_texture=4,
            organic_matter=4,
        )
        IndicatorValues.objects.create(
            taxon=taxon3,
            light=2,
            temperature=2,
            continentality=2,
            edaphic_moisture=2,
            atmospheric_moisture=2,
            soil_reaction=2,
            nutrient=2,
            salinity=2,
            soil_texture=2,
            organic_matter=2,
        )
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)
        # no soil_coverage defined for all observations
        Observation.objects.create(
            inventory=inventory, taxon=taxon1)
        Observation.objects.create(
            inventory=inventory, taxon=taxon2)
        Observation.objects.create(
            inventory=inventory, taxon=taxon3)
        expected_analys = {
            "light": {"mean": Decimal("4"), "standard_deviation": Decimal("2")},
            "temperature": {"mean": Decimal("4"), "standard_deviation": Decimal("2")},
            "continentality": {"mean": Decimal("4"), "standard_deviation": Decimal("2")},
            "edaphic_moisture": {"mean": Decimal("4"), "standard_deviation": Decimal("2")},
            "atmospheric_moisture": {"mean": Decimal("4"), "standard_deviation": Decimal("2")},
            "soil_reaction": {"mean": Decimal("4"), "standard_deviation": Decimal("2")},
            "nutrient": {"mean": Decimal("4"), "standard_deviation": Decimal("2")},
            "salinity": {"mean": Decimal("4"), "standard_deviation": Decimal("2")},
            "soil_texture": {"mean": Decimal("4"), "standard_deviation": Decimal("2")},
            "organic_matter": {"mean": Decimal("4"), "standard_deviation": Decimal("2")},
        }

        analysis = self.client.get(
            reverse.reverse(
                "inventory-analysis",
                args=[inventory.id]),
            format="json")

        self.assertEqual(analysis.data, expected_analys)
        self.assertEqual(analysis.status_code, status.HTTP_200_OK)

    def test_inv_analys_with_all_quantif_obs_but_some_indic_values_returns_valid_analys(self):
        """
        Test generating an analysis on an inventory with all observations quantified
        but with only some indicator values must return a valid analysis
        """
        taxon1 = Taxon.objects.create(id=1, is_a_plant=True)
        taxon2 = Taxon.objects.create(id=2, is_a_plant=True)
        taxon3 = Taxon.objects.create(id=3, is_a_plant=True)
        # 1 value is set to none for the taxon1
        IndicatorValues.objects.create(
            taxon=taxon1,
            light=6,
            temperature=6,
            continentality=6,
            edaphic_moisture=6,
            atmospheric_moisture=6,
            soil_reaction=6,
            nutrient=6,
            salinity=6,
            soil_texture=6,
            organic_matter=None,
        )
        # 4 values are set to none for the taxon2
        IndicatorValues.objects.create(
            taxon=taxon2,
            light=4,
            temperature=4,
            continentality=4,
            edaphic_moisture=4,
            atmospheric_moisture=4,
            soil_reaction=4,
            nutrient=None,
            salinity=None,
            soil_texture=None,
            organic_matter=None,
        )
        # all values are set to none for the taxon3
        IndicatorValues.objects.create(
            taxon=taxon3,
            light=None,
            temperature=None,
            continentality=None,
            edaphic_moisture=None,
            atmospheric_moisture=None,
            soil_reaction=None,
            nutrient=None,
            salinity=None,
            soil_texture=None,
            organic_matter=None,
        )
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)
        Observation.objects.create(
            inventory=inventory, taxon=taxon1, soil_coverage=0.25)
        Observation.objects.create(
            inventory=inventory, taxon=taxon2, soil_coverage=0.25)
        Observation.objects.create(
            inventory=inventory, taxon=taxon3, soil_coverage=0.25)
        expected_analys = {
            "light": {"mean": Decimal("5"), "standard_deviation": Decimal("1")},
            "temperature": {"mean": Decimal("5"), "standard_deviation": Decimal("1")},
            "continentality": {"mean": Decimal("5"), "standard_deviation": Decimal("1")},
            "edaphic_moisture": {"mean": Decimal("5"), "standard_deviation": Decimal("1")},
            "atmospheric_moisture": {"mean": Decimal("5"), "standard_deviation": Decimal("1")},
            "soil_reaction": {"mean": Decimal("5"), "standard_deviation": Decimal("1")},
            "nutrient": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "salinity": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "soil_texture": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            # all values are none for this indicator so mean and stdev is none
            "organic_matter": {"mean": None, "standard_deviation": None},
        }

        analysis = self.client.get(
            reverse.reverse(
                "inventory-analysis",
                args=[inventory.id]),
            format="json")

        self.assertEqual(analysis.data, expected_analys)
        self.assertEqual(analysis.status_code, status.HTTP_200_OK)

    def test_inv_analys_with_some_not_quantif_obs_and_some_indic_values_returns_valid_analys(self):
        """
        Test generating an analysis on an inventory with some not quantified observations
        and with some indicator values must return a valid analysis
        """
        taxon1 = Taxon.objects.create(id=1, is_a_plant=True)
        taxon2 = Taxon.objects.create(id=2, is_a_plant=True)
        taxon3 = Taxon.objects.create(id=3, is_a_plant=True)
        # 1 value is set to none for the taxon1
        IndicatorValues.objects.create(
            taxon=taxon1,
            light=6,
            temperature=6,
            continentality=6,
            edaphic_moisture=6,
            atmospheric_moisture=6,
            soil_reaction=6,
            nutrient=6,
            salinity=6,
            soil_texture=6,
            organic_matter=None,
        )
        # 4 values are set to none for the taxon2
        IndicatorValues.objects.create(
            taxon=taxon2,
            light=4,
            temperature=4,
            continentality=4,
            edaphic_moisture=4,
            atmospheric_moisture=4,
            soil_reaction=4,
            nutrient=None,
            salinity=None,
            soil_texture=None,
            organic_matter=None,
        )
        # 4 values are set to none for the taxon3
        IndicatorValues.objects.create(
            taxon=taxon3,
            light=5,
            temperature=5,
            continentality=5,
            edaphic_moisture=5,
            atmospheric_moisture=5,
            soil_reaction=5,
            nutrient=None,
            salinity=None,
            soil_texture=None,
            organic_matter=None,
        )
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)
        Observation.objects.create(
            inventory=inventory, taxon=taxon1, soil_coverage=0.25)
        Observation.objects.create(
            inventory=inventory, taxon=taxon2, soil_coverage=0.25)
        # no soil_coverage defined for this observation
        Observation.objects.create(
            inventory=inventory, taxon=taxon3)
        expected_analys = {
            "light": {"mean": Decimal("5"), "standard_deviation": Decimal("1")},
            "temperature": {"mean": Decimal("5"), "standard_deviation": Decimal("1")},
            "continentality": {"mean": Decimal("5"), "standard_deviation": Decimal("1")},
            "edaphic_moisture": {"mean": Decimal("5"), "standard_deviation": Decimal("1")},
            "atmospheric_moisture": {"mean": Decimal("5"), "standard_deviation": Decimal("1")},
            "soil_reaction": {"mean": Decimal("5"), "standard_deviation": Decimal("1")},
            "nutrient": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "salinity": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "soil_texture": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            # all values are none for this indicator so mean and stdev is none
            "organic_matter": {"mean": None, "standard_deviation": None},
        }

        analysis = self.client.get(
            reverse.reverse(
                "inventory-analysis",
                args=[inventory.id]),
            format="json")

        self.assertEqual(analysis.data, expected_analys)
        self.assertEqual(analysis.status_code, status.HTTP_200_OK)

    def test_inv_analys_with_only_one_obs_returns_valid_analys(self):
        """
        Test generating an analysis on an inventory with only one observation
        must return a valid analysis
        """
        taxon1 = Taxon.objects.create(id=1, is_a_plant=True)
        IndicatorValues.objects.create(
            taxon=taxon1,
            light=6,
            temperature=6,
            continentality=6,
            edaphic_moisture=6,
            atmospheric_moisture=6,
            soil_reaction=6,
            nutrient=6,
            salinity=6,
            soil_texture=6,
            organic_matter=6,
        )
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)
        Observation.objects.create(
            inventory=inventory, taxon=taxon1, soil_coverage=1)
        expected_analys = {
            "light": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "temperature": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "continentality": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "edaphic_moisture": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "atmospheric_moisture": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "soil_reaction": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "nutrient": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "salinity": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "soil_texture": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
            "organic_matter": {"mean": Decimal("6"), "standard_deviation": Decimal("0")},
        }

        analysis = self.client.get(
            reverse.reverse(
                "inventory-analysis",
                args=[inventory.id]),
            format="json")

        self.assertEqual(analysis.data, expected_analys)
        self.assertEqual(analysis.status_code, status.HTTP_200_OK)
