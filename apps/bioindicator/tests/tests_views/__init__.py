"""
Test classes for views
"""

from bioindicator.tests.tests_views.tests_views_indicator_values_description import *
from bioindicator.tests.tests_views.tests_views_inventory_analysis import *
from bioindicator.tests.tests_views.tests_views_inventory_biodiversity import *
from bioindicator.tests.tests_views.tests_views_inventory_list_analysis import *
