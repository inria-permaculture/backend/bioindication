# pylint: disable=missing-module-docstring
from rest_framework import reverse, status
from rest_framework.test import APITestCase
from core.models import InventoryList, Inventory, Observation, Taxon


class TestInventoryBiodiversityApiViewTestCase(APITestCase):
    """
    Test the inventory biodiversity view
    """

    @staticmethod
    def create_root_taxon():
        """
        Create the root taxon, which references itself
        """
        root_taxon = Taxon.objects.create(id=1)
        root_taxon.parent = root_taxon
        root_taxon.save()
        return root_taxon

    def test_inventory_analys_on_a_non_existing_inventory_returns_http_code_404(self):
        """
        Test generating a biodiversity index of a non existing inventory must
        return http code 404
        """
        non_existing_inventory_id = 999

        biodiversity = self.client.get(
            reverse.reverse(
                "inventory-biodiversity",
                args=[non_existing_inventory_id]),
            format="json")

        self.assertEqual(biodiversity.status_code, status.HTTP_404_NOT_FOUND)

    def test_inv_biodiv_without_observation_returns_empty_biodiv_index(self):
        """
        Test generating an biodiv index on an inventory without any observation
        must return an empty biodiv
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)

        expected_biodiv_index = {
            "index": None
        }

        biodiversity = self.client.get(
            reverse.reverse(
                "inventory-biodiversity",
                args=[inventory.id]),
            format="json")

        self.assertEqual(biodiversity.data, expected_biodiv_index)
        self.assertEqual(biodiversity.status_code, status.HTTP_200_OK)

    def test_inv_biodiv_1_with_obs_must_returns_valid_biodiv_index(self):
        """
        Test generating an biodiv index on an inventory with some observations
        must return a valid biodiv index
        Current observations taxonomy :
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)

        root_taxon = self.create_root_taxon()

        taxon2 = Taxon.objects.create(id=2, parent=root_taxon)
        taxon3 = Taxon.objects.create(id=3, parent=root_taxon)

        Observation.objects.create(inventory=inventory, taxon=taxon2)
        Observation.objects.create(inventory=inventory, taxon=taxon3)

        expected_biodiv_index = {
            "index": 3
        }

        biodiversity = self.client.get(
            reverse.reverse(
                "inventory-biodiversity",
                args=[inventory.id]),
            format="json")

        self.assertEqual(biodiversity.data, expected_biodiv_index)
        self.assertEqual(biodiversity.status_code, status.HTTP_200_OK)

    def test_inv_biodiv_2_with_obs_must_returns_valid_biodiv_index(self):
        """
        Test generating an biodiv index on an inventory with some observations
        must return a valid biodiv index
        Current observations taxonomy :
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)

        root_taxon = self.create_root_taxon()

        taxon2 = Taxon.objects.create(id=2, parent=root_taxon)
        taxon3 = Taxon.objects.create(id=3, parent=root_taxon)
        taxon4 = Taxon.objects.create(id=4, parent=taxon3)
        taxon5 = Taxon.objects.create(id=5, parent=taxon3)

        Observation.objects.create(inventory=inventory, taxon=taxon2)
        Observation.objects.create(inventory=inventory, taxon=taxon3)
        Observation.objects.create(inventory=inventory, taxon=taxon4)
        Observation.objects.create(inventory=inventory, taxon=taxon5)

        expected_biodiv_index = {
            "index": 5
        }

        biodiversity = self.client.get(
            reverse.reverse(
                "inventory-biodiversity",
                args=[inventory.id]),
            format="json")

        self.assertEqual(biodiversity.data, expected_biodiv_index)
        self.assertEqual(biodiversity.status_code, status.HTTP_200_OK)

    def test_inv_biodiv_3_with_obs_must_returns_valid_biodiv_index(self):
        """
        Test generating an biodiv index on an inventory with some observations
        must return a valid biodiv index
        Current observations taxonomy :
        """
        inventory_list = InventoryList.objects.create()
        inventory = Inventory.objects.create(
            inventory_list=inventory_list)

        root_taxon = self.create_root_taxon()

        taxon2 = Taxon.objects.create(id=2, parent=root_taxon)
        taxon3 = Taxon.objects.create(id=3, parent=root_taxon)
        taxon4 = Taxon.objects.create(id=4, parent=taxon3)
        taxon5 = Taxon.objects.create(id=5, parent=taxon3)
        taxon6 = Taxon.objects.create(id=6, parent=taxon4)
        taxon7 = Taxon.objects.create(id=7, parent=taxon5)
        # just to test with a non observed leaf
        Taxon.objects.create(id=8, parent=taxon6)

        Observation.objects.create(inventory=inventory, taxon=taxon2)
        Observation.objects.create(inventory=inventory, taxon=taxon6)
        Observation.objects.create(inventory=inventory, taxon=taxon7)

        expected_biodiv_index = {
            "index": 7
        }

        biodiversity = self.client.get(
            reverse.reverse(
                "inventory-biodiversity",
                args=[inventory.id]),
            format="json")

        self.assertEqual(biodiversity.data, expected_biodiv_index)
        self.assertEqual(biodiversity.status_code, status.HTTP_200_OK)
