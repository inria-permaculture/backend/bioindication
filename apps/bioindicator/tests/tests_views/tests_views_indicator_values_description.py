# pylint: disable=missing-module-docstring
from rest_framework.test import APITestCase
from rest_framework import status, reverse
from core.models import IndicatorValuesDescription


class GetIndicatorValuesDescriptionViewsetTestCase(APITestCase):
    """
    Test the get indicator values description view
    """

    def test_get_with_existing_indic_values_desc_returns_http_code_200(self):
        """
        Test get indicator values description when exist must return a http code 200
        Migration should automatically create a single indicator values description
        """
        # indicator_values_description = IndicatorValuesDescription.objects.first()
        response = self.client.get(
            reverse.reverse("indicator-values-description"),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_with_non_existing_indic_values_desc_returns_http_code_404(self):
        """
        Test get indicator values description when exist must return a http code 200
        Migration should automatically create a single indicator values description
        so we need to delete it manually to test this case
        """
        IndicatorValuesDescription.objects.all().delete()
        response = self.client.get(
            reverse.reverse("indicator-values-description"),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
