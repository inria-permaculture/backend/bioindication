# pylint: disable=missing-module-docstring
from decimal import Decimal
from rest_framework import reverse, status
from rest_framework.test import APITestCase
from core.models import InventoryList, Inventory, Taxon, Observation, IndicatorValues


class TestInventoryListAnalysisApiViewTestCase(APITestCase):
    """
    Test the inventory list analysis view
    """

    def test_inventory_analys_on_a_non_existing_inventory_list_returns_http_code_404(self):
        """
        Test generating an analysis of a non existing inventory list
        must return http code 404
        """
        non_existing_inventory_list_id = 999

        analysis = self.client.get(
            reverse.reverse(
                "inventory-list-analysis",
                args=[non_existing_inventory_list_id]),
            format="json")

        self.assertEqual(analysis.status_code, status.HTTP_404_NOT_FOUND)

    def test_list_inv_analys_without_observation_returns_empty_analys(self):
        """
        Test generating an analysis on an inventory list
        with inv without any observation must return an empty analysis
        """
        inventory_list = InventoryList.objects.create()
        Inventory.objects.create(
            inventory_list=inventory_list)
        Inventory.objects.create(
            inventory_list=inventory_list)

        expected_analys = {
            "light": {"mean": None, "standard_deviation": None},
            "temperature": {"mean": None, "standard_deviation": None},
            "continentality": {"mean": None, "standard_deviation": None},
            "edaphic_moisture": {"mean": None, "standard_deviation": None},
            "atmospheric_moisture": {"mean": None, "standard_deviation": None},
            "soil_reaction": {"mean": None, "standard_deviation": None},
            "nutrient": {"mean": None, "standard_deviation": None},
            "salinity": {"mean": None, "standard_deviation": None},
            "soil_texture": {"mean": None, "standard_deviation": None},
            "organic_matter": {"mean": None, "standard_deviation": None},
        }

        analysis = self.client.get(
            reverse.reverse(
                "inventory-list-analysis",
                args=[inventory_list.id]),
            format="json")

        self.assertEqual(analysis.data, expected_analys)
        self.assertEqual(analysis.status_code, status.HTTP_200_OK)

    def test_list_inv_analys_with_quantified_obs_returns_valid_analysis(self):
        """
        Test generating an analysis on an inventory list
        with inv with quantified obs must return a valid analysis
        """
        taxon1 = Taxon.objects.create(id=1, is_a_plant=True)
        taxon2 = Taxon.objects.create(id=2, is_a_plant=True)
        IndicatorValues.objects.create(
            taxon=taxon1,
            light=6,
            temperature=6,
            continentality=6,
            edaphic_moisture=6,
            atmospheric_moisture=6,
            soil_reaction=6,
            nutrient=6,
            salinity=6,
            soil_texture=6,
            organic_matter=6,
        )
        IndicatorValues.objects.create(
            taxon=taxon2,
            light=4,
            temperature=4,
            continentality=4,
            edaphic_moisture=4,
            atmospheric_moisture=4,
            soil_reaction=4,
            nutrient=4,
            salinity=4,
            soil_texture=4,
            organic_matter=4,
        )
        inventory_list = InventoryList.objects.create()
        inventory1 = Inventory.objects.create(
            inventory_list=inventory_list)
        inventory2 = Inventory.objects.create(
            inventory_list=inventory_list)
        Observation.objects.create(
            inventory=inventory1, taxon=taxon1, soil_coverage=0.95)
        Observation.objects.create(
            inventory=inventory2, taxon=taxon2, soil_coverage=0.25)
        expected_analys = {
            "light": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "temperature": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "continentality": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "edaphic_moisture": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "atmospheric_moisture": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "soil_reaction": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "nutrient": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "salinity": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "soil_texture": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
            "organic_matter": {"mean": Decimal("5.6"), "standard_deviation": Decimal("1.15")},
        }

        analysis = self.client.get(
            reverse.reverse(
                "inventory-list-analysis",
                args=[inventory_list.id]),
            format="json")

        self.assertEqual(analysis.data, expected_analys)
        self.assertEqual(analysis.status_code, status.HTTP_200_OK)
