# pylint: disable=missing-module-docstring,too-few-public-methods
from django.db import models
from django.db.models import F, Q, Value, ExpressionWrapper
from django.db.models.fields import IntegerField
from django.db.models.functions import Greatest
from core.models import Observation, IndicatorValues
from bioindicator.services.services_inventories_analysis import InventoriesAnalysisService


class InventorySuggestionsService():
    """
    Manage suggestion of plants to grow for an inventory
    """

    @staticmethod
    def get_sql_dist_calc_for_indic(indicator, analysis):
        """
        Distance function for a given indicator
        Returns 0 if the analysis has no value for the indicator of interest
        """

        # WARNING: do not modify without modifying get_dist
        value = analysis[indicator]['mean']
        if value is not None:
            return (F(indicator) - value) * (F(indicator) - value)
        return Value(0, output_field=IntegerField())

    @staticmethod
    def get_dist(val1, val2):
        """
        Direct computation of the distance computed by get_sql_dist_calc_for_indic
        """

        # WARNING: do not modify without modifying get_sql_dist_calc_for_indic
        return (val1 - val2) * (val1 - val2)

    @staticmethod
    def get_max_global_dist():
        """
        Compute the maximum global distance
        (see __annotate_sum_global_distance)
        """
        light_max_dist = InventorySuggestionsService.get_dist(1, 9)
        temperature_max_dist = InventorySuggestionsService.get_dist(1, 9)
        continentality_max_dist = InventorySuggestionsService.get_dist(1, 9)
        edaphic_moisture_max_dist = InventorySuggestionsService.get_dist(1, 12)
        atmospheric_moisture_max_dist = InventorySuggestionsService.get_dist(1, 9)
        soil_reaction_max_dist = InventorySuggestionsService.get_dist(1, 9)
        nutrient_max_dist = InventorySuggestionsService.get_dist(1, 9)
        salinity_max_dist = InventorySuggestionsService.get_dist(0, 9)
        soil_texture_max_dist = InventorySuggestionsService.get_dist(1, 9)
        organic_matter_max_dist = InventorySuggestionsService.get_dist(1, 9)

        return (light_max_dist +
                temperature_max_dist +
                continentality_max_dist +
                edaphic_moisture_max_dist +
                atmospheric_moisture_max_dist +
                soil_reaction_max_dist +
                nutrient_max_dist +
                salinity_max_dist +
                soil_texture_max_dist +
                organic_matter_max_dist)

    @classmethod
    def get_suggested_plants_for_an_inventory(cls, inventory, filter_usages, nb_to_suggest=20):
        """
        Get a list of plants that fit well with a given inventory
        """
        indicators = IndicatorValues.objects.all()
        indicators = cls.__exclude_indic_of_taxa_already_present_in_inv(
            indicators, inventory.id)
        analysis = InventoriesAnalysisService.generate_plants_analysis_of_inventories(
            [inventory.id])
        suggested_plants = cls.__get_suggested_plants_for_an_analysis(
            indicators, analysis, filter_usages, nb_to_suggest)
        return suggested_plants

    @classmethod
    def __exclude_indic_of_taxa_already_present_in_inv(cls, indicators_queryset, inventory_id):
        """
        Exclude all already observed taxa, because we don't
        want to suggest these if they are already here
        """
        already_present_taxa = Observation.objects.filter(
            inventory_id=inventory_id).values("taxon_id")
        for taxon in already_present_taxa:
            indicators_queryset = indicators_queryset.exclude(
                taxon_id=taxon['taxon_id'])
        return indicators_queryset

    @classmethod
    def __get_suggested_plants_for_an_analysis(cls, indicators_queryset, analysis,
                                               filter_usages, nb_to_suggest):
        """
        Get a list of plants that fit well with a given analysis
        with plants usages matching the given usages
        """
        if cls.__check_if_analysis_is_empty(analysis):
            return []
        indicators_queryset = cls.__exclude_indic_of_unsuggestible_taxa(
            indicators_queryset)
        indicators_queryset = cls.__exclude_indic_not_matching_given_filters(
            indicators_queryset, filter_usages)
        indicators_queryset = cls.__exclude_indic_with_at_least_one_undefined_value(
            indicators_queryset)
        indicators_queryset = cls.__annotate_distance_for_each_indic_value(
            indicators_queryset, analysis)
        indicators_queryset = cls.__annotate_sum_global_distance(
            indicators_queryset)
        indicators_queryset = cls.__get_most_fitting_indicators(
            indicators_queryset, nb_to_suggest)
        id_of_suggested_plants = cls.__get_plants_related_to_indicators(
            indicators_queryset)
        return id_of_suggested_plants

    @classmethod
    def __check_if_analysis_is_empty(cls, analysis):
        """
        Check if all mean values of an analysis are undefined
        """
        is_analysis_empty = True
        for value in analysis:
            if analysis[value]['mean'] is not None:
                is_analysis_empty = False
                break
        return is_analysis_empty

    @classmethod
    def __exclude_indic_of_unsuggestible_taxa(cls, indicators_queryset):
        """
        Exclude indicator values related to non plants taxa and
        taxa without a related plant usages row.
        It's not really usefull now because we have only indic
        for plant taxa and all plant taxa have a
        related plant usages(but still could be with all usages = false)
        """
        return indicators_queryset.exclude(
            taxon__is_a_plant=False, taxon__plant_usages__isnull=True)

    @classmethod
    def __exclude_indic_not_matching_given_filters(cls, indicators_queryset, filter_usages):
        # pylint: disable=too-many-branches
        """
        Exclude indicator values related to taxa not matching given filters
        """
        if filter_usages["is_domesticated"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__is_domesticated=False)
        if filter_usages["food"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__food=False)
        if filter_usages["honey"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__honey=False)
        if filter_usages["ornamental"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__ornamental=False)
        if filter_usages["medicine"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__medicine=False)
        if filter_usages["fuelwood"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__fuelwood=False)
        if filter_usages["construction"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__construction=False)
        if filter_usages["psychotropic"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__psychotropic=False)
        if filter_usages["poisonous"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__poisonous=False)
        if filter_usages["green_manure"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__green_manure=False)
        if filter_usages["basketry"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__basketry=False)
        if filter_usages["depolluting"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__depolluting=False)
        if filter_usages["textile"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__textile=False)
        if filter_usages["paper"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__paper=False)
        if filter_usages["tinctorial"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__tinctorial=False)
        if filter_usages["fragrance"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__fragrance=False)
        if "stratum" in filter_usages:
            if filter_usages["stratum"] != "ANY":
                indicators_queryset = indicators_queryset.filter(
                    taxon__plant_usages__stratum=filter_usages["stratum"])
        if filter_usages["invasive"]:
            indicators_queryset = indicators_queryset.exclude(
                taxon__plant_usages__invasive=False)
        return indicators_queryset

    @classmethod
    def __exclude_indic_with_at_least_one_undefined_value(cls, indicators_queryset):
        """
        Exclude all indicators with at least one undefined
        indicator value. Because without that exlusion, they will be more
        often suggested even if they don't fit very well
        """
        return indicators_queryset.exclude(
            Q(light__isnull=True) |
            Q(temperature__isnull=True) |
            Q(continentality__isnull=True) |
            Q(edaphic_moisture__isnull=True) |
            Q(atmospheric_moisture__isnull=True) |
            Q(soil_reaction__isnull=True) |
            Q(nutrient__isnull=True) |
            Q(salinity__isnull=True) |
            Q(soil_texture__isnull=True) |
            Q(organic_matter__isnull=True)
        )

    @classmethod
    def __annotate_distance_for_each_indic_value(cls, indicators_queryset, analysis):
        """
        Annotate queryset with a calculated distance between each indicator value
        and related analysis value
        """
        indicators_queryset = indicators_queryset.annotate(
            light_dist=InventorySuggestionsService.get_sql_dist_calc_for_indic(
                "light", analysis))
        indicators_queryset = indicators_queryset.annotate(
            temperature_dist=InventorySuggestionsService.get_sql_dist_calc_for_indic(
                "temperature", analysis))
        indicators_queryset = indicators_queryset.annotate(
            continentality_dist=InventorySuggestionsService.get_sql_dist_calc_for_indic(
                "continentality", analysis))
        indicators_queryset = indicators_queryset.annotate(
            edaphic_moisture_dist=InventorySuggestionsService.get_sql_dist_calc_for_indic(
                "edaphic_moisture", analysis))
        indicators_queryset = indicators_queryset.annotate(
            atmospheric_moisture_dist=InventorySuggestionsService.get_sql_dist_calc_for_indic(
                "atmospheric_moisture", analysis))
        indicators_queryset = indicators_queryset.annotate(
            soil_reaction_dist=InventorySuggestionsService.get_sql_dist_calc_for_indic(
                "soil_reaction", analysis))
        indicators_queryset = indicators_queryset.annotate(
            nutrient_dist=InventorySuggestionsService.get_sql_dist_calc_for_indic(
                "nutrient", analysis))
        indicators_queryset = indicators_queryset.annotate(
            salinity_dist=InventorySuggestionsService.get_sql_dist_calc_for_indic(
                "salinity", analysis))
        indicators_queryset = indicators_queryset.annotate(
            soil_texture_dist=InventorySuggestionsService.get_sql_dist_calc_for_indic(
                "soil_texture", analysis))
        indicators_queryset = indicators_queryset.annotate(
            organic_matter_dist=InventorySuggestionsService.get_sql_dist_calc_for_indic(
                "organic_matter", analysis))

        return indicators_queryset

    @classmethod
    def __annotate_sum_global_distance(cls, indicators_queryset):
        """
        Annotate queryset with a calculated global distance
        Global distance formula = Sum(each_field_distance)
        """
        indicators_queryset = indicators_queryset.annotate(
            distance=(
                F("light_dist") +
                F("temperature_dist") +
                F("continentality_dist") +
                F("edaphic_moisture_dist") +
                F("atmospheric_moisture_dist") +
                F("soil_reaction_dist") +
                F("nutrient_dist") +
                F("salinity_dist") +
                F("soil_texture_dist") +
                F("organic_matter_dist")
            )
        )
        max_relevant_dist = 100  # Empiric
        indicators_queryset = indicators_queryset.annotate(
            score=ExpressionWrapper(
                Greatest(0, (1 - F("distance") / max_relevant_dist) * 100),
                output_field=models.DecimalField()
            )
        )
        return indicators_queryset

    @classmethod
    def __get_most_fitting_indicators(cls, indicators_queryset, nb_to_get):
        """
        Get the first indicators closest to the inventory analysis
        """
        return indicators_queryset.order_by(
            "distance")[:nb_to_get]

    @classmethod
    def __get_plants_related_to_indicators(cls, indicators_queryset):
        """
        Get plants related to a list of indicators
        """
        plants = []
        for indicator in indicators_queryset:
            plants.append({"taxon": indicator.taxon,
                           "distance": indicator.distance,
                           "score": indicator.score})
        return plants
