# pylint: disable=missing-module-docstring, too-few-public-methods
class InventoryBiodiversityService():
    """
    Compute biodiversity for a given inventory
    """

    @classmethod
    def compute_biodiversity_index(cls, inventory):
        """
        Compute the biodiversity index of an inventory:
          * Number of nodes (either internal or leaves) in the
            inventory's induced subtree
        """
        taxa = []  # List of encountered taxa
        for obs in inventory.observation_set.all():
            taxa.append(obs.taxon)

        if len(taxa) == 0:
            return {
                'index': None
            }

        # Build phylogenetic tree
        tree = {}  # taxon-id: [parent, [children]]
        i = 0
        while i < len(taxa):
            taxon = taxa[i]
            if taxon not in tree:
                tree[taxon] = [-1, []]
            if taxon.parent not in tree:
                tree[taxon.parent] = [-1, []]
            if taxon.parent not in taxa:
                taxa.append(taxon.parent)
            if taxon != taxon.parent:
                tree[taxon][0] = taxon.parent
                tree[taxon.parent][1].append(taxon)
            i = i + 1

        # Remove "trunk" of the tree
        # (anything between the root and the first branching point)
        root = taxa[-1]
        while len(tree[root][1]) == 1:
            current = tree[root][1][0]  # First and only child of root
            del tree[root]
            tree[current][0] = -1
            root = current

        biodiversity = {
            'index': len(tree.keys())
        }

        return biodiversity
