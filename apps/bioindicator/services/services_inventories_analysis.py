# pylint: disable=missing-module-docstring, too-few-public-methods
from core.models import IndicatorValues, Observation
from core.utils import (calc_weighted_mean,
                        calc_weighted_squared_deviation_to_mean,
                        calc_corrected_weighted_standard_deviation)


class InventoriesAnalysisService():
    """
    Manage calculation of an analysis for a list of inventories
    """

    # get all indic values from IndicatorValues model
    indicator_values_fields = [field.name for field in IndicatorValues._meta.fields
                               if field.name not in (
                                   "taxon", "history") and "stdev" not in field.name]

    @staticmethod
    def __get_indicators_with_soil_coverage_list(observations):
        """
        Create a list of indicators values with related weights from a
        list of observations
        """
        indicators_with_soil_coverage_list = []
        for observation in observations:
            try:
                taxon_indicator_values = IndicatorValues.objects.get(
                    taxon=observation.taxon.id)
                indicators_with_soil_coverage_list.append(
                    {
                        "indicator_values": taxon_indicator_values,
                        "weight": observation.soil_coverage
                    }
                )
            except IndicatorValues.DoesNotExist:
                pass
        return indicators_with_soil_coverage_list

    @staticmethod
    def __fill_missing_weights(indicators_with_soil_coverage_list, observations):
        """
        Fill missing weights
        We fill it with the value of the mean of other weights
        or with the value 1/nb observations if not weight is defined
        """
        not_none_weights = [
            element["weight"] for element in indicators_with_soil_coverage_list
            if element["weight"] is not None
        ]
        # replace each missing weight with the mean of other weights
        if len(not_none_weights) > 0:
            weights_mean = sum(not_none_weights) / len(not_none_weights)
            for element in indicators_with_soil_coverage_list:
                if element["weight"] is None:
                    element["weight"] = weights_mean
        # if all weights are missing, we are giving them the value of 1/nb_observations
        else:
            nb_observations = len(observations)
            for element in indicators_with_soil_coverage_list:
                element["weight"] = nb_observations
        return indicators_with_soil_coverage_list

    @classmethod
    def __fill_missing_indicators_values(cls, indicators_with_soil_coverage_list):
        """
        Fill missing indicators values
        We fill it with the value of the mean of other values
        or with none if no value is defined
        """
        for indicator_key in cls.indicator_values_fields:
            not_none_values = [
                getattr(element["indicator_values"], indicator_key)
                for element in indicators_with_soil_coverage_list
                if getattr(element["indicator_values"], indicator_key) is not None
            ]
            # replace each missing value with the mean of all value of the current indicator
            if len(not_none_values) > 0:
                value_mean = sum(not_none_values) / len(not_none_values)
                for element in indicators_with_soil_coverage_list:
                    if getattr(element["indicator_values"], indicator_key) is None:
                        setattr(element["indicator_values"],
                                indicator_key, value_mean)
            # if all values for this indicator are missing, we are giving them the None value
            else:
                for element in indicators_with_soil_coverage_list:
                    setattr(element["indicator_values"], indicator_key, None)
        return indicators_with_soil_coverage_list

    @classmethod
    def __calculate_mean_stdev_of_analysis(cls, analysis, indicators_with_soil_coverage_list):
        """
        Calculate mean and stdev for each indicator of analysis
        """
        # calculate mean and stdev for each indicator
        for indicator_key in cls.indicator_values_fields:
            # pylint: disable=cell-var-from-loop
            not_none_values = [
                getattr(element["indicator_values"], indicator_key)
                for element in indicators_with_soil_coverage_list
                if getattr(element["indicator_values"], indicator_key) is not None
            ]
            # calculate mean and stdev only for indicator without none value
            if len(not_none_values) == len(indicators_with_soil_coverage_list):
                # extract all weighted values for the current indicator
                weighted_values = list(
                    map(lambda element:
                        getattr(element["indicator_values"],
                                indicator_key) * element["weight"],
                        indicators_with_soil_coverage_list)
                )

                # extract all weights
                weights = list(
                    map(lambda element:
                        element["weight"],
                        indicators_with_soil_coverage_list)
                )

                # calculate the weighted mean
                mean = calc_weighted_mean(weighted_values, weights)
                analysis[indicator_key]["mean"] = mean

                sum_weighted_squared_deviation_to_mean = sum(list(
                    map(lambda element:
                        calc_weighted_squared_deviation_to_mean(
                            element["weight"],
                            getattr(element["indicator_values"],
                                    indicator_key),
                            mean),
                        indicators_with_soil_coverage_list)
                ))

                # calculate the weighted corrected standard deviation
                standard_deviation = calc_corrected_weighted_standard_deviation(
                    sum_weighted_squared_deviation_to_mean, weights)
                analysis[indicator_key]["standard_deviation"] = standard_deviation
        return analysis

    @classmethod
    def generate_plants_analysis_of_inventories(cls, inventories_id):
        """
        Generate an analysis from the plants observed in the given list of
        inventory.
        The analysis is composed of a mean and a standard deviation
        for each indicator values
        """
        analysis = {
            "light": {"mean": None, "standard_deviation": None},
            "temperature": {"mean": None, "standard_deviation": None},
            "continentality": {"mean": None, "standard_deviation": None},
            "edaphic_moisture": {"mean": None, "standard_deviation": None},
            "atmospheric_moisture": {"mean": None, "standard_deviation": None},
            "soil_reaction": {"mean": None, "standard_deviation": None},
            "nutrient": {"mean": None, "standard_deviation": None},
            "salinity": {"mean": None, "standard_deviation": None},
            "soil_texture": {"mean": None, "standard_deviation": None},
            "organic_matter": {"mean": None, "standard_deviation": None},
        }
        observations = Observation.objects.filter(
            inventory_id__in=inventories_id, taxon__is_a_plant=True)

        # get all indicators values and related soil coverage from obs
        indicators_with_soil_coverage_list = cls.__get_indicators_with_soil_coverage_list(
            observations)

        # return empty analysis if we haven't any values
        if len(indicators_with_soil_coverage_list) > 0:
            indicators_with_soil_coverage_list = cls.__fill_missing_weights(
                indicators_with_soil_coverage_list, observations)
            indicators_with_soil_coverage_list = cls.__fill_missing_indicators_values(
                indicators_with_soil_coverage_list)

            analysis = cls.__calculate_mean_stdev_of_analysis(analysis,
                                                              indicators_with_soil_coverage_list)
        return analysis
