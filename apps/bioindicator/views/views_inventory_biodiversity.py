# pylint: disable=missing-module-docstring
from rest_framework import views, status
from rest_framework.response import Response
from django.http import Http404
from drf_spectacular.utils import (extend_schema, OpenApiResponse)
from core.utils import http_codes_description
from core.models import Inventory
from bioindicator.serializers import InventoryBiodiversitySerializer
from bioindicator.services import InventoryBiodiversityService


class InventoryBiodiversityView(views.APIView):
    """
    View related to inventory biodiversity
    """

    def get_object(self, pk):
        # pylint: disable=no-self-use
        """
        Retrieve an inventory object based on its primary key
        """
        try:
            return Inventory.objects.get(pk=pk)
        except Inventory.DoesNotExist as error:
            raise Http404 from error

    @extend_schema(responses={
        200: OpenApiResponse(response=InventoryBiodiversitySerializer,
                             description=http_codes_description.SUCCESS),
        404: OpenApiResponse(description=http_codes_description.NOT_FOUND),
    })
    def get(self, request, pk):
        """
        Compute the biodiversity index of the given inventory
        """
        inventory = self.get_object(pk)
        inventory_biodiversity = \
            InventoryBiodiversityService.compute_biodiversity_index(inventory)
        serializer = InventoryBiodiversitySerializer(inventory_biodiversity)
        return Response(serializer.data, status=status.HTTP_200_OK)
