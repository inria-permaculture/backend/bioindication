# pylint: disable=missing-module-docstring
from rest_framework import views, status
from rest_framework.response import Response
from django.http import Http404
from drf_spectacular.utils import (extend_schema, OpenApiResponse)
from core.utils import http_codes_description
from core.models import InventoryList, Inventory
from bioindicator.serializers import InventoriesAnalysisSerializer
from bioindicator.services import InventoriesAnalysisService


class InventoryListAnalysisView(views.APIView):
    """
    View related to inventory list analysis manipulations
    """

    def get_object(self, pk):
        # pylint: disable=invalid-name, no-self-use
        """
        Retrieve an inventory list object based on its primary key
        """
        try:
            return InventoryList.objects.get(pk=pk)
        except InventoryList.DoesNotExist as error:
            raise Http404 from error

    @extend_schema(responses={
        200: OpenApiResponse(response=InventoriesAnalysisSerializer,
                             description=http_codes_description.SUCCESS),
        404: OpenApiResponse(description=http_codes_description.NOT_FOUND),
    })
    def get(self, request, pk):
        # pylint: disable=invalid-name
        """
        Generate the analysis of a given inventory list
        """
        inventory_list = self.get_object(pk)
        inventories = Inventory.objects.filter(
            inventory_list__id=inventory_list.id)
        inventories_id = [inv.id for inv in inventories]
        inv_list_analysis = InventoriesAnalysisService.generate_plants_analysis_of_inventories(
            inventories_id)
        serializer = InventoriesAnalysisSerializer(inv_list_analysis)
        return Response(serializer.data, status=status.HTTP_200_OK)
