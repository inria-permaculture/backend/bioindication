# pylint: disable=missing-module-docstring
from rest_framework import views, status
from rest_framework.response import Response
from django.http import Http404
from drf_spectacular.utils import (extend_schema, OpenApiResponse)
from core.utils import http_codes_description
from core.models import Inventory
from bioindicator.serializers import (
    InventorySuggestionsSerializer, InventorySuggestionsFiltersQuerySerializer)
from bioindicator.services import InventorySuggestionsService


class InventorySuggestionsView(views.APIView):
    """
    View related to inventory suggestions manipulations
    """

    def get_object(self, pk):
        # pylint: disable=invalid-name, no-self-use
        """
        Retrieve an inventory object based on its primary key
        """
        try:
            return Inventory.objects.get(pk=pk)
        except Inventory.DoesNotExist as error:
            raise Http404 from error

    @extend_schema(
        parameters=[InventorySuggestionsFiltersQuerySerializer],
        responses={
            200: OpenApiResponse(response=InventorySuggestionsSerializer(many=True),
                                 description=http_codes_description.SUCCESS),
            404: OpenApiResponse(description=http_codes_description.NOT_FOUND),
        })
    def get(self, request, pk):
        # pylint: disable=invalid-name
        """
        Get a list of suggested plants for the given inventory
        """
        inventory = self.get_object(pk)

        query_params_serializer = InventorySuggestionsFiltersQuerySerializer(
            data=request.query_params)
        query_params_serializer.is_valid(raise_exception=True)
        filter_usages = query_params_serializer.data

        inventory_suggestions_service = InventorySuggestionsService()
        suggested_plants = inventory_suggestions_service.get_suggested_plants_for_an_inventory(
            inventory,
            filter_usages
        )

        serializer = InventorySuggestionsSerializer(
            suggested_plants, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)
