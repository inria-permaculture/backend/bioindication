# pylint: disable=missing-module-docstring
from rest_framework import views, status
from rest_framework.response import Response
from drf_spectacular.utils import (extend_schema, OpenApiResponse)
from core.utils import http_codes_description
from core.models import IndicatorValuesDescription
from bioindicator.serializers import IndicatorValuesDescriptionSerializer


class IndicatorValuesDescriptionView(views.APIView):
    """
    View related to indicator values description
    """

    @extend_schema(responses={
        200: OpenApiResponse(response=IndicatorValuesDescriptionSerializer,
                             description=http_codes_description.SUCCESS),
        404: OpenApiResponse(description=http_codes_description.NOT_FOUND),
    })
    def get(self, request):
        # pylint: disable=unused-argument
        """
        Retrieve the unique description of the indicator values
        """
        description = IndicatorValuesDescription.objects.first()
        if not description:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = IndicatorValuesDescriptionSerializer(description)
        return Response(serializer.data, status=status.HTTP_200_OK)
