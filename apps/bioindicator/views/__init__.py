"""
API endpoints
"""

from bioindicator.views.views_indicator_values_description import *
from bioindicator.views.views_inventory_analysis import *
from bioindicator.views.views_inventory_list_analysis import *
from bioindicator.views.views_inventory_suggestions import *
from bioindicator.views.views_inventory_biodiversity import *
