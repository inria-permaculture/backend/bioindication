"""
API endpoints routes definitions
"""
from rest_framework.routers import DefaultRouter
from django.urls import path
from bioindicator.views import (
    IndicatorValuesDescriptionView, InventoryAnalysisView,
    InventoryListAnalysisView, InventorySuggestionsView,
    InventoryBiodiversityView)
from bioindicator.viewsets import (IndicatorValuesViewSet, PlantUsagesViewSet)


urlpatterns = [
    path('indicator-values-description/', IndicatorValuesDescriptionView.as_view(),
         name="indicator-values-description"),
    path('inventories/<int:pk>/analysis/',
         InventoryAnalysisView.as_view(), name="inventory-analysis"),
    path('inventory-lists/<int:pk>/analysis/',
         InventoryListAnalysisView.as_view(), name="inventory-list-analysis"),
    path('inventories/<int:pk>/suggestions/',
         InventorySuggestionsView.as_view(), name="inventory-suggestions"),
    path('inventories/<int:pk>/biodiversity/',
         InventoryBiodiversityView.as_view(), name="inventory-biodiversity")
]

router = DefaultRouter()
router.register(r"indicators-values", IndicatorValuesViewSet,
                basename="indicator-values")
router.register(r"plants-usages", PlantUsagesViewSet,
                basename="plant-usages")
urlpatterns += router.urls
