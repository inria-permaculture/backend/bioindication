# Bioindication - backend

## Architecture
If you want to understand this repository architecture, here the detailled folder structure :

    bioindicator ->  App to manage bioindication data and diagnostics
    core -> Common stuff related to multiple apps
    identify -> App to identify taxons based on texts, photos or sounds
    inventory -> App to manage shared inventories of the life forms observed in various places
    vernaclarify -> App to manage names(scientific, vernacular), taxonomy and taxon