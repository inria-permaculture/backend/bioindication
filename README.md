# Bioindication - backend
Universal collaborative platform backend with various goals:
- Knowledge building and sharing on scientific or common names of living organisms
- Knowledge building and sharing on bioindication data
- Creation of shared inventories of the life forms observed in various places


## Set up your development environment

This back is used both by https://bioindication.com/ and https://weedipedia.bioindication.com/

In order to make your own local dev environment, we recommend you install all 3 components locally.

The following takes you through the install process for the bioindication back.
To install the fronts, please see
* https://gitlab.com/inria-permaculture/frontend/bioindication
* https://gitlab.com/inria-permaculture/frontend/weedipedia


### Fork the project

Go to https://gitlab.com/inria-permaculture/backend/bioindication and click on the `Fork` button at the top-right corner.

Fill in the form with:

* Project name: we recommend you pick `bioindication_back`
* Project URL: we recommend you place the project in your personnal namespace
* Project slug: we recommend you pick `bioindication_back`
* The Project description doesn't matter
* Visibility level: the project being forked is private so your fork has to be private too

Finally, click on the  `Fork project` button

This creates your own **copy** of the project, including your own git repository, you can push to it regardless of your permissions on the original project a.k.a. *upstream*, you will then be able to submit your work to *upstream* as a Merge Request.


### Get the source

Go to your fork gitlab page (if you've just forked the project this is the page you are on), click on the `Clone` button and copy the url under `Clone with SSH`.

Then, open a terminal, clone the repo and `cd` into it:

```sh
git clone <your-forked-repo-url> bioindication_back
cd bioindication_back
```


### Install the django project and depencies
You need to install Docker on your machine. You can follow this tutorial : https://docs.docker.com/get-docker/.

By default docker can only be ran by the root user, or by prefacing every docker command by "sudo". If you would like to run docker commands without sudo, there is a way described here : https://docs.docker.com/engine/install/linux-postinstall/.

Now you can build and start the containers with this command (prefaced by `sudo` if you did not make the short adaptation mentioned above):

```sh
docker-compose -f deployment/docker-compose.yml up -d --build
```

If you want to check that your newly created containers are running, run the following command:

```sh
docker container ls
```

You should get an output similar to this one:

```
CONTAINER ID   IMAGE                  COMMAND                  CREATED          STATUS          PORTS                    NAMES
5eafaf77b8f3   bioindication_dev      "/usr/src/app/deploy…"   36 seconds ago   Up 32 seconds   0.0.0.0:8000->8000/tcp   bioindication_dev
acb587ec0db6   postgres:12.0-alpine   "docker-entrypoint.s…"   38 seconds ago   Up 35 seconds   5432/tcp                 bioindication_db_dev
```

If you want to stop the containers run:

```sh
docker-compose -f deployment/docker-compose.yml down
```

And restart with:

```sh
docker-compose -f deployment/docker-compose.yml up -d 
```

## Populate the database

### Taxonomy

If you want to populate the database with ncbi/taxonomy-fr data, retrieve files `taxonomy.csv` and `vernacular_names.csv` from  https://gitlab.com/inria-permaculture/backend/ncbi-taxonomy-data-extractor
and copy them into the bioindication container. In the dev configuration this can be done by simply copying the files in the bioindication directory.

Then, to import the data into your database, run this command:

```sh
docker exec bioindication_dev python manage.py import_taxonomy taxonomy.csv vernacular_names.csv
```

This should take quite some time since there is a lot of data to import

### Bioindication values

Retrieve file `baseflor-cgb.csv` from https://gitlab.com/inria-permaculture/backend/baseflor-ellenberg-data-extractor and copy it into the bioindication container. In the dev configuration this can be done by simply copying the file in the bioindication directory.

Then, to import the data into your database, run this command:

```sh
docker exec bioindication_dev python manage.py import_indicators_values baseflor-cgb.csv
```

### Plant usage data

Retrieve file 'ethnobotanique_synonymes.csv' from https://gitlab.com/inria-permaculture/document/data and copy it into the bioindication container. In the dev configuration this can be done by simply copying the file in the bioindication directory.

Then, to import the data into your database, run this command:

```sh
docker exec bioindication_dev python manage.py import_plants_usages ethnobotanique_synonymes.csv
```


## Reset the database

If you need to reset the database, run:

```sh
docker exec bioindication_dev python manage.py flush --no-input
```

## Usage
The server runs automatically after the containers start. By default, the server listens on port 8000.

For example, you can try to request the taxon with id = 1 by visiting the following URL in your favorite web browser: http://localhost:8000/v1/vernaclarify/taxon/1/

You should get the following result:

```
"id":1,
"parent":100901,
"rank":"superkingdom",
"name":"Bacteria"
```

## Tests
To test the project, you can use the following commands:

```sh
docker exec bioindication_dev python testing/run_tests.py
docker exec bioindication_dev python testing/run_lint.py
```

This will run resp. unit tests and linting.

It is recommended you run both commands before you push your changes since these will be run by continuous integration

## Tech & framework
- Python
- Django
- Django Rest Framework
- PostgreSQL
- Docker
- docker-compose
