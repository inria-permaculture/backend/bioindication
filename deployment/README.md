# Bioindication - backend

## Architecture
If you want to understand this repository architecture, here the detailled folder structure :

    backup.sh -> Database backup script
    docker-compose.prod.yml -> Multi containers orchestrator used for the creation of the production environment
    docker-compose.yml -> Multi containers orchestrator used for the creation of the development environment
    Dockerfile -> Django app docker image for development environnement
    Dockerfile.nginx -> Nginx docker image
    Dockerfile.postgresql -> Postgresql docker image
    Dockerfile.prod -> Django app docker image for production environnement
    extract-db-for-odbl.sh -> Extract tables to be publicy exposed for odbl compliance
    entrypoint.sh -> Entrypoint script used in the development docker image
    init-letsencrypt.sh -> Initialization of letsencrypt certificate to use during deployments
    nginx.conf -> Nginx configuration file
    requirements.txt -> Used for specifying what python packages are required to run this django project/application
