#!/bin/sh
# Backup of our database
pg_dump --username=$POSTGRES_USER --dbname=$POSTGRES_DB --format=t --file=home/db_backup/bioindication_db_dump.tar
