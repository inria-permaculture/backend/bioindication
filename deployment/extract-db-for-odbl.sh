#!/bin/sh
# Backup of data related to vernaclarify
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_taxon TO /home/open_db/taxons.csv DELIMITER',' CSV HEADER;"
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_historicaltaxon TO /home/open_db/historicaltaxon.csv DELIMITER',' CSV HEADER;"
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_scientificentry TO /home/open_db/scientifics.csv DELIMITER',' CSV HEADER;"
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_historicalscientificentry TO /home/open_db/historicalscientificentry.csv DELIMITER',' CSV HEADER;"
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_vernacularentry TO /home/open_db/vernaculars.csv DELIMITER',' CSV HEADER;"
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_historicalvernacularentry TO /home/open_db/historicalsvernacularentry.csv DELIMITER',' CSV HEADER;"
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_identifiers TO /home/open_db/identifiers.csv DELIMITER',' CSV HEADER;"
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_historicalidentifiers TO /home/open_db/historicalidentifiers.csv DELIMITER',' CSV HEADER;"

# Backup of data related to bioindicator
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_indicatorvalues TO /home/open_db/indicatorvalues.csv DELIMITER',' CSV HEADER;"
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_historicalindicatorvalues TO /home/open_db/historicalindicatorvalues.csv DELIMITER',' CSV HEADER;"
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_indicatorvaluesdescription TO /home/open_db/indicatorvaluesdescription.csv DELIMITER',' CSV HEADER;"
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_historicalindicatorvaluesdescription TO /home/open_db/historicalindicatorvaluesdescription.csv DELIMITER',' CSV HEADER;"
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_plantusages TO /home/open_db/plantusages.csv DELIMITER',' CSV HEADER;"
psql --username=$POSTGRES_USER --dbname=$POSTGRES_DB -c "\copy core_historicalplantusages TO /home/open_db/historicalplantusages.csv DELIMITER',' CSV HEADER;"
